﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zone : MonoBehaviour
{
	// übergibt die positive Blickrichtung der Bewegungszone, damit sie direkt auf die Beschleunigung gerechnet werden kann
	public Vector3 rep ()
	{
		Vector3 result;

		result = this.transform.forward;

		return result;
	}
}
