﻿using UnityEngine;

public class EnemyProjectileShootgunBehavior : MonoBehaviour
{

	public float speed;
	private float spawnTime;

	// push wird durch die Methode Direction festgelegt und stellt die Abweichung vom Ziel dar
	// zur Sicherstellung der beständigen Zugriffsmöglichkeit, wird der Vektor mit Nullwerten initialisiert
	// directionModifikation ist die resultierende Abweichung
	private Vector3 push = Vector3.zero;
	//private Vector3 directionModifikation = Vector3.zero;


	private void Start ()
	{
		Vector3 towardsPlayer = Camera.main.transform.position - transform.position;
		// auf die Zielposition wird push aufaddiert
		// dadruch ergibt sich eine neue Zielposition, welche dann für die Roation als Vorne bestimmt wird
		towardsPlayer += push;
		transform.rotation = Quaternion.LookRotation (towardsPlayer, Vector3.up);

		spawnTime = Time.time;

		PistolTarget pistolTarget = GetComponent<PistolTarget> ();
		if (pistolTarget != null) {
			pistolTarget.OnHit += (hitGameObject => {
				Disappear ();
				ParticleSystem.MainModule particleSystemMain = GetComponent<ParticleSystem> ().main;
				particleSystemMain.simulationSpeed *= 3;
			});
		}

        
	}

	void Update ()
	{
		Vector3 direction = transform.TransformDirection (Vector3.forward);


		transform.position += direction * speed * Time.deltaTime;

		// failsafe: remove projectile 2 minutes after spawn
		// in case the projectile somehow managed to get through the walls
		if (spawnTime + 120 < Time.time) {
			Destroy (gameObject);
		}

	}

	private bool IsValidCollisionTarget (Collider other)
	{
		return other.GetComponent<DroneFiringPoint> () == null
		&& other.GetComponent<DroneRepellingSphere> () == null
		&& other.GetComponent<EnemyProjectileShootgunBehavior> () == null
		&& other.GetComponent<EnemyProjectileParabolaBehavior> () == null
		&& other.GetComponentInParent<DroneBehavior> () == null;

		// should only leave objects that have PlayerHitVolume
		// ...and the good old walls
	}

	private void OnTriggerEnter (Collider other)
	{
		// überprüft bei Kollision, ob es sich um einen Collider handelt, der den Schuss abfängt oder nicht 
		// der große Collider der Spinne und der Wand sollen den Schuss nicht abfangen
		if (other.GetComponent<wall> () == null && other.GetComponent<zone> () == null && other.GetComponent<shoot> () == null) { 
			if (other.GetComponent<move> () != null && other.GetComponent<move> ().colliderGros != other || other.GetComponent<move> () == null) {

				if (IsValidCollisionTarget (other)) {
					PlayerHitVolume playerHitVolume = other.GetComponent<PlayerHitVolume> ();
					if (playerHitVolume != null) {
						playerHitVolume.Hit (gameObject);
					}

					if (other.GetComponent<PistolControl> () != null) {
						Vector3 direction = transform.TransformDirection (Vector3.forward);
						Vector3 impactPoint = other.ClosestPoint (transform.position);
						Vector3 normal = (transform.position - impactPoint).normalized;
						Vector3 newDirection = Vector3.Reflect (direction, normal);
						transform.rotation *= Quaternion.FromToRotation (direction, newDirection);
					} else {
						Disappear ();
					}
				}
			}
		}

	}

	public void Disappear ()
	{
		// stop movement
		speed = 0;

		// don't get hit anymore
		Collider collider = GetComponent<Collider> ();
		if (collider != null) {
			collider.enabled = false;
		}

		// stop emitting new particles
		ParticleSystem particleSystem = GetComponent<ParticleSystem> ();
		particleSystem.Stop (true, ParticleSystemStopBehavior.StopEmitting);

		// destroy self after all already existing particles have disappeared
		float remainingLifetime = particleSystem.main.startLifetime.constantMax;
		Destroy (gameObject, remainingLifetime);
	}

	// wird beim Erstellen ausgeführt, um die Abweichung vom Ziel zu bestimmen
	public void Direction (float x, float y, float z)
	{
		push.x = x;
		push.y = y;
		push.z = z;
	}
}
