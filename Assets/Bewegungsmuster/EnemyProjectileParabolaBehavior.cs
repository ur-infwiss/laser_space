﻿using UnityEngine;

public class EnemyProjectileParabolaBehavior : MonoBehaviour
{

	// time ist die Zeit, bis das Projektil das Ziel erreicht
	// gravitation ist die Komponente, welche den Wert der Y-Achse kontinuierlich verringert
	// initSpeed ist die Anfangsgeschwindigkeit, mit der das Projektil gestartet werden muss, dammit das Ziel getroffen wird
	private float time;
	private Vector3 gravitation;
	private Vector3 initSpeed;

	private float spawnTime;



	private void Start ()
	{
        //Tag the Game Object
        gameObject.tag = "Projectile";

        // der Vektor der Graviation wird festeglegt
        // -1.0 hat sich empirisch als sinnvolle Größe erwiesen
        gravitation.x = 0;
		gravitation.z = 0;
		gravitation.y = -1.0f;

		// time erhält einen Zufallswert um mehr Variation bei der Schussform zu ermöglichen
		time = Random.Range (1.5f, 4.6f);

		Vector3 towardsPlayer = Camera.main.transform.position - transform.position;

		// initations Speed wird berechnet, als Vorlage dient die phsykalische Formel für den waagrechten Wurf
		initSpeed = (towardsPlayer / time) - ((gravitation * time) / 2);


		transform.rotation = Quaternion.LookRotation (towardsPlayer, Vector3.up);

		spawnTime = Time.time;

		PistolTarget pistolTarget = GetComponent<PistolTarget> ();
		if (pistolTarget != null) {
			pistolTarget.OnHit += (hitGameObject => {
				Disappear ();
				ParticleSystem.MainModule particleSystemMain = GetComponent<ParticleSystem> ().main;
				particleSystemMain.simulationSpeed *= 3;
			});
		}


	}

	void Update ()
	{
		//Vector3 direction = transform.TransformDirection (Vector3.forward);


		// kontinuierlich wird die gravitation vom initations Speed Vector abgezogen und mit der deltaTime multipliziert, um unabhängig von der Framrate zu bleiben 
		initSpeed += gravitation * Time.deltaTime;


		transform.position += initSpeed * Time.deltaTime;

		// failsafe: remove projectile 2 minutes after spawn
		// in case the projectile somehow managed to get through the walls
		if (spawnTime + 120 < Time.time) {
			Destroy (gameObject);
		}

	}

	private bool IsValidCollisionTarget (Collider other)
	{
		return other.GetComponent<DroneFiringPoint> () == null
		&& other.GetComponent<DroneRepellingSphere> () == null
		&& other.GetComponent<EnemyProjectileShootgunBehavior> () == null
		&& other.GetComponent<EnemyProjectileParabolaBehavior> () == null
		&& other.GetComponentInParent<DroneBehavior> () == null;

		// should only leave objects that have PlayerHitVolume
		// ...and the good old walls
	}

	private void OnTriggerEnter (Collider other)
	{
		if (other.GetComponent<wall> () == null && other.GetComponent<zone> () == null && other.GetComponent<shoot> () == null) {
			if (other.GetComponent<move> () != null && other.GetComponent<move> ().colliderGros != other || other.GetComponent<move> () == null) {

				if (IsValidCollisionTarget (other)) {
					PlayerHitVolume playerHitVolume = other.GetComponent<PlayerHitVolume> ();
					if (playerHitVolume != null) {
						playerHitVolume.Hit (gameObject);
					}

					if (other.GetComponent<PistolControl> () != null) {
						Vector3 direction = transform.TransformDirection (Vector3.forward);
						Vector3 impactPoint = other.ClosestPoint (transform.position);
						Vector3 normal = (transform.position - impactPoint).normalized;
						Vector3 newDirection = Vector3.Reflect (direction, normal);
						transform.rotation *= Quaternion.FromToRotation (direction, newDirection);
					} else {
						Disappear ();
					}
				}
			}
		}

	}

	public void Disappear ()
	{
		// stop movement
		// speed = 0;

		// don't get hit anymore
		Collider collider = GetComponent<Collider> ();
		if (collider != null) {
			collider.enabled = false;
		}

		// stop emitting new particles
		ParticleSystem particleSystem = GetComponent<ParticleSystem> ();
		particleSystem.Stop (true, ParticleSystemStopBehavior.StopEmitting);

		// destroy self after all already existing particles have disappeared
		float remainingLifetime = particleSystem.main.startLifetime.constantMax;
		Destroy (gameObject, remainingLifetime);
	}

}
