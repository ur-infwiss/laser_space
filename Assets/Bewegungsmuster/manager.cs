﻿using UnityEngine;

public class manager : MonoBehaviour
{
    // prefab ist die Vorlage für den Gegnertyp
    public GameObject prefab;

    //spawnzone ist der Bereich, in dem die Einheiten zufällig erscheinen, dieser kann im Programm geändert werden
    private BoxCollider spawnZone;

    // anzahlFeinde legt fest, wie viele Einheiten bei Start des Spiels spawnen
    public int initialSpawnCount = 0;

    public bool continuousSpawning = false;

    public int maxSpawnTime = 100;
    public int spawnTime = 100;


    // Use this for initialization
    // die Dimensionen der Spawnzone werden ermittelt, dann wird die in anzahlFeinde festgelegte Zahl an Einheiten gleichzeitig gespawnt
    void Start()
    {
        spawnZone = GetComponent<BoxCollider>();

        for (int i = 0; i < initialSpawnCount; i++)
        {
            SpawnPrefab();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (continuousSpawning)
        {
            spawnTime--;
            if (spawnTime < 0)
            {
                SpawnPrefab();
                spawnTime = maxSpawnTime;
            }

        }

    }

    private void SpawnPrefab()
    {
        Vector3 pos = new Vector3(
            Random.Range(spawnZone.bounds.min.x, spawnZone.bounds.max.x),
            Random.Range(spawnZone.bounds.min.y, spawnZone.bounds.max.y),
            Random.Range(spawnZone.bounds.min.z, spawnZone.bounds.max.z));
        Instantiate(prefab, pos, Quaternion.identity);
    }
}
