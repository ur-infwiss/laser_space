﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class move : MonoBehaviour
{
    //public AudioClip spiderDeathSqueel;
    //public AudioClip spiderShootong;

    AudioSource spiderWalking;
    public AudioClip spiderDeath;
    AudioSource spiderShot;

    // prefab der optischen Effekte beim Schießen und Zerstören der Drohnen
    public GameObject explosionPrefab;
	public GameObject muzzleFlashPrefab;
	//Gameobjects, die die verschiedenen Schussverhalten hinterlegt haben
	public GameObject projectilePrefabShotgun;
	public GameObject projectilePrefabParabola;
	//Position des Abschussortes der Gegner
	public Transform muzzleTransform;

	// fireRate legt fest, wann die Einheit feuert,
	// fireInterval ist das Intervall, in welchem gefeuert wird

	private Transform target;
	private float fireRate;
	//int liveTime = 1600;
	public Collider colliderGros;

	//Die Listen für alle Gameobjecte werden initiiert, mit denen kollidiert werden kann
	private List<GameObject> enemies = new List<GameObject> ();
	private List<GameObject> walls = new List<GameObject> ();
	private List<GameObject> zones = new List<GameObject> ();
	private List<GameObject> shootZones = new List<GameObject> ();

	// Variablen für Beschleunigung und Geschwindigkeit
	public Vector3 acc;
	public Vector3 speed;

	// empirisch ermittelte Variablen
	// Abstoßungskraft zwischen den einzelnen Einheiten
	float rep = 0.01f;
	// Abstoßungskraft zwischen Wand und den Einheiten
	float repWall = 0.035f;
	// Anziehungskraft zwischen Wand und den Einheiten
	float adWall = 0.035f;
	// allgemeine Reibung der Bewegung der Einheiten
	float reib = 0.05f;
	// Bewegungsgeschwindigkeit der einheiten in den Bewegungszonen
	float ms = 0.05f;
	// Richtung, in die die Oberseite der Einheit zeigt
	Vector3 upRotation;
	// gibt an, ob sich die Einheit an einer Wand befindet
	bool isOnWall;
	// gibt an, ob sich die Einheit in einer Zone befindet, die das Schießen erlaubt
	bool isInShootZone;

	// manipulierbare Varibalen für die Firerate, Anzahl an Schüssen und Streuung der Shotgun
	public int fireInterval = 1; //changed from 300
	public int minShotgunBullets = 2;
	public int maxShotgunBullets = 7;
	public float maxShotgunScattering = 0.5f;



	// Use this for initialization
	void Start ()
	{
        AudioSource[] audios = GetComponents<AudioSource>();
        spiderWalking = audios[0];
        spiderShot = audios[1];

        gameObject.tag = "Enemy";

		if (muzzleTransform == null) {
			muzzleTransform = transform.Find ("Muzzle");
		}

		// legt einen zufälligen Wert für die aktuelle Firerate Variable fest, damit nicht alle Einheiten zum selben Zeitpunkt schießen 
		fireRate = Random.Range (0, fireInterval);

		// legt die Kamera als das Ziel fest
		target = Camera.main.transform;

	}

	// Funktion, die die Kollisionen mit anderen Collider registiert
	// je nach angehängten Script wird das Object an die entsprechende Liste angehängt
	private void OnTriggerEnter (Collider other)
	{
        
		if (other.GetComponent<move> () != null) {
			enemies.Add (other.gameObject);
		}
		if (other.GetComponent<wall> () != null) {
			walls.Add (other.gameObject);
		}
		if (other.GetComponent<zone> () != null) {
			zones.Add (other.gameObject);
		}
		if (other.GetComponent<shoot> () != null) {
			shootZones.Add (other.gameObject);
		}

	}

	// Funktion, die das Verlassen eines Collider's registiert
	// je nach angehängtem Script wird das Object aus der entsprechenden Liste entfernt
	private void OnTriggerExit (Collider other)
	{
        
		if (other.GetComponent<move> () != null) {
			enemies.Remove (other.gameObject);
		}
		if (other.GetComponent<wall> () != null) {
			walls.Remove (other.gameObject);
		}
		if (other.GetComponent<zone> () != null) {
			zones.Remove (other.gameObject);
		}
		if (other.GetComponent<shoot> () != null) {
			shootZones.Remove (other.gameObject);
		}
	}




	// Update is called once per frame
	void Update ()
	{
        if (gameObject != null) {

			//Die Geschwindigkeit des letzten Frames bleibt bestehen
			// Beschleunigung wird auf 0 zurückgesetzt
			acc = Vector3.zero;

			// boolsche Werte werden auf falsch zurückgesetzt
			isOnWall = false;
			isInShootZone = false;

			// die Richtung für die Oberseite der Einheit wird zurückgesetzt
			upRotation = Vector3.zero;

			//jedes Element der walls Liste wird abgearbeitet
			foreach (GameObject wall in walls) {
				// es wird kontrolliert, ob nicht durch einen Fehler eine Komponente in eine falschen Liste geraten ist
				if (wall.GetComponent<wall> () != null) {

					// es wird die kürzeste Strecke zwischen den Collidern der Einheit und der Wand ermittelt
					Vector3 closPoint = wall.GetComponent<wall>().GetComponent<Collider>().ClosestPoint (this.transform.position);
					Vector3 distance = this.transform.position - closPoint;

					// die absolute Abstoßung von der Wand wird ermittelt
					// wenn der Abstand zur Wand zu klein wäre, wird ein Minimalwert bestimmt
					// ansonsten wird die Abstoßungskraft der Wand durch den Abstand einer Einheit zur Wand geteilt, somit wird die Abstoßungskraft mit zunehmender Nähe immer größer 
					float absRep = repWall / 0.01f;
					if (distance.magnitude > 0.01) {
						absRep = repWall / distance.magnitude;
					}
					// das Vorzeichen der Abstoßkraft wird ermittelt und dann wird die Anziehungskraft der Wand, die sich aus der länge mal der Anziehungskraft berechnet 
					// (um so weiter von der Wand entfernt um so mehr Anziehungskraft) und von der resultierenden Abstoßungskraft abgezogen 
					absRep -= (absRep / Mathf.Abs (absRep)) * adWall * distance.magnitude;

					// die Richtung der Wand wird ermittelt als Vector3 
					Vector3 wallDirection = wall.GetComponent<wall> ().rep ();

					// die Richtung der Oberseite der Einheit wird ermittelt, sie ergibt sich aus der Summe der Richtungen aller Wände,die den Collider der Einheit berühren, wobei jede Wand abhängig ihrer Entfernung eingerechnet wird 
					// das Ergebnis wird zum schluss normalisiert
					upRotation += (wallDirection / distance.magnitude * distance.magnitude);
			
					// die Richtung der Wand wird mit der Abstoßunskraft multipliziert
					//da der entsprechende Vektor nur eine Zahl für die Richtung der Wand beinhaltet, und ansonsten 0 enthält, wird die Abstoßungskraft mit der korrekten Achse multipliziert
					wallDirection *= absRep;
 
					// die resultierende Abstoßungskraft der einzelnen Wand in der richtigen Richtung wird auf den gesammt Beschleunigungsvektor addiert
					// wird die Liste mindestens einmal druchlaufen, wird isOnWall auf true gesetzt, damit wir wissen, dass mindestens ein Wand in Reichweite ist 
					acc += wallDirection;
					isOnWall = true;
                }

			}


			//jedes Element der enemies Liste wird abgearbeitet
			foreach (GameObject enemie in enemies) {
            
				if (enemie != null) {

					// die Distanz zwischen der aktuellen Einheit und allen anderen Einheiten in der Liste wird ermittelt
					//um zu extreme Größen zu vermeiden, werden die Werte in einem Intervall von -1 und 1 ausgeschlossen
					Vector3 distance = this.transform.position - enemie.transform.position;
					if (distance.x > 0 && distance.x < 1) {
						distance.x = 1;
					}
					if (distance.x > -1 && distance.x < 0) {
						distance.x = -1;
					}
					if (distance.y > 0 && distance.y < 1) {
						distance.y = 1;
					}
					if (distance.y > -1 && distance.y < 0) {
						distance.y = -1;
					}
					if (distance.z > 0 && distance.z < 1) {
						distance.z = 1;
					}
					if (distance.z > -1 && distance.z < 0) {
						distance.z = -1;
					}

					// für jede Achse wird der Umkehrwert gebildet, jede Achse muss einzeln abgehandelt werden, weil sonst ein Fehler beim Teilen durch 0 zustande kommen kann
					if (distance.x != 0) {
						distance.x = 1 / distance.x;
					}
					if (distance.y != 0) {
						distance.y = 1 / distance.y;
					}
					if (distance.z != 0) {
						distance.z = 1 / distance.z;
					}

					// die resultierenden Distanzwerte werden mit den Abstoßkräften multipliziert
					// danach wird der resultierende Vektor der einzelnen Abstoßkraft von einer Einheit auf den gesammten Beschleunigungsvektor gerechnet
					Vector3 vec = distance * rep;
					acc += vec;
                }
			}

			//jedes Element der zones Liste wird abgearbeitet
			foreach (GameObject zone in zones) {
				// die Richtung der Bewegungszone wird ermittelt
				Vector3 zoneDirection = zone.GetComponent<zone> ().rep ();

				// die Richtung der Zone wird mit der konstanten Bewegungsgeschwindigkeit in den Zonen multipliziert
				// der resultierende einzelne Bewegungsvektor der Zone wird auf den gesammten Beschleunigungsvektor gerechnet
				zoneDirection *= ms;
				acc += zoneDirection;
                if (!spiderWalking.isPlaying)
                {
                    spiderWalking.Play();
                }
            }

			//jedes Element der shootZones Liste wird abgearbeitet
			foreach (GameObject zone in shootZones) {
				// ist ein Element in der Liste, wird der boolsche Wert auf true gesetzt und die Einheit darf schießen
				isInShootZone = true;
			}

			// ist die Einheit an keiner Wand wird die Beschnleunigung so verändert, das sie konstant nach unten fällt
			if (!isOnWall) {
				acc.x = 0;
				acc.z = 0;
				acc.y -= 0.08f; // zik changed from -0.06f
			}

			// der aktuelle Beschleinigungsvektor wird auf den Geschwindigkeitsvektor addiert
			//danach wird die physiklaische Formel für die Reibung angewendet 
			speed += acc;
			speed -= reib * speed;

			// unterschreitet die Geschwindigkeit einen Minimlawert, wird sie auf 0 reduziert
			if (speed.magnitude < 0.01) {
				speed = Vector3.zero;
			}
        
			// resultierende Bewegung wird auf das Gameobject umgesetzt
			transform.Translate (Time.deltaTime * speed, Space.World);

			// wenn nur noch wenig Bewegung stattfindet, orentiert sich die Einheit statt an der Bewegung an der Position der Kamera
			Vector3 lookDirection = speed.magnitude < 1.0f ?
			target.position - transform.position :
			speed;
			lookDirection.Normalize ();

			upRotation.Normalize ();

			// befindet sich die Einheit an der Wand, wird die korrekte Drehrichtung ermittelt
			if (isOnWall) {

				Vector3 cross1 = Vector3.Cross (upRotation, lookDirection);
				Vector3 cross2 = Vector3.Cross (cross1, upRotation);

				transform.rotation = Quaternion.SlerpUnclamped (transform.rotation, Quaternion.LookRotation (cross2, upRotation), Time.deltaTime);
			}
			
 
			// befindet sich die Einheit in einer der Schusszonen, wird folgender Code ausgeführt
			if (isInShootZone) {
				// sofern die Oberseite der Wand größer als 1 ist , also die Spinnen sich nicht an der Decke befinden
				if (upRotation.y > 0) {
					// die Firerate wird abhängig von deltaTime reduziert, um kontinuierlichzu bleiben 
					fireRate -= 100 * Time.deltaTime;
					if (fireRate <= 0) {

						if (muzzleFlashPrefab != null) {
							Instantiate (muzzleFlashPrefab, muzzleTransform.position, muzzleTransform.rotation);
						}

						if (projectilePrefabParabola != null) {
							//  feuert einen Parabelschuß ab
							//GameObject go = 
                            Instantiate (projectilePrefabParabola, muzzleTransform.position, muzzleTransform.rotation);
                            if (!spiderShot.isPlaying)
                            {
                                spiderShot.Play();
                            }
                        }
						// setzt die Firerate wieder auf den Ursprungswert
						fireRate = fireInterval;
					}

					// falls die Spinnen an der Decke sind
				} else {
					fireRate -= 10 * Time.deltaTime;
					if (fireRate <= 0) {

						if (muzzleFlashPrefab != null) {
							Instantiate (muzzleFlashPrefab, muzzleTransform.position, muzzleTransform.rotation);
                            if (!spiderShot.isPlaying)
                            {
                                spiderShot.Play();
                            }
                        }

						if (projectilePrefabShotgun != null) {

							// es wird eine zufällige Menge an Schüßen initiert , alle bekommen eine zufällige Abweichung übergeben
							for (int i = 0; i < Random.Range (minShotgunBullets, maxShotgunBullets); i++) {
								GameObject go = Instantiate (projectilePrefabShotgun, muzzleTransform.position, muzzleTransform.rotation);
                                EnemyProjectileShootgunBehavior sb = go.GetComponent<EnemyProjectileShootgunBehavior>();
                                if (sb)
                                    sb.Direction (Random.Range (-maxShotgunScattering, maxShotgunScattering), Random.Range (-maxShotgunScattering, maxShotgunScattering), Random.Range (-maxShotgunScattering, maxShotgunScattering));
							}

						}
						fireRate = fireInterval;
					}
				}

			}
		}

	}

	private bool applicationIsQuitting = false;

	private void OnApplicationQuit ()
	{
		applicationIsQuitting = true;
	}

	private void OnDestroy ()
	{
        if (spiderDeath != null)
        {
            AudioSource.PlayClipAtPoint(spiderDeath, transform.position);
        }

        if (!applicationIsQuitting && explosionPrefab != null) {
			Instantiate (explosionPrefab, transform.position, transform.rotation);
		}

        EventSink.Disappear(gameObject);
    }
}
