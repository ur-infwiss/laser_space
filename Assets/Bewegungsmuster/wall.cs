﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wall : MonoBehaviour
{
	// hinterlegt den Collider im Script damit die Projektile darauf zugreifen können
	//public Collider collider;

	//  übergibt die negative Blickrichtung der Wand, damit sie auf die Abstoßkraft gerechnet werden kann
	public Vector3 rep ()
	{
		return -this.transform.forward;
	}
}
