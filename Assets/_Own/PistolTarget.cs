﻿using System.Collections.Generic;
using UnityEngine;

public class PistolTarget : MonoBehaviour
{
    public delegate void PistolHitHandler(GameObject hitGameObject);

    public event PistolHitHandler OnHit;

    // needed by PistolControl
    public SphereCollider sphereCollider;

    public void Start()
    {
        if (sphereCollider == null)
        {
            sphereCollider = transform.GetComponentInChildren<SphereCollider>();
        }
    }

    public void Hit()
    {
        GameObject.FindObjectOfType<Game>()?.OnPistolTargetHit(this);
        GameObject.FindObjectOfType<GameController>()?.OnPistolTargetHit(this);

        if (OnHit != null)
        {
            OnHit(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #region tracking all instances

    public static List<PistolTarget> instances = new List<PistolTarget>();

    private void OnEnable()
    {
        instances.Add(this);
    }

    private void OnDisable()
    {
        instances.Remove(this);
    }

    #endregion

    // Wenn PistolTarget getroffen wird, das aber nicht erkannt wird --> hat eischussloch
    private void LateUpdate()
    {
        if (Time.frameCount % 10 != 0)
        {
            GameObject bulletHole = gameObject.GetComponentInChildren<DecalDestroyer>()?.gameObject;
            if (bulletHole != null)
            {
                if (gameObject.GetComponentInChildren<ShieldBehavior>()?.gameObject == null)
                {
                    Hit();
                }
                else
                {
                    gameObject.GetComponentInChildren<ShieldBehavior>().gameObject.GetComponent<PistolTarget>().Hit();
                }

                Destroy(bulletHole);
                
            }
        }
    }

}
