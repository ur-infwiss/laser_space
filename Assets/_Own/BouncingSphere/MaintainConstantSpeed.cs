﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MaintainConstantSpeed : MonoBehaviour
{
    public float targetSpeed = 1.0f;
    public float effectStrength = 1.0f;

    void Update()
    {
        var rigidBody = GetComponent<Rigidbody>();
        Vector3 velocity = rigidBody.velocity;
        float missingSpeed = targetSpeed - velocity.magnitude;
        Vector3 finalAcceleration = velocity.normalized * missingSpeed * effectStrength;
        rigidBody.AddForce(finalAcceleration, ForceMode.Acceleration);
        Debug.DrawRay(transform.position, finalAcceleration);
    }
}
