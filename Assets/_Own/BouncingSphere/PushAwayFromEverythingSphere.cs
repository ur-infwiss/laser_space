﻿using UnityEngine;

[RequireComponent(typeof(SphereCollider)), RequireComponent(typeof(Rigidbody))]
public class PushAwayFromEverythingSphere : MonoBehaviour
{
    public float bouncingForceMultiplier = 1.0f;

    void Update()
    {
        Vector3 toSurface = DistanceToSurfaceGrid.ComputeVectorToNearestSurface(transform.position);
        float ownRadius = GetComponent<SphereCollider>().radius;
        Vector3 forceDirection = -toSurface.normalized;
        float forceMagnitude = Mathf.Max(0, 1.0f + ownRadius - toSurface.magnitude);
        Vector3 finalAcceleration = forceDirection * forceMagnitude * bouncingForceMultiplier;
        GetComponent<Rigidbody>().AddForce(finalAcceleration, ForceMode.Acceleration);
        Debug.DrawRay(transform.position, finalAcceleration);
    }
}
