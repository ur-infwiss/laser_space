﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.IO;

public class SoundCalculator : MonoBehaviour
{
    // static variables
    private float TRANSITION_TIME = .5f;
    private float GAME_TIME_MAX = 240.00f; // in Seconds
    private float PLAYER_DEATH_MAX = 10.0f;
    private string LOG_PATH = "Assets/_Own/Audio_Log.csv";

    // non static variables
    private float normalizedGameTime = 0f;
    private float threadCountTotal = 0f;
    private float elapsedGameTime;
    private float enemyCountInScene; //Counts Drones and Spiders in Scene with Tag "Enemy"
    private float projectileCountInScene; //Counts all Projectiles in Scene with Tag "Projectiles"
    private int oldLogTime = -1;

    public float valueGameState = 0f;
    private float currentHits = 0f;
    private float valueNormalizedHits = 0f;
    private float valueNormalizedThreads = 0f;

    // Snapshots for the Old Mixer with only three stages
    /*public AudioMixerSnapshot easy;
    public AudioMixerSnapshot middle;
    public AudioMixerSnapshot hard;
    public string gameState = "";*/

    public AudioMixer masterMixer;
    public float[] weights;
    public AudioMixerSnapshot[] linearSnapshots;
    public AudioMixerSnapshot gameOverSnapshot;
    public bool writeLogFile;
    

    // Start is called before the first frame update
    void Start()
    {
        if (writeLogFile)
        {
            File.WriteAllText(LOG_PATH, "Time; Game_State; Thread_Count; Hit_Count");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (writeLogFile)
        {
            WriteLogFile();
        }
        calculateGameVariables(); // all public variables getting calculated
        calculateNormalizedGameTime(); // calculate the elapsed game time between 0 - 1
        calculateCurrentHits();
        calculateLogThreads();

        calculateGameStateVal();

        blendSnapshotsLinear();
    }

    private void calculateLogThreads()
    {
        // Error Handling: No Log10(0). If there are no Threads in the currentGameState -> log10(1) = 0
        // Value ranges from 0.5 to 1.5
        if(threadCountTotal != 0)
        {
            valueNormalizedThreads = normalize(Mathf.Log10(threadCountTotal), 0f, 1.3f);
        } else
        {
            valueNormalizedThreads = Mathf.Log10(threadCountTotal+1);
        }
        
    }

    private void blendSnapshotsLinear()
    {
        weights[0] = 1.0f - valueGameState;
        weights[1] = valueGameState;

        masterMixer.TransitionToSnapshots(linearSnapshots, weights, TRANSITION_TIME);
        
    }

    /*private void blendSnapshotsOld()
    {
        if (gameStateVal >= 0f && gameStateVal < 4f && gameState != "easy")
        {
            easy.TransitionTo(TRANSITION_TIME);
            gameState = "easy";
        }
        else if (gameStateVal >= 4f && gameStateVal < 8f)
        {
            middle.TransitionTo(TRANSITION_TIME);
            gameState = "middle";
        }
        else if (gameStateVal >= 8f)
        {
            hard.TransitionTo(TRANSITION_TIME);
            gameState = "hard";
        }
    } */

    private void calculateCurrentHits()
    {
        GameObject cameraEye = GameObject.Find("Camera (eye)");
        VignetteController vignette = cameraEye.GetComponent<VignetteController>();
        // Get current hits
        currentHits = vignette.currentHits;
        valueNormalizedHits = normalize(currentHits, 0, PLAYER_DEATH_MAX);
    }

    private void calculateGameStateVal()
    {
        // threadCountTotal: 0.0f - 1.2f
        // valueNormalizedHits: 0.0f - 1.0f (1.0f = PLAYER_DEATH_MAX)
        // normalizedGameTime: 0.0f - 1.0f (1.0f = GAME_TIME_MAX)

        valueGameState = normalize((normalizedGameTime + valueNormalizedThreads + valueNormalizedHits), .0f, 3.0f);

        // If the calculated value is >1.0f for some reason (could go to 1.02)
        if (valueGameState > 1.00f)
        {
            valueGameState = 1.00f;
        }
    }

    private void calculateGameVariables()
    {
        enemyCountInScene = GameObject.FindGameObjectsWithTag("Enemy").Length;
        projectileCountInScene = GameObject.FindGameObjectsWithTag("Projectile").Length;
        threadCountTotal = enemyCountInScene + projectileCountInScene;
    }

    private void calculateNormalizedGameTime()
    {
        float gameTimeWorkaround = 0.0f;
        elapsedGameTime = Mathf.Round(Time.realtimeSinceStartup * 100f) / 100f;

        // This if Statement is necessary to normalize the GameTime bc of the need of a min and a max value
        if (elapsedGameTime <= GAME_TIME_MAX)
        {
            gameTimeWorkaround = elapsedGameTime;
        } else
        {
            gameTimeWorkaround = GAME_TIME_MAX;
        }
        // normalizedVal = (x-min)/(max-min) with min = 0
        normalizedGameTime = normalize(gameTimeWorkaround, 0.0f, GAME_TIME_MAX);
    }

    private float normalize(float value, float min, float max)
    {
        float normValue = -1.0f;
        normValue = (value - min) / (max - min);
        return normValue;
    }

    private void WriteLogFile()
    {
        // round float (gametime) to int
        int currentLogTime = Mathf.RoundToInt(elapsedGameTime);

        // prints every second
        if (currentLogTime != oldLogTime)
        {
            oldLogTime = currentLogTime;
            //Write some text to the test.txt file
            StreamWriter writer = new StreamWriter(LOG_PATH, true);
            writer.WriteLine(currentLogTime + "; " + valueGameState + "; " + threadCountTotal + "; " + currentHits);
            writer.Close();
        }
    }
}
