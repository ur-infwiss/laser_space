﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

[RequireComponent(typeof(Text))]
public class GameOverText : MonoBehaviour
{
    private string baseText = "";
    private AudioSource musicSource;
    public AudioSource gameOverSound2D;
	


    void OnEnable()
    {
        Text text = GetComponent<Text>();
        if (baseText.Length == 0)
        {
            baseText = text.text;
        }

        
        Game game = GameObject.FindObjectOfType<Game>();
        GameController gamecontroller = GameObject.FindObjectOfType<GameController>();

        //if (game != null)
        if ((game != null) || (gamecontroller != null))
        {
            gameOverSound2D.Play();
            if (gamecontroller != null)
            {
                string scoreline = "Score: " + GameController.score;
                if (gamecontroller.winner == true) text.text = "YOU WON!\n" + scoreline + "\n" + "Beaten: " + gamecontroller.DestroyedTotal;
                else text.text = baseText + scoreline + "\n" + "Beaten: " + gamecontroller.DestroyedTotal;
            }
            else text.text = baseText + game.DestroyedTotal;
            GameObject music = GameObject.Find("Music");
            musicSource = music.GetComponent<AudioSource>();
            musicSource.Stop();
        }
    }
	

	
}
