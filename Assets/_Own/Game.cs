﻿using System.Collections;
using UnityEngine;
using EasyButtons;

public class Game : MonoBehaviour
{
    [HideInInspector]
    private Spawner[] spawners;

    public int destroyedDrones = 0;
    public int destroyedSpiders = 0;
    public int destroyedProjectiles = 0;
    public int DestroyedTotal { get { return destroyedDrones + destroyedSpiders + destroyedProjectiles; } }

    private int enemiesAlive = 0;
    public int EnemiesAlive { get { return enemiesAlive; } }

    /// <summary>waves start at 1 - 0 means disabled</summary>
    public int currentWave = 0;
    /// <summary>relative to Time.time</summary>
    public float startOfCurrentWave = 0;
    public float DurationOfCurrentWave { get { return currentWave * 1.0f; } }
    public int RemainingDurationOfCurrentWave { get { return (int)Mathf.Max(startOfCurrentWave + DurationOfCurrentWave - Time.time, 0.0f); } }

    public GameObject gameOver = null;

    public void OnPistolTargetHit(PistolTarget pt)
    {
        if (pt.gameObject.GetComponent<DroneBehavior>() != null)
        {
            destroyedDrones += 1;
        }
        else if (pt.gameObject.GetComponent<move>() != null)
        {
            destroyedSpiders += 1;
        }
        else if (pt.gameObject.GetComponent<EnemyProjectileBehavior>() != null
            || pt.gameObject.GetComponent<EnemyProjectileParabolaBehavior>() != null
            || pt.gameObject.GetComponent<EnemyProjectileShootgunBehavior>() != null)
        {
            destroyedProjectiles += 1;
        }

        EventSink.GameStatechange(this);
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public void StartNextWave()
    {
        currentWave++;
        startOfCurrentWave = Time.time;

        for (int i = 0; i < spawners.Length; i++)
        {
            var spawner = spawners[i];
            int maxInstances = ((currentWave - 1) / spawners.Length);
            if ((currentWave - 1) % spawners.Length >= i) maxInstances++;
            spawner.maxInstances = maxInstances;
        }

        EventSink.GameStatechange(this);
    }

    void Start()
    {
        spawners = FindObjectsOfType<Spawner>();
    }

    void Update()
    {
        if (Time.frameCount % 30 == 0)
        {
            {
                int numEnemies = 0;
                foreach (var spawner in spawners)
                {
                    numEnemies += spawner.instances;
                }
                enemiesAlive = numEnemies;
            }

            if (currentWave > 0)
            {
                if (RemainingDurationOfCurrentWave <= 0)
                {
                    foreach (var spawner in spawners)
                    {
                        spawner.maxInstances = 0;
                    }

                    if ((enemiesAlive == 0) && (startOfCurrentWave + 2 * DurationOfCurrentWave < Time.time))
                    {
                        StartNextWave();
                    }
                }
            }

            EventSink.GameStatechange(this);
        }
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public void DestroyAll()
    {
        foreach (var spawner in spawners)
        {
            spawner.DestroyAllChildren();
        }
        EventSink.GameStatechange(this);
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public void GameOver()
    {
        gameOver.SetActive(true);

        foreach (var spawner in spawners)
        {
            spawner.maxInstances = 0;
        }
        DestroyAll();
        currentWave = 0;
        startOfCurrentWave = 0;
        EventSink.GameStatechange(this);
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public void Reset()
    {
        StartCoroutine(ResetImpl());
    }
    private IEnumerator ResetImpl()
    {
        foreach (var spawner in spawners)
        {
            spawner.maxInstances = 0;
        }
        DestroyAll();

        yield return null;

        gameOver.SetActive(false);

        yield return null;

        destroyedDrones = 0;
        destroyedSpiders = 0;
        destroyedProjectiles = 0;
        currentWave = 0;
        startOfCurrentWave = 0;

        GameObject.FindObjectOfType<VignetteController>().currentHits = 1.0f;

        EventSink.GameStatechange(this);
    }
}
