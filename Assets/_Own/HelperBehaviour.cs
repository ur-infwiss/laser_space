﻿/* In diesem Skript werden die Verschiedenen Zustände der Helferdrohne ausgeführt. Die Navigation zum Ziel ist auf HelperNavigation.cs ausgelagert.
 * Die restlichen Verhaltensweisen werden direkt in diesem Skript implementiert
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridMaster;

public class HelperBehaviour : MonoBehaviour
{
    public enum ActionState { moveToTarget, highlightTarget, stayInPlayerVision, rotateTowardsPlayer, idle }; 
    public ActionState actionState;
    HelperNavigation navigation;
    GridBase grid;
    HelperSpeed speed;
    Transform target;
    float distanceToTarget;
    float amplitude;
    float frequency = 0.1f;
    float upFrequency;

    void Start()
    {
        navigation = HelperNavigation.GetInstance();
        grid = GridBase.GetInstance();
        speed = HelperSpeed.GetInstance();
        target = grid.target;
        distanceToTarget = Vector3.Distance(transform.position, target.position);
        navigation.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        switch (actionState)
        {
            case ActionState.moveToTarget:
                {
                    navigation.enabled = true;
                    speed.MoveToTargetSpeed(distanceToTarget);
                    if (target.position != grid.target.position)
                    {
                        speed.Decelerate(speed.currentSpeed / 2);
                        target = grid.target;
                        distanceToTarget = Vector3.Distance(transform.position, target.position);
                        speed.MoveToTargetSpeed(distanceToTarget);
                    }

                    break;
                }
            case ActionState.highlightTarget:
                {
                    navigation.enabled = false;
                    amplitude = transform.GetComponent<SphereCollider>().radius / 2;
                    upFrequency = 0.3f;
                    //Accelerating step by step until maximum idle speed is reached...
                    if (speed.currentSpeed < speed.maxIdleSpeed && target == grid.target)
                    {
                        speed.Accelerate();
                    }
                    Drift(amplitude, frequency, upFrequency); //...while maintaining a sinus movement
                    if (target != grid.target)
                    {
                        OnTargetChange();
                    }
                    break;
                }
            case ActionState.stayInPlayerVision:
                {
                    navigation.enabled = false;
                    Vector3 inCameraView = Camera.main.WorldToViewportPoint(transform.position); //Get Helper position relative to Camera view
                    var cameraScale = Camera.main.WorldToViewportPoint(transform.lossyScale); //Get Helper Scale relative to Camera view
                    float distanceToCamera = Vector3.Distance(transform.position, Camera.main.transform.position); 
                    amplitude = transform.GetComponent<SphereCollider>().radius / 3 * (distanceToCamera * 0.1f); //amplitude of drifting movement depends on the distance to Camera
                    upFrequency = 0.2f;
                    if (target == grid.target)
                    {
                        if (inCameraView.x - cameraScale.x >= 0 && inCameraView.x + cameraScale.x <= 1) //if the Helper is in Camera view, we accelerate the idle movement until max idle speed is reached
                        {
                            inCameraView = Camera.main.WorldToViewportPoint(transform.position);
                            if (speed.currentSpeed < speed.maxIdleSpeed)
                            {
                                speed.Accelerate();
                            }
                        }
                        else
                        {
                            Vector3 targetPosition;

                            if (inCameraView.x - cameraScale.x < 0) //if the helper is to the left of camera view, move towards the left side of camera view
                            {
                                targetPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.2f, 0.5f, 2f));
                            }
                            else if (inCameraView.x + cameraScale.x > 1) //if the helper is to the right of camera view, move towards the right side of camera view
                            {
                                targetPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.8f, 0.5f, 2f));
                            }
                            else
                            {
                                targetPosition = transform.position; //else, stay in current position 
                            }
                            Vector3 dir = targetPosition - transform.position;
                            float distance = Vector3.Distance(targetPosition, transform.position) * 0.2f;
                            transform.Translate(dir.normalized * distance * Time.deltaTime, Space.World);
                        }

                        Drift(amplitude, frequency, upFrequency); //drifting movement during "downtimes"
                    }
                    else if (target != grid.target) //if a new target appears, decelerate to minSpeed, then call actionState moveToTarget
                    {
                        OnTargetChange();
                    }
                    break;
                }

            case ActionState.rotateTowardsPlayer:
                {
                    navigation.enabled = false;
                    Vector3 rotationDir = (Camera.main.transform.position - transform.position).normalized; //Get direction to rotate towards
                    Quaternion rotateToPlayer = Quaternion.LookRotation(rotationDir);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotateToPlayer, Time.deltaTime * (speed.currentSpeed * 3)); //rotate towards player

                    if (Vector3.Distance(rotationDir, transform.forward) < 0.2f && !grid.CheckTargetType()) //if helper is looking at camera and the reached target is not the player, highlight the target
                    {
                        actionState = ActionState.highlightTarget;
                    }
                    if (Vector3.Distance(rotationDir, transform.forward) < 0.2f && grid.CheckTargetType()) //if helper is looking at camera and the reached target is player, stay in player vision
                    {
                        actionState = ActionState.stayInPlayerVision;
                    }
                    break;
                }
            case ActionState.idle:
                {
                    navigation.enabled = false;
                    float distanceToCamera = Vector3.Distance(transform.position, Camera.main.transform.position);
                    amplitude = transform.GetComponent<SphereCollider>().radius / 3 * (distanceToCamera * 0.1f); //the further away we are from the camera, the stronger the movement
                    upFrequency = 0.2f;
                    if (speed.currentSpeed < speed.maxIdleSpeed && target == grid.target) //accelerate idle movement
                    {
                        speed.Accelerate();
                    }
                    Drift(amplitude, frequency, upFrequency);
                    if (target != grid.target) //if a new target appears, decelerate before calling actionState moveTowardsTarget
                    {
                        OnTargetChange();
                    }
                    break;
                }
        }
    }

    void OnTargetChange()
    {
        if (speed.currentSpeed > speed.minSpeed)
        {
            speed.Decelerate(0.05f);
            Drift(amplitude, frequency, upFrequency);
        }
        else
        {
            target = grid.target;
            distanceToTarget = Vector3.Distance(transform.position, grid.target.position);
            actionState = ActionState.moveToTarget;
        }
    }

    void Drift(float amp, float freq, float upFreq)
    {
        int direction = 0; //if direction value is even, Helper will move to right, else the movement direction is left 

        //Quelle der Mathematischen Formel: https://forum.unity.com/threads/sin-movement-on-y-axis.10357/
        float movementYAxis = amp * (Mathf.Sin(2 * Mathf.PI * upFreq * Time.time) - Mathf.Sin(2 * Mathf.PI * upFreq * (Time.time - Time.deltaTime)));
        transform.Translate(transform.up * movementYAxis * speed.currentSpeed, Space.World);
        if (direction % 2 == 0)
        {
            movementYAxis = amp * (Mathf.Sin(2 * Mathf.PI * frequency * Time.time) - Mathf.Sin(2 * Mathf.PI * frequency * (Time.time - Time.deltaTime)));
            transform.Translate(transform.right * movementYAxis * speed.currentSpeed, Space.World);
            direction++;
        }
        else
        {
            movementYAxis = (amp * 2) * (Mathf.Sin(2 * Mathf.PI * frequency * Time.time) - Mathf.Sin(2 * Mathf.PI * frequency * (Time.time - Time.deltaTime)));
            transform.Translate(transform.right * -movementYAxis * speed.currentSpeed, Space.World);
            direction--;
        }
    }
}
