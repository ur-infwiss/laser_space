﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private int SPAWN_TIME = 1;
    public GameObject prefab;

    public long maxInstances = 10;
    public int instances { get { return children.transform.childCount; } }
    public GameObject children;

    private BoxCollider spawnZone;

    //fuer Zugang zu Gamecontroller
    GameObject GameController;
    ShieldManager shieldmanager;

    // Use this for initialization
    void Start()
    {
        children = new GameObject("Spawned Instances");
        children.transform.parent = transform;
        children.transform.position = Vector3.zero;
        children.transform.rotation = Quaternion.identity;

        spawnZone = GetComponent<BoxCollider>();

        StartCoroutine(WaitAndSpawn());

        //fuer Zugang zum GameController
        GameController = GameObject.Find("GameController");
        shieldmanager = GameController.GetComponent<ShieldManager>();
    }

    IEnumerator WaitAndSpawn()
    {
        while (true)
        {
            if (children.transform.childCount < maxInstances)
            {
                SpawnInstance(); 
                SetLayerRecursively(children);

            }

            yield return new WaitForSeconds(SPAWN_TIME);
        }
    }

    public void SpawnInstance()
    {
        Vector3 initialPosition = spawnZone != null ?
            new Vector3(
                Random.Range(spawnZone.bounds.min.x, spawnZone.bounds.max.x),
                Random.Range(spawnZone.bounds.min.y, spawnZone.bounds.max.y),
                Random.Range(spawnZone.bounds.min.z, spawnZone.bounds.max.z)
            ) :
            transform.position;
        if ((prefab.name == "Drone_Shield")| (prefab.name == "Endboss_Drone_Shield")| (prefab.name == "big_Drone_Shield")| (prefab.name == "little_Drone_Shield"))
        {
            List<int> AllNumbersList = new List<int>();
            int number = 100;

            while(number < 1000)
            {
                if (shieldmanager.NumberList.IndexOf(number) < 0)
                {
                    AllNumbersList.Add(number);
                }
                number++;
            }

            //Debug.Log(AllNumbersList);
            //Debug.Log("AllNumbers = "+string.Join("", new List<int>(AllNumbersList).ConvertAll(i => i.ToString()).ToArray()));
            int newnumber = AllNumbersList[Random.Range(0, AllNumbersList.Count)];
            shieldmanager.NumberList.Add(newnumber);
            shieldmanager.NumberListString = string.Join(" ", new List<int>(shieldmanager.NumberList).ConvertAll(i => i.ToString()).ToArray());
            Instantiate(prefab, initialPosition, transform.rotation, children.transform).transform.GetChild(0).GetChild(3).gameObject.name = newnumber.ToString();
            //Debug.Log("NumberList = "+string.Join("", new List<int>(Static_Methods.NumberList).ConvertAll(i => i.ToString()).ToArray()));
        }
        else
        {
            Instantiate(prefab, initialPosition, transform.rotation, children.transform);
        }
        
    }

    //Source: https://forum.unity.com/threads/change-gameobject-layer-at-run-time-wont-apply-to-child.10091/
    public static void SetLayerRecursively(GameObject go)
    {
        foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = 9;
        }
    }

    public void DestroyAllChildren()
    {
        for (int i = children.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(children.transform.GetChild(i).gameObject);
        }
    }
}
