﻿/* Hier wird die Bewegung der Helferdrohne zum Ziel kalkuliert und ausgeführt. Es wird ein A* Algorithmus verwendet (siehe GridBase.cs, Pathfinder.cs, Node.cs)
 * Die Geschwindigkeit ergibt sich durch HelperSpeed.cs. Gegnerischen Drohnen wird ausgewichen. 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridMaster;

[RequireComponent(typeof(SphereCollider))]
public class HelperNavigation : MonoBehaviour
{
    GridBase grid;
    Bounds areaBounds;
    HelperSpeed speed;
    Pathfinding.Pathfinder pathfinder = new Pathfinding.Pathfinder();

    public Vector3 targetPosition;
    Vector3 startingPosition;
    Vector3 originalTarget;
    Vector3 direction;
    float HelperCircumference;

    List<Vector3> path = new List<Vector3>();

    private void Start()
    {
        Invoke("MovementStart", 1);
    }


    void MovementStart()
    {
        //Instantiate the needed scripts
        grid = GridBase.GetInstance();
        areaBounds = BoundsOfScene.GetBoundsOfScene();
        speed = HelperSpeed.GetInstance();

        //get the needed Positions from grid
        targetPosition = grid.target.position;
        startingPosition = transform.position;
        originalTarget = grid.target.position;
        HelperCircumference = GetComponent<SphereCollider>().radius * 2;

        //Get the list of nodes and transform them to a VectorList for further navigation
        List<Node> pathNodes = GetPath(startingPosition, targetPosition);
        path = GetVectorListFromNodes(pathNodes);

    }

    private void Update()
    {
        Vector3 nextTargetPosition;

        if (!Equals(originalTarget, grid.target.position)) //if the target changes, a new path is calculated
        {
            Vector3 newStartingPosition = transform.position;
            originalTarget = grid.target.position;
            float newDistanceToTarget = Vector3.Distance(newStartingPosition, originalTarget);
            List<Node> newPath = GetPath(newStartingPosition, originalTarget);
            path = GetVectorListFromNodes(newPath);
        }

        while (path.Count > 0 && Vector3.Distance(transform.position, path[0]) < HelperCircumference) //while there are still positions in the path left and the current position is reached, 
        {
            path.RemoveAt(0); //remove current position
        }

        if (path.Count > 0)
        {
            startingPosition = transform.position;
            nextTargetPosition = path[0];
            direction = nextTargetPosition - startingPosition; //calculate movement direction

            if (!Equals(direction, Vector3.zero))
            {
                MoveToTargetWhileEvadingDrones();
            }
        }

        else if (path.Count == 0) //In the last step, we switch to the next ActionState
        {

            direction = targetPosition - startingPosition;
            if (Vector3.Distance(startingPosition, targetPosition) > 0.1f) //move towards the endposition
            {
                transform.Translate(direction.normalized * speed.currentSpeed * Time.deltaTime, Space.World);
                startingPosition = transform.position;
            }
            else //once the target position is reached, change the state to rotation towards player
            {
              transform.GetComponent<HelperBehaviour>().actionState = HelperBehaviour.ActionState.rotateTowardsPlayer;
            }
        }
    }


    List<Node> GetPath(Vector3 pathStart, Vector3 pathEnd)
    {
        float targetHeight = grid.target.lossyScale.y / 2;
        if (grid.CheckTargetType()) //if target is camera, the final position is a point in the cameras field of view (depending on the current view of camera)
        {
            pathEnd = Camera.main.ViewportToWorldPoint(new Vector3(0.8f, 0.5f, 2));
        }
        else
        {
            if (pathEnd.y >= areaBounds.max.y / 2) //if the target is in the upper half of the area bounds, the endposition is below the target
            {
                pathEnd.y -= (HelperCircumference + targetHeight);
            }
            else //else, the finalposition is above the target
            {
                pathEnd.y += HelperCircumference + targetHeight;
            }
        }
        Node startNode = grid.GetNodeFromVector3(pathStart);
        Node endNode = grid.GetNodeFromVector3(pathEnd);
        targetPosition = pathEnd;
        endNode.isWalkable = true;
        pathfinder.startPosition = startNode;
        pathfinder.endPosition = endNode;
        List<Node> pathNodes = new List<Node>();
        pathNodes = pathfinder.findPath();
        return pathNodes;
    }

    List<Vector3> GetVectorListFromNodes(List<Node> vectorPath) //nodelist is transformed to vectorlist to make navigation easier
    {
        path.Clear();
        foreach (Node n in vectorPath)
        {
            Vector3 nextWaypoint = new Vector3(n.x, n.y, n.z);
            path.Add(nextWaypoint);
        }
        return path;
    }

    private int DirectionOfObstacles(Vector3 obstaclePosition) //Check where Object is relative to Helperposittion
    {
        int obstacleDirection;
        var positionRelativeToHelper = transform.InverseTransformPoint(obstaclePosition);
        if (positionRelativeToHelper.x < -0.2f)
        {
            return obstacleDirection = 0; // object is left of Helper
        }
        else if (positionRelativeToHelper.x > -0.2f && positionRelativeToHelper.x < 0.2f)
        {
            return obstacleDirection = 1; //object is in front of Helper
        }
        else return obstacleDirection = 2; //object is right of Helper
    }

    private void MoveToTargetWhileEvadingDrones()
    {

        Collider[] hit = Physics.OverlapSphere(transform.position, transform.GetComponent<SphereCollider>().radius, LayerMask.GetMask("Drone Evasion"));
        if (hit.Length == 0) //if there are no obstacles just keep moving
        {
            Quaternion targetRotation = Quaternion.LookRotation(direction);

            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * speed.currentSpeed);
            transform.Translate(direction.normalized * speed.currentSpeed * Time.deltaTime, Space.World);
        }
        else foreach (var c in hit)
            {
                Vector3 obstacles = c.ClosestPointOnBounds(transform.position);

                if (transform.position.x < areaBounds.max.x && transform.position.x > areaBounds.min.x
                && transform.position.y < areaBounds.max.y && transform.position.y > areaBounds.min.y
                    && transform.position.z < areaBounds.max.z && transform.position.z > areaBounds.min.z)
                {
                    if (DirectionOfObstacles(obstacles) == 0) //if obstacle is left of helper, move to right
                    {
                        transform.Translate(Vector3.right * speed.currentSpeed * Time.deltaTime, Space.World);
                    }
                    else if (DirectionOfObstacles(obstacles) == 2) //if obstacle is right, move to the left
                    {
                        transform.Translate(Vector3.left * speed.currentSpeed * Time.deltaTime, Space.World);
                    }
                    else if (DirectionOfObstacles(obstacles) == 1) //if obstacle is right in front, move up
                    {
                        transform.Translate(Vector3.up * speed.currentSpeed * Time.deltaTime, Space.World);
                    }
                }
            }
    }

    public static HelperNavigation instance;
    public static HelperNavigation GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        instance = this;
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < path.Count - 1; i++)
        {
            Gizmos.color = Color.green;
            Debug.DrawLine(path[i], path[i + 1]);
        }
        Gizmos.color = Color.red;
        Debug.DrawRay(transform.position, direction);
    }

}
