﻿using UnityEngine;

[ExecuteInEditMode]
public class BoundsOfScene : MonoBehaviour
{

    private static bool needsUpdate = true;
    public static bool NeedsUpdate { get { return needsUpdate; } }

    private static Bounds boundsOfScene;


    public static Bounds GetBoundsOfScene()
    {
        if (needsUpdate)
        {
            boundsOfScene = ComputeBoundsOfScene();
            needsUpdate = false;
        }

        return boundsOfScene;
    }

    public static void Invalidate()
    {
        needsUpdate = true;
    }

    private static Bounds ComputeBoundsOfScene()
    {
        Bounds result = new Bounds(Vector3.zero, Vector3.zero);

        foreach (Renderer r in GameObject.FindObjectsOfType<Renderer>())
        {
            if (result.size != Vector3.zero)
            {
                result.Encapsulate(r.bounds);
            }
            else
            {
                result = r.bounds;
            }
        }
        return result;
    }

    void OnDrawGizmosSelected()
    {
        Bounds boundsOfScene = GetBoundsOfScene();
        Gizmos.DrawWireCube(boundsOfScene.center, boundsOfScene.size);
    }
}
