﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowIsoLine : MonoBehaviour
{
    public float distanceToSurface = 1.0f;

    void OnDrawGizmosSelected()
    {
        Vector3 oldPosition;
        Vector3 target = Camera.main.transform.position;
        Vector3 position = transform.position;
        int i = 0;
        do
        {
            oldPosition = position;
            position += (target - position).normalized * 0.05f;
            position = DistanceToSurfaceGrid.FindNearestIsoSurface(position, distanceToSurface);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(oldPosition, position);
            Vector3 toSurface = DistanceToSurfaceGrid.ComputeVectorToNearestSurface(position);
            Gizmos.color = Color.magenta;
            Gizmos.DrawRay(position, toSurface);
            i++;
        } while (i < 1000 && (oldPosition - position).magnitude > 0.01f);
    }
}
