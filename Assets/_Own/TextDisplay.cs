﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(FPS))]
[RequireComponent(typeof(Game))]
public class TextDisplay : MonoBehaviour {

    [HideInInspector]
    private Text textComponent;

    [HideInInspector]
    private Spawner[] spawners;

    private FPS fps;
    private Game game;

    private string lastSpeechResult = "";
    private DateTime timestampOfLastSpeechResult = DateTime.Now;

    private void OnEnable()
    {
        SpeechRecognition.OnSpeechResult += OnSpeechResult;
    }

    private void OnDisable()
    {
        SpeechRecognition.OnSpeechResult -= OnSpeechResult;
    }

    void Start()
    {
        textComponent = GetComponent<Text>();
        spawners = FindObjectsOfType<Spawner>();
        fps = GetComponent<FPS>();
        game = GetComponent<Game>();
    }

    private void OnSpeechResult(string text)
    {
        lastSpeechResult = text;
        timestampOfLastSpeechResult = DateTime.Now;
    }

    void Update () {
        if (Time.frameCount % 30 == 0)
        {
            //string line1 = fps.SmoothedFrametimeMS + "ms " + fps.Fps + "FPS";
            string line1 = "Wave: " + game.currentWave;
            string line2;
            if (game.RemainingDurationOfCurrentWave > 0)
            {
                line2 = ">>> " + game.RemainingDurationOfCurrentWave + "s <<<";
            } else
            {
                line2 = "Enemies: " + game.EnemiesAlive;
            }
            //string line3 = "Score: " + game.DestroyedTotal;
            string line3 = timestampOfLastSpeechResult.AddSeconds(5.0) > DateTime.Now ? lastSpeechResult : "";
            textComponent.text = line1 + "\n" + line2 + "\n" + line3;
            //+ "\n" + SystemInfo.graphicsDeviceName;
        }
	}
}
