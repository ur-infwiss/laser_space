﻿using UnityEngine;

public class SpiderKillZone : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<move>() != null)
        {
            Destroy(collision.gameObject);
        }        
    }
}
