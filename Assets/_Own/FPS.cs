﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS : MonoBehaviour {

    public float smoothedFrametime = 10;
    public int SmoothedFrametimeMS { get { return (int)(smoothedFrametime * 1000); } }
    public int Fps { get { return (int)(1f / smoothedFrametime); } }

    private void Start()
    {
        InvokeRepeating("LogFrametime", 1, 0.5f);
    }

    void Update () {
        const float smoothingFactor = 1;
        smoothedFrametime = Mathf.Lerp(smoothedFrametime, Time.deltaTime, 1f - Mathf.Exp(-Time.deltaTime * smoothingFactor));
    }

    private void LogFrametime()
    {
        EventSink.FrameDuration(smoothedFrametime);
    }
}
