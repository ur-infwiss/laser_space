﻿using System.Diagnostics;
using UnityEngine;
using System.Collections.Generic;

using EasyButtons;
using MarchingCubesProject;

public enum MARCHING_MODE { CUBES, TETRAHEDRON };

[ExecuteInEditMode]
public class IsoSurface : MonoBehaviour
{
#if UNITY_EDITOR

    public Material m_material;

    public MARCHING_MODE mode = MARCHING_MODE.CUBES;

    [Range(-2.0f, 2.0f)]
    public float isoValue = 0.2f;

    [Range(0.1f, 0.5f)]
    public float sampleStepSize = 0.15f;

    private Marching marching = null;
    List<GameObject> meshes = new List<GameObject>();
    private bool needsUpdate = false;

    void OnValidate()
    {
        if (marching == null || marching.Surface != isoValue)
        {
            sampleStepSize = 0.5f;
            needsUpdate = true;
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (needsUpdate)
        {
            UpdateMesh();
        }
    }

    void Update()
    {
        if (UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
        {
            needsUpdate = false;
            RemoveMesh();
        }
    }

    [Button]
    public void RemoveMesh()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            GameObject childGameObject = transform.GetChild(i).gameObject;
            if (childGameObject.name.Contains("IsoSurface Mesh"))
            {
                DestroyImmediate(childGameObject);
            }
        }
    }

    [Button]
    public void UpdateMesh()
    {
        needsUpdate = false;

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        //Set the mode used to create the mesh.
        //Cubes is faster and creates less verts, tetrahedrons is slower and creates more verts but better represents the mesh surface.
        if (mode == MARCHING_MODE.TETRAHEDRON)
            marching = new MarchingTertrahedron();
        else
            marching = new MarchingCubes();

        //Surface is the value that represents the surface of mesh
        //For example the perlin noise has a range of -1 to 1 so the mid point is where we want the surface to cut through.
        //The target value does not have to be the mid point it can be any value with in the range.
        marching.Surface = isoValue;

        Bounds boundsOfScene = BoundsOfScene.GetBoundsOfScene();

        //The size of voxel array
        Vector3Int voxelGridSize = new Vector3Int(
            Mathf.RoundToInt(boundsOfScene.size.x / sampleStepSize),
            Mathf.RoundToInt(boundsOfScene.size.y / sampleStepSize),
            Mathf.RoundToInt(boundsOfScene.size.z / sampleStepSize));

        float[] voxels = new float[voxelGridSize.x * voxelGridSize.y * voxelGridSize.z];

        //Vector3 samplingStepSize = DistanceToSurfaceGrid.GridCellSize / voxelSubdivisionFactor;
        Vector3 samplingStepSize = new Vector3(
            boundsOfScene.size.x / voxelGridSize.x,
            boundsOfScene.size.y / voxelGridSize.y,
            boundsOfScene.size.z / voxelGridSize.z);
        Vector3 halfSamplingStepSize = samplingStepSize * 0.5f;
        Vector3 samplingOffset = boundsOfScene.min + halfSamplingStepSize;

        // sample DistanceToSurfaceGrid at regular intervals into the array voxels
        {
            Vector3Int xyz = new Vector3Int();
            for (xyz.x = 0; xyz.x < voxelGridSize.x; xyz.x++)
            {
                for (xyz.y = 0; xyz.y < voxelGridSize.y; xyz.y++)
                {
                    for (xyz.z = 0; xyz.z < voxelGridSize.z; xyz.z++)
                    {
                        int idx = (int)(xyz.x + xyz.y * voxelGridSize.x + xyz.z * voxelGridSize.x * voxelGridSize.y);
                        Vector3 samplingPosition = samplingOffset + Vector3.Scale(samplingStepSize, xyz);
                        voxels[idx] = DistanceToSurfaceGrid.DistanceToNearestSurface(samplingPosition);
                    }
                }
            }
        }

        List<Vector3> verts = new List<Vector3>();
        List<int> indices = new List<int>();

        //The mesh produced is not optimal. There is one vert for each index.
        //Would need to weld vertices for better quality mesh.
        marching.Generate(voxels, voxelGridSize.x, voxelGridSize.y, voxelGridSize.z, verts, indices);

        // the generated vertices are in the range (0,0,0)...voxelGridSize
        // - shift them into the space of the sampled cuboid
        for (int i = 0; i < verts.Count; i++)
        {
            verts[i] = samplingOffset + Vector3.Scale(verts[i], samplingStepSize);
        }

        //A mesh in unity can only be made up of 65000 verts.
        //Need to split the verts between multiple meshes.
        int maxVertsPerMesh = 30000; //must be divisible by 3, ie 3 verts == 1 triangle
        int numMeshes = verts.Count / maxVertsPerMesh + 1;

        RemoveMesh();

        for (int i = 0; i < numMeshes; i++)
        {

            List<Vector3> splitVerts = new List<Vector3>();
            List<int> splitIndices = new List<int>();

            for (int j = 0; j < maxVertsPerMesh; j++)
            {
                int idx = i * maxVertsPerMesh + j;

                if (idx < verts.Count)
                {
                    splitVerts.Add(verts[idx]);
                    splitIndices.Add(j);
                }
            }

            if (splitVerts.Count == 0) continue;

            Mesh mesh = new Mesh();
            mesh.SetVertices(splitVerts);
            mesh.SetTriangles(splitIndices, 0);
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            GameObject go = new GameObject("IsoSurface Mesh " + i);
            go.transform.parent = transform;
            go.AddComponent<MeshFilter>();
            go.AddComponent<MeshRenderer>();
            go.GetComponent<Renderer>().material = m_material;
            go.GetComponent<MeshFilter>().mesh = mesh;

            meshes.Add(go);
        }

        stopWatch.Stop();
        //UnityEngine.Debug.Log("IsoSurface Mesh - update duration: " + stopWatch.Elapsed
        //    + " triangles: " + (verts.Count / 3)
        //    + " meshes: " + numMeshes);
    }
#endif

}
