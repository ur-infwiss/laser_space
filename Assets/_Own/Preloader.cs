﻿using System.Collections;
using UnityEngine;

public class Preloader : MonoBehaviour
{
    public GameObject[] prefabs;

    void Start()
    {
        StartCoroutine(LoadPrefabs());
    }

    IEnumerator LoadPrefabs()
    {
        foreach (var prefab in prefabs)
        {
            yield return new WaitForSeconds(0.1f);
            yield return new WaitWhile(() => BoundsOfScene.NeedsUpdate || DistanceToSurfaceGrid.NeedsUpdate);

            var instance = Instantiate(prefab, transform);
            yield return null;

            Destroy(instance);
            yield return null;
        }
    }
}
