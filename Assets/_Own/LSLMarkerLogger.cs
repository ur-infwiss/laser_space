﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.LSL4Unity.Scripts;

public class LSLMarkerLogger : MonoBehaviour
{
    struct MessagePayload
    {
        public string[] args;
        public Dictionary<string, object> kwargs;
    }

    private LSLMarkerStream lslMarkerStream;

    private void OnEnable()
    {
        lslMarkerStream = GetComponent<LSLMarkerStream>();
        EventSink.lslLogger = this;
    }

    public void LogEvent(Dictionary<string, object> kwargs, [System.Runtime.CompilerServices.CallerMemberName] string callerMemberName = "")
    {
        if (!kwargs.ContainsKey("name"))
        {
            kwargs.Add("name", callerMemberName);
        }
        string kwargsAsJSON = JsonConvert.SerializeObject(kwargs);
        string argsAsJSON = "[]";
        string payload = "{\"args\":" + argsAsJSON + ",\"kwargs\":" + kwargsAsJSON + "}";
        lslMarkerStream.Write(payload);
    }

    private void OnDisable()
    {
        EventSink.lslLogger = null;
    }
}
