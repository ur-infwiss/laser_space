﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.LSL4Unity.Scripts.AbstractInlets;
using LSL;
using System.Linq;

public class LSL_FloatInlet_Test : AFloatInlet
{
    /*
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }*/

      string  lastSample = "";

    protected override void Process(float[] newSample, double timeStamp)
    {
        // just as an example, make a string out of all channel values of this sample
        lastSample = string.Join(" ", newSample.Select(c => c.ToString()).ToArray());

        Debug.Log(
            string.Format("Got {0} samples at {1}", newSample.Length, timeStamp)
            );
    }
}
