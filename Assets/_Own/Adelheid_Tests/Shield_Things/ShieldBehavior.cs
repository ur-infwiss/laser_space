﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
//using System.Linq;

public class ShieldBehavior : MonoBehaviour
{
    public GameObject explosionPrefab;
    public AudioClip soundeffectDestroyed;

    public GameObject showScore;

    //for scoring
    private int scorepoints = 20;

    //public var Zeit = 0.0;
    private bool destroying;
    private float haloeffekt;

    //Fuer Static Methods
    GameObject gameController;
    ShieldManager shieldmanager;

    //Shieldfarbe
    Color oldColor;

    private void Start()
    {
        destroying = false;
        haloeffekt = 0.0f;

        PistolTarget pistolTarget = GetComponent<PistolTarget>();
        if (pistolTarget != null)
        {
            pistolTarget.OnHit += (hitGameObject =>
            {
                StartCoroutine(wasHit());
            });
        }

        //EventSink.Appear(gameObject);
        //Finden des GameControllers
        gameController = GameObject.Find("GameController");
        shieldmanager = gameController.GetComponent<ShieldManager>();

        //For shieldcolor
        MeshRenderer MR = GetComponent<MeshRenderer>();

        oldColor = MR.material.GetColor("_Secondarycolor");


    }

	public void LateUpdate()
	{
		// Rotate number towards player
		
		Vector3 towardsPlayer = Camera.main.transform.position - transform.position;
        Quaternion rotateTo = new Quaternion(0f, 0f, 0f, 0f);
		this.transform.rotation = Quaternion.Euler(0, 0, 0);
		//Quaternion rotateTo = Quaternion.Euler(0, 180, 0);

        rotateTo.SetLookRotation(towardsPlayer, Vector3.up);

        this.transform.rotation = rotateTo * this.transform.rotation;
	}
	

    public IEnumerator wasHit()
    {
        MeshRenderer MR = GetComponent<MeshRenderer>();
        

        //Effekt anstellen
        MR.material.SetFloat("_Enablepulsation", 1);
        MR.material.SetFloat("_Globalopacity", 1);
        MR.material.SetColor("_Secondarycolor", new Color(191f/255f, 117f/255f, 12f/255f, 1f/255f));
        //GetComponent<MeshRenderer>().material.SetFloat("_PulsationSpeed", 10);
        //Warten
        yield return new WaitForSeconds(4);
        //Effekt ausstellen 
        //MR.material.SetColor("_Secondarycolor", new Color(25f/255f, 14f/255f, 125f/255f, 1f/255f));
        MR.material.SetColor("_Secondarycolor", oldColor);
        MR.material.SetFloat("_Globalopacity", 0.6f);
        //MR.material.DisableKeyword("_Enablepulsation");
        MR.material.SetFloat("_Enablepulsation", 0);
    }

    
    public void Destroying()
    {
        MeshRenderer MR = GetComponent<MeshRenderer>();
        //Effekt anstellen
        MR.material.SetFloat("_Enablepulsation", 1);
        MR.material.SetFloat("_Globalopacity", 1);
        MR.material.SetColor("_Secondarycolor", new Color(198f/255f, 17f/255f, 0f/255f, 1f/255f));
        Destroy(gameObject, 2);

    }

    private bool applicationIsQuitting = false;

    private void OnApplicationQuit()
    {
        applicationIsQuitting = true;
    }

    private void OnDestroy()
    {
        if (shieldmanager.NumberList.IndexOf(int.Parse(this.name)) >= 0)
        {
            shieldmanager.NumberList.Remove(int.Parse(this.name));
            shieldmanager.NumberListString = string.Join(" ", new List<int>(shieldmanager.NumberList).ConvertAll(i => i.ToString()).ToArray());
        }

        if (!applicationIsQuitting && explosionPrefab != null)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);

            if (soundeffectDestroyed != null)
            {
                AudioSource.PlayClipAtPoint(soundeffectDestroyed, transform.position);
            }

            if (gameController.GetComponent<EnemyController>().externalDestroy == false)
            {
                gameController.GetComponent<GameController>().IncreaseScore(scorepoints);
                //Score anzeigen
                GameObject scoring = Instantiate(showScore, transform.position, new Quaternion(0f, 0f, 0f, 0f));
                scoring.GetComponent<ScoreBehaviour>().SetScore(scorepoints);
                scoring.transform.LookAt(GameObject.Find("Camera (eye)").transform.position);

            }

            //fuer Logging
            shieldmanager.shieldWasDeleted = true;
        }
        


    }

}
