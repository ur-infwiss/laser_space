﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldManager : MonoBehaviour
{

    //Liste mit vergebenen Zahlen
    public List<int> NumberList = new List<int>();
    public string NumberListString = "";

    [HideInInspector]
    private GameObject ObjectToDelete;

    //fuer Logging nach Schildzerstörung
    public bool shieldWasDeleted = false;

    public void Delete(string ObjectName)
    {
        if (ObjectName != "0")
        {
            Debug.Log("ObjectName:" + ObjectName);

            ObjectToDelete = GameObject.Find(ObjectName);

            if (ObjectToDelete == null) { Debug.Log("Object nicht gefunden"); }
            else
            {
                if (ObjectToDelete.GetComponent<ShieldBehavior>() != null)
                {
                    ObjectToDelete.GetComponent<ShieldBehavior>().Destroying();
                }
                else
                {
                    Destroy(ObjectToDelete);
                }

                Debug.Log("Zerstoert");
            }


            Debug.Log("Ende Delete");

        }

    }

}
