﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBehaviour : MonoBehaviour
{
    [HideInInspector]
    private GameObject textObject;
    [HideInInspector]
    public int scorepoints = 10;
    private Vector3 direction;



    // Start is called before the first frame update
    void Start()
    {
        direction = new Vector3(0f, 1f, 0f);
        textObject = gameObject.transform.Find("Scoretext").gameObject;

        textObject.GetComponent<TextMesh>().text = "+ " + scorepoints;


        Destroy(gameObject, 1.2f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += direction * 0.5f * Time.deltaTime;
        this.transform.localScale *= (1f + 0.1f * Time.deltaTime);

        
    }

    public void SetScore (int points)
    {
        scorepoints = points;
        textObject = gameObject.transform.Find("Scoretext").gameObject;
        textObject.GetComponent<TextMesh>().text = "+ " + scorepoints;
    }

    public void SetPositionAndRotation(Vector3 position)
    {
        transform.position = position;

        Vector3 towardsPlayer = Camera.main.transform.position - transform.position;
        Quaternion rotateTo = new Quaternion(0f, 0f, 0f, 0f);

        rotateTo.SetLookRotation(towardsPlayer, Vector3.up);

        this.transform.rotation = rotateTo * this.transform.rotation;

    }
}
