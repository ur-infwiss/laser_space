﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// fuer Eye Tracking
using ViveSR.anipal.Eye;
// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;
//fuer Validation Bits
using System;

public class EyeDataLSL : MonoBehaviour
{
    // Fuer EyeTracking

    private EyeData_v2 eyeData = new EyeData_v2();
    private bool StartRecord = false;

    // Fuer LSL

    private const string unique_source_id = "eye_data_laser_space";

    //private string unique_source_id;

    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;

    private int channelCount = 42;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private float[] currentSample;

    //public Transform sampleSource;

    public string StreamName = "EyeTrackingViveProLaserSpace";
    public string StreamType = "Unity.SRanipal.Eye.EyeData_v2";

    //public bool StreamRotationAsQuaternion = true;
    //public bool StreamRotationAsEuler = true;
    //public bool StreamPosition = true;

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    //private const double dataRate = 90;
    private const double dataRate = 90;



    // Start is called before the first frame update
    void Start()
    {
        var channelDefinitions = SetupChannels();

        currentSample = new float[channelCount];
        /*
        for(int i = 0; i < currentSample.Length; i++)
        {
            currentSample[i] = -1;
        }*/
        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_float32, unique_source_id);

        // it's not possible to create a XMLElement before and append it.
        liblsl.XMLElement chns = streamInfo.desc().append_child("channels");
        // so this workaround has been introduced.

        foreach (var def in channelDefinitions)
        {
            chns.append_child("channel")
                .append_child_value("label", def.label)
                .append_child_value("unit", def.unit)
                .append_child_value("type", def.type);
        }

        outlet = new liblsl.StreamOutlet(streamInfo);

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (outlet == null)
            return;

        sample();
    }

    private void sample()
    {
        
        ViveSR.Error error = SRanipal_Eye_API.GetEyeData_v2(ref eyeData);
        if (error == ViveSR.Error.WORK)
        {
            StartRecord = true;
        }

        if (StartRecord == true)
        {
            int offset = -1;


            currentSample[++offset] = eyeData.frame_sequence;
            currentSample[++offset] = eyeData.timestamp;

            currentSample[++offset] = eyeData.verbose_data.right.pupil_diameter_mm;
            currentSample[++offset] = eyeData.verbose_data.right.pupil_position_in_sensor_area.x;
            currentSample[++offset] = eyeData.verbose_data.right.pupil_position_in_sensor_area.y;
            currentSample[++offset] = eyeData.verbose_data.right.gaze_origin_mm.x;
            currentSample[++offset] = eyeData.verbose_data.right.gaze_origin_mm.y;
            currentSample[++offset] = eyeData.verbose_data.right.gaze_origin_mm.z;
            currentSample[++offset] = eyeData.verbose_data.right.gaze_direction_normalized.x;
            currentSample[++offset] = eyeData.verbose_data.right.gaze_direction_normalized.y;
            currentSample[++offset] = eyeData.verbose_data.right.gaze_direction_normalized.z;
            currentSample[++offset] = eyeData.verbose_data.right.eye_openness;
            currentSample[++offset] = eyeData.expression_data.right.eye_wide;

            currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(eyeData.verbose_data.right.eye_data_validata_bit_mask),0);
            currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(eyeData.verbose_data.right.eye_data_validata_bit_mask),4);


            currentSample[++offset] = eyeData.verbose_data.left.pupil_diameter_mm;
            currentSample[++offset] = eyeData.verbose_data.left.pupil_position_in_sensor_area.x;
            currentSample[++offset] = eyeData.verbose_data.left.pupil_position_in_sensor_area.y;
            currentSample[++offset] = eyeData.verbose_data.left.gaze_origin_mm.x;
            currentSample[++offset] = eyeData.verbose_data.left.gaze_origin_mm.y;
            currentSample[++offset] = eyeData.verbose_data.left.gaze_origin_mm.z;
            currentSample[++offset] = eyeData.verbose_data.left.gaze_direction_normalized.x;
            currentSample[++offset] = eyeData.verbose_data.left.gaze_direction_normalized.y;
            currentSample[++offset] = eyeData.verbose_data.left.gaze_direction_normalized.z;
            currentSample[++offset] = eyeData.verbose_data.left.eye_openness;
            currentSample[++offset] = eyeData.expression_data.left.eye_wide;

            currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(eyeData.verbose_data.left.eye_data_validata_bit_mask), 0);
            currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(eyeData.verbose_data.left.eye_data_validata_bit_mask), 4);

			
			currentSample[++offset] = eyeData.verbose_data.combined.eye_data.pupil_diameter_mm;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.pupil_position_in_sensor_area.x;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.pupil_position_in_sensor_area.y;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.gaze_origin_mm.x;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.gaze_origin_mm.y;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.gaze_origin_mm.z;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.gaze_direction_normalized.x;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.gaze_direction_normalized.y;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.gaze_direction_normalized.z;
            currentSample[++offset] = eyeData.verbose_data.combined.eye_data.eye_openness;

            currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(eyeData.verbose_data.combined.eye_data.eye_data_validata_bit_mask), 0);
            currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(eyeData.verbose_data.combined.eye_data.eye_data_validata_bit_mask), 4);

			currentSample[++offset] = returnFloat(eyeData.verbose_data.combined.convergence_distance_validity);
            currentSample[++offset] = eyeData.verbose_data.combined.convergence_distance_mm;

			
            outlet.push_sample(currentSample, liblsl.local_clock());

        }

        
    }

	//converting bool to float
	private float returnFloat(bool value)
	{
		if (value == true)
		{
			return 1.0f;
		}
		else
		{
			return 0.0f;
		}
	}
	
    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {

        

        var list = new List<ChannelDefinition>();

        string[] timeInfo = {"frame_sequenz", "timestamp"};
        foreach (var item in timeInfo)
        {
            var definition = new ChannelDefinition();
            definition.label = item;
            definition.unit = "counter";
            definition.type = "time value";
            list.Add(definition);
        }

        string[] eyeName = { "right", "left", "combined" };
        string[] pupilData = { "pupil_diameter", "pupil_position_x", "pupil_position_y"};
        string[] gazeData = { "gaze_origin_x", "gaze_origin_y", "gaze_origin_z", "gaze_direction_x", "gaze_direction_y", "gaze_direction_z" };
        string[] formData = {"eye_openness", "eye_width"};
        string[] validationBits = { "firstBytes", "secondBytes" };
		string[] combinedEyes = {"convergence_distance_validity", "convergence_distance_mm"};

        foreach (string name in eyeName)
        {
            foreach (var item in pupilData)
            {
                var definition = new ChannelDefinition();
                definition.label = item + "_" + name;
                if (item == "pupil_diameter")
                {
                    definition.unit = "mm";
                }
                else
                {
                    definition.unit = "vector part";
                }
                definition.type = "pupil component of " + name + " eye";
                list.Add(definition);
            }

            foreach (var item in gazeData)
            {
                var definition = new ChannelDefinition();
                definition.label = item + "_" + name;
                definition.unit = "vector part";
                definition.type = "gaze component of " + name + " eye";
                list.Add(definition);
            }

            foreach (var item in formData)
            {
				if (item == "eye_width" & name == "combined")
				{
					continue;
				}
                var definition = new ChannelDefinition();
                definition.label = item + "_" + name;
                definition.unit = "0 to 1";
                definition.type = "form component of " + name + " eye";
                list.Add(definition);
            }

            foreach (var item in validationBits)
            {
                var definition = new ChannelDefinition();
                definition.label = item + "_" + name;
                definition.unit = "validation value";
                definition.type = "validation bits of " + name + " eye";
                list.Add(definition);
            }
			
			if (name == "combined")
			{
				foreach (var item in combinedEyes)
				{
					var definition = new ChannelDefinition();
					definition.label = item + "_" + name;
					definition.unit = "combined eyes values";
					definition.type = "combined eyes values";
					list.Add(definition);
				}
			}

        }


        return list;
    }

    #endregion



}
