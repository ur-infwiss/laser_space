﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;

public class PlayerDataLSL : MonoBehaviour
{
    // Fuer LSL

    private const string unique_source_id = "player_data_laser_space";


    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;

    private int channelCount = 0;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private float[] currentSample;

    //public Transform sampleSource;

    public string StreamName = "PlayerDataLaserSpace";
    public string StreamType = "Player_Values";

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    //private const double dataRate = 90;
    private const double dataRate = liblsl.IRREGULAR_RATE;





    // Start is called before the first frame update
    void Start()
    {
        var channelDefinitions = SetupChannels();

        channelCount = channelDefinitions.Count;

        currentSample = new float[channelCount];

        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_int16, unique_source_id);

        // it's not possible to create a XMLElement before and append it.
        liblsl.XMLElement chns = streamInfo.desc().append_child("channels");
        // so this workaround has been introduced.

        foreach (var def in channelDefinitions)
        {
            chns.append_child("channel")
                .append_child_value("label", def.label)
                .append_child_value("unit", def.unit)
                .append_child_value("type", def.type);
        }

        outlet = new liblsl.StreamOutlet(streamInfo);

    }

    int currentShots = 0;
    int currentMunition = 0;

    // Update is called once per frame
    void LateUpdate()
    {
        bool pushed = false;

        if ((Time.frameCount % 10 == 0))
        {
            pushed = true;
            PushPlayerData();
        }

        if ((pushed == false) && ((PistolControl.newShot == true) || (VignetteController.wasHit == true) || (GameController.newSuccess == true)))
        {
            PistolControl.newShot = false;
            VignetteController.wasHit = false;
            GameController.newSuccess = false;
            pushed = true;
            PushPlayerData();
        }
        
    }

    void PushPlayerData()
    {
        int offset = -1;

        //Checken ob 2 oder 1 Pistole vorhanden
        if (!UnityEngine.XR.XRDevice.isPresent)
        {
            GameObject Pistol = GameObject.Find("NoVRPistolSpawner").transform.Find("Pistol(Clone)").gameObject;
            // Schussanzahl
            currentSample[++offset] = Pistol.GetComponent<PistolControl>().firedShots;
            //Munitionsanzahl
            currentSample[++offset] = Mathf.Max(0, (int)Mathf.Floor(Pistol.GetComponent<PistolCharge>().currentShots));
        }
        else
        {   
			if (GameObject.Find("Controller (right)") != null && GameObject.Find("Controller (left)") != null)
			{
				if (GameObject.Find("Controller (right)").transform.Find("Pistol(Clone)").gameObject != null && GameObject.Find("Controller (left)").transform.Find("Pistol(Clone)").gameObject != null)
				{
					GameObject RightHandPistol = GameObject.Find("Controller (right)").transform.Find("Pistol(Clone)").gameObject;
					GameObject LeftHandPistol = GameObject.Find("Controller (left)").transform.Find("Pistol(Clone)").gameObject;
					//Schussanzahl
					currentSample[++offset] = RightHandPistol.GetComponent<PistolControl>().firedShots + LeftHandPistol.GetComponent<PistolControl>().firedShots;
					//Munitionsanzahl
					currentSample[++offset] = Mathf.Max(0, (int)Mathf.Floor(RightHandPistol.GetComponent<PistolCharge>().currentShots));
					currentSample[++offset] = Mathf.Max(0, (int)Mathf.Floor(LeftHandPistol.GetComponent<PistolCharge>().currentShots));
				}
				else
				{
					Debug.LogError("Mindestens 1 Pistole nicht gefunden!");
					currentSample[++offset] = 0;

					currentSample[++offset] = 0;
					currentSample[++offset] = 0;

				}
			} 
			else
			{
				Debug.LogError("Mindestens 1 Kontroller nicht gefunden!");
				currentSample[++offset] = 0;

				currentSample[++offset] = 0;
				currentSample[++offset] = 0;

			}
		}

        //Verwundungsanzahl // Ist Fliesskomma...; Angabe in 1 / 10000 vom GameOverThreashold
        //--> Bei 10000 ist Game Over
        VignetteController vignette = GameObject.Find("Camera (eye)").GetComponent<VignetteController>();
        // Getroffen worden
        currentSample[++offset] = vignette.hitCounterHead;
        currentSample[++offset] = vignette.hitCounterPistol;
        // Aktuelle Verwundung
        currentSample[++offset] = Mathf.RoundToInt(10000 * vignette.currentHits / vignette.gameOverThreshold);

        //Trefferanzahl
        currentSample[++offset] = GameObject.Find("GameController").GetComponent<GameController>().successfullShots;

        outlet.push_sample(currentSample, liblsl.local_clock());

    }

    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {
        
        var list = new List<ChannelDefinition>();

        string[] playerInfo;

        //Checken ob 2 oder 1 Pistole vorhanden
        if (!UnityEngine.XR.XRDevice.isPresent)
        {
            playerInfo = new string[]{ "Schussanzahl", "Munitionsanzahl", "Verwundungsanzahl_Kopf", "Verwundungsanzahl_Pistole", "Aktuelle_Verwundung", "Trefferanzahl" };

        }
        else
            playerInfo = new string[]{ "Schussanzahl", "Munitionsanzahl_rechts", "Munitionsanzahl_links", "Verwundungsanzahl_Kopf", "Verwundungsanzahl_Pistole", "Aktuelle_Verwundung", "Trefferanzahl" };


        foreach (var item in playerInfo)
        {
            var definition = new ChannelDefinition();
            definition.label = item;
            definition.unit = "counter";
            definition.type = "player value";
            list.Add(definition);
        }


        return list;
    }

    #endregion


}
