﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;

public class PistolShots_LSL : MonoBehaviour
{
    // Fuer LSL

    private const string unique_source_id = "PistolShots_LaserSpace";


    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;

    private int channelCount = 0;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private string[] currentSample;

    //public Transform sampleSource;

    public string StreamName = "PistolShots_LaserSpace";
    public string StreamType = "Pistol_Shot_Values";

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    //private const double dataRate = 90;
    private const double dataRate = liblsl.IRREGULAR_RATE;





    // Start is called before the first frame update
    void Start()
    {
        var channelDefinitions = SetupChannels();

        channelCount = channelDefinitions.Count;

        currentSample = new string[channelCount];

        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_string, unique_source_id);

        // it's not possible to create a XMLElement before and append it.
        liblsl.XMLElement chns = streamInfo.desc().append_child("channels");
        // so this workaround has been introduced.

        foreach (var def in channelDefinitions)
        {
            chns.append_child("channel")
                .append_child_value("label", def.label)
                .append_child_value("unit", def.unit)
                .append_child_value("type", def.type);
        }

        outlet = new liblsl.StreamOutlet(streamInfo);

    }




    public void PushPistolShot(GameObject pistol, Vector3 start, Vector3 end, PistolTarget hitTarget, float angleToCenterOfTarget)
    {
		Debug.Log("in pistolshotpush");
        int offset = -1;

		currentSample[++offset] = pistol.name;
		currentSample[++offset] = start.ToString();
		currentSample[++offset] = end.ToString();
		if (hitTarget != null)
		{
			currentSample[++offset] = hitTarget.name;
		}
		else
		{
			currentSample[++offset] = "-1";
		}
		
		currentSample[++offset] = angleToCenterOfTarget.ToString();
		

        outlet.push_sample(currentSample, liblsl.local_clock());

    }

	
    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {
        
        var list = new List<ChannelDefinition>();

        string[] pistolInfo = {"gameObjectName", "start", "ende", "hitTarget", "angleToCenterOfTarget"};



        foreach (var item in pistolInfo)
        {
            var definition = new ChannelDefinition();
            definition.label = item;
            definition.unit = "string";
            definition.type = "pistol shot value";
            list.Add(definition);
        }


        return list;
    }

    #endregion


}
