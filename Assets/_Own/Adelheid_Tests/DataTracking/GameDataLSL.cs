﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;

public class GameDataLSL : MonoBehaviour
{

    // Fuer LSL

    private const string unique_source_id = "game_status_laser_space";

    //private string unique_source_id;

    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;

    private int channelCount = 6;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private float[] currentSample;

    //public Transform sampleSource;

    public string StreamName = "GameStatusLaserSpace";
    public string StreamType = "Unity.GameControllerData";

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    //private const double dataRate = 90;
    private const double dataRate = liblsl.IRREGULAR_RATE;


    // Start is called before the first frame update
    void Start()
    {
        var channelDefinitions = SetupChannels();

        currentSample = new float[channelCount];
        /*
        for(int i = 0; i < currentSample.Length; i++)
        {
            currentSample[i] = -1;
        }*/
        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_int16, unique_source_id);

        // it's not possible to create a XMLElement before and append it.
        liblsl.XMLElement chns = streamInfo.desc().append_child("channels");
        // so this workaround has been introduced.

        foreach (var def in channelDefinitions)
        {
            chns.append_child("channel")
                .append_child_value("label", def.label)
                .append_child_value("unit", def.unit)
                .append_child_value("type", def.type);
        }

        outlet = new liblsl.StreamOutlet(streamInfo);
    }

    public void pushGameStatus(int wavenumber, int timercount, int score, bool tutorialstatus, bool gamestatus, int endgamestatus)
    {
        int offset = -1;

        currentSample[++offset] = wavenumber;
        currentSample[++offset] = timercount;
        currentSample[++offset] = score;
        if (tutorialstatus == true)
        {
            currentSample[++offset] = 1;
        }
        else
        {
            currentSample[++offset] = 0;
        }
        if (gamestatus == true)
        {
            currentSample[++offset] = 1;
        }
        else
        {
            currentSample[++offset] = 0;
        }
        currentSample[++offset] = endgamestatus;

        outlet.push_sample(currentSample, liblsl.local_clock());
    }

    /*
    // Update is called once per frame
    void Update()
    {
        
    }
    */

    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {


        var list = new List<ChannelDefinition>();

        string[] timeInfo = { "wavenumber", "timercount", "score", "tutorialstatus", "gamestatus", "endgamestatus" };
        foreach (var item in timeInfo)
        {
            var definition = new ChannelDefinition();
            definition.label = item;
            definition.unit = "number";
            definition.type = "game status";
            list.Add(definition);
        }


        return list;
    }

    #endregion

}
