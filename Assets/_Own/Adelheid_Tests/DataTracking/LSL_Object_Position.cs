﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LSL_Object_Position : MonoBehaviour
{
    public string typename = "Drones";

    
    // Start is called before the first frame update

    //Erstmaliges Tracken bei der Objecterstellung
    void Start()
    {
        if (gameObject.GetComponent<DroneBehavior>() != null)
        {
            //Bei den Dronen ist Position durch Drones gegeben, aber Rotation durch Kindobject...
            FindObjectsOfType<LSL_Object_PositionManager>()[0].PushObjectDataToStream(transform, transform.Find("PA_Drone"), typename);

        }
        else 
        {
            FindObjectsOfType<LSL_Object_PositionManager>()[0].PushObjectDataToStream(transform, typename);
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Time.frameCount % 10 == 0)
        {
            if (gameObject.GetComponent<DroneBehavior>() != null)
            {
                //Bei den Dronen ist Position durch Drones gegeben, aber Rotation durch Kindobject...
                FindObjectsOfType<LSL_Object_PositionManager>()[0].PushObjectDataToStream(transform, transform.Find("PA_Drone"), typename);

            }
            else
            {
                FindObjectsOfType<LSL_Object_PositionManager>()[0].PushObjectDataToStream(transform, typename);
            }


        }
    }
	
	void OnDestroy()
	{
		if (gameObject.GetComponent<DroneBehavior>() != null)
            {
                //Bei den Dronen ist Position durch Drones gegeben, aber Rotation durch Kindobject...
                FindObjectsOfType<LSL_Object_PositionManager>()[0].PushObjectDataToStream(transform, transform.Find("PA_Drone"), typename);

            }
            else
            {
                FindObjectsOfType<LSL_Object_PositionManager>()[0].PushObjectDataToStream(transform, typename);
            }
	}
	
}
