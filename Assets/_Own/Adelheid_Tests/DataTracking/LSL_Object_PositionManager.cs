﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;

// Idee: Alle Feindströme werden en mal von externem Object generiert und verwaltet

public class LSL_Object_PositionManager : MonoBehaviour
{
    private string[] Streamnames = { "Drones", "SDrones", "LDrones", "LSDrones", "BDrones", "BSDrones", "EDrones", "Spiders", "SSpiders", "LSpiders", "LSSpiders", "BSpiders", "BSSpiders", "ESpiders", "EnemyProjectiles" };
    private Dictionary<string, int> indexOfstream = new Dictionary<string, int>();

    // ! Projectile ergänzen!!! 

    private const string unique_source_id_substring = "object_type_data_laser_space_";

    private string unique_source_id;

    struct LSLStream
    {
        public string name;
        public liblsl.StreamOutlet outlet;
        public liblsl.StreamInfo streamInfo;
    }

    private LSLStream[] streams;

    private int channelCount = 0;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private float[] currentSample;

    //public Transform sampleSource;

    public string StreamName_substring = "_LaserSpace";
    public string StreamType = "Unity.Quaternion";

    public bool StreamRotationAsQuaternion = true;
    public bool StreamRotationAsEuler = true;
    public bool StreamPosition = true;

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    private const double dataRate = liblsl.IRREGULAR_RATE;



    // Start is called before the first frame update
    void Start()
    {
        var channelDefinitions = SetupChannels();

        channelCount = channelDefinitions.Count;

        currentSample = new float[channelCount];

        streams = new LSLStream[Streamnames.Length];

        int counter = 0;

        foreach (string name in Streamnames)
        {
            streams[counter].name = name;
            streams[counter].streamInfo = new liblsl.StreamInfo(name + StreamName_substring, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_float32, unique_source_id_substring + name);

            liblsl.XMLElement chns = streams[counter].streamInfo.desc().append_child("channels");

            foreach (var def in channelDefinitions)
            {
                chns.append_child("channel")
                    .append_child_value("label", def.label)
                    .append_child_value("unit", def.unit)
                    .append_child_value("type", def.type);
            }

            streams[counter].outlet = new liblsl.StreamOutlet(streams[counter].streamInfo);

            indexOfstream.Add(name, counter);

            counter++;
        }


    }

    /*
    // Update is called once per frame
    void Update()
    {
        
    }*/


    public void PushObjectDataToStream(Transform newTransform, string name)
    {


        int index = indexOfstream[name];

        int offset = -1;

        //InstanceID
        currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(newTransform.gameObject.GetInstanceID()), 0);

        if (StreamRotationAsQuaternion)
        {
            var rotation = newTransform.rotation;

            currentSample[++offset] = rotation.x;
            currentSample[++offset] = rotation.y;
            currentSample[++offset] = rotation.z;
            currentSample[++offset] = rotation.w;
        }
        if (StreamRotationAsEuler)
        {
            var rotation = newTransform.rotation.eulerAngles;

            currentSample[++offset] = rotation.x;
            currentSample[++offset] = rotation.y;
            currentSample[++offset] = rotation.z;
        }
        if (StreamPosition)
        {
            var position = newTransform.position;

            currentSample[++offset] = position.x;
            currentSample[++offset] = position.y;
            currentSample[++offset] = position.z;
        }

        streams[index].outlet.push_sample(currentSample, liblsl.local_clock());


    }

    //fuer Drohnen überladene Methode
    public void PushObjectDataToStream(Transform positionTransform, Transform rotationTransform, string name)
    {


        int index = indexOfstream[name];

        int offset = -1;

        //InstanceID
        //currentSample[++offset] = (float)positionTransform.gameObject.GetInstanceID();
		currentSample[++offset] = BitConverter.ToSingle(BitConverter.GetBytes(positionTransform.gameObject.GetInstanceID()), 0);

        if (StreamRotationAsQuaternion)
        {
            var rotation = rotationTransform.rotation;

            currentSample[++offset] = rotation.x;
            currentSample[++offset] = rotation.y;
            currentSample[++offset] = rotation.z;
            currentSample[++offset] = rotation.w;
        }
        if (StreamRotationAsEuler)
        {
            var rotation = rotationTransform.rotation.eulerAngles;

            currentSample[++offset] = rotation.x;
            currentSample[++offset] = rotation.y;
            currentSample[++offset] = rotation.z;
        }
        if (StreamPosition)
        {
            var position = positionTransform.position;

            currentSample[++offset] = position.x;
            currentSample[++offset] = position.y;
            currentSample[++offset] = position.z;
        }

        streams[index].outlet.push_sample(currentSample, liblsl.local_clock());


    }



    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {
        var list = new List<ChannelDefinition>();

        var enemyid = new ChannelDefinition();
        enemyid.label = "ObjectID";
        enemyid.unit = "object identity number";
        enemyid.type = "object ID";
        list.Add(enemyid);

        if (StreamRotationAsQuaternion)
        {
            string[] quatlabels = { "x", "y", "z", "w" };

            foreach (var item in quatlabels)
            {
                var definition = new ChannelDefinition();
                definition.label = item;
                definition.unit = "unit quaternion";
                definition.type = "quaternion component";
                list.Add(definition);
            }
        }

        if (StreamRotationAsEuler)
        {
            string[] eulerLabels = { "x", "y", "z" };

            foreach (var item in eulerLabels)
            {
                var definition = new ChannelDefinition();
                definition.label = item;
                definition.unit = "degree";
                definition.type = "axis angle";
                list.Add(definition);
            }
        }


        if (StreamPosition)
        {
            string[] eulerLabels = { "x", "y", "z" };

            foreach (var item in eulerLabels)
            {
                var definition = new ChannelDefinition();
                definition.label = item;
                definition.unit = "meter";
                definition.type = "position in world space";
                list.Add(definition);
            }
        }

        return list;
    }

    #endregion

}
