﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// fuer Eye Tracking
using ViveSR.anipal.Eye;

using System;
using System.Runtime.InteropServices;

// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;
//fuer Validation Bits
using System;

public class EyeFocusDataLSL : MonoBehaviour
{
    // Fuer EyeTracking

    private EyeData_v2 eyeData = new EyeData_v2();
	private FocusInfo FocusInfo;
    private readonly float MaxDistance = 30;
	private readonly GazeIndex[] GazePriority = new GazeIndex[] { GazeIndex.COMBINE, GazeIndex.LEFT, GazeIndex.RIGHT };

	
    private bool StartRecord = false;

    // Fuer LSL

    private const string unique_source_id = "eye_focus_data_laser_space";

    //private string unique_source_id;

    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;

    private int channelCount = 15;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private string[] currentSample;

    //public Transform sampleSource;

    public string StreamName = "EyeFocusLaserSpace";
    public string StreamType = "Unity.SRanipal.FocusInfo";

    //public bool StreamRotationAsQuaternion = true;
    //public bool StreamRotationAsEuler = true;
    //public bool StreamPosition = true;

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    //private const double dataRate = 90;
    private const double dataRate = 90;



    // Start is called before the first frame update
    void Start()
    {
        var channelDefinitions = SetupChannels();

        currentSample = new string[channelCount];
        /*
        for(int i = 0; i < currentSample.Length; i++)
        {
            currentSample[i] = -1;
        }*/
        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_string, unique_source_id);

        // it's not possible to create a XMLElement before and append it.
        liblsl.XMLElement chns = streamInfo.desc().append_child("channels");
        // so this workaround has been introduced.

        foreach (var def in channelDefinitions)
        {
            chns.append_child("channel")
                .append_child_value("label", def.label)
                .append_child_value("unit", def.unit)
                .append_child_value("type", def.type);
        }

        outlet = new liblsl.StreamOutlet(streamInfo);

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (outlet == null)
            return;

        sample();
    }

    private void sample()
    {
        
        ViveSR.Error error = SRanipal_Eye_API.GetEyeData_v2(ref eyeData);
        if (error == ViveSR.Error.WORK)
        {
            StartRecord = true;
        }

        if (StartRecord == true)
        {
            int offset = -1;

			foreach (GazeIndex index in GazePriority)
            {
                Ray GazeRay;
                int layer_id = LayerMask.NameToLayer("Drone Evasion");
                bool eye_focus;
                    
					
				eye_focus = SRanipal_Eye_v2.Focus(index, out GazeRay, out FocusInfo, 0, MaxDistance, (1 << layer_id), eyeData);

                if (eye_focus)
                {
                    GameObject gameobject = FocusInfo.transform.gameObject;
                    currentSample[++offset] = gameobject.GetInstanceID().ToString();
					currentSample[++offset] = gameobject.name;
					currentSample[++offset] = gameobject.transform.position.x.ToString();
					currentSample[++offset] = gameobject.transform.position.y.ToString();
					currentSample[++offset] = gameobject.transform.position.z.ToString();
                    break;
                }
				else
				{
					currentSample[++offset] = "-1";
					currentSample[++offset] = "-1";
					currentSample[++offset] = "-1";
					currentSample[++offset] = "-1";
					currentSample[++offset] = "-1";
				}
            }

            

            outlet.push_sample(currentSample, liblsl.local_clock());

        }

        
    }

    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {
       

        var list = new List<ChannelDefinition>();

		string[] eyegaze = {"combined", "left", "right"};
        string[] channels = {"objectID", "name", "x", "y", "z"};
		foreach (var gaze in eyegaze)
		{
			foreach (var item in channels)
			{
				var definition = new ChannelDefinition();
				definition.label = item + "_" + gaze;
				definition.unit = "string";
				definition.type = "focus info";
				list.Add(definition);
			}
		}




        return list;
    }

    #endregion



}
