﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;

public class FullScreenTracking : MonoBehaviour
{
    private const string unique_source_id = "fullscreen_color_laser_space";

    //private string unique_source_id;

    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;

    private int channelCount = 5;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private float[] currentSample;

    //public Transform sampleSource;

    public string StreamName = "FullScreenColorLaserSpace";
    public string StreamType = "Unity.Color";

    //public bool StreamRotationAsQuaternion = true;
    //public bool StreamRotationAsEuler = true;
    //public bool StreamPosition = true;

    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    //private const double dataRate = 90;
    private const double dataRate = liblsl.IRREGULAR_RATE;


    // Start is called before the first frame update
    void Start()
    {
        var channelDefinitions = SetupChannels();

        currentSample = new float[channelCount];
        /*
        for(int i = 0; i < currentSample.Length; i++)
        {
            currentSample[i] = -1;
        }*/
        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_float32, unique_source_id);

        // it's not possible to create a XMLElement before and append it.
        liblsl.XMLElement chns = streamInfo.desc().append_child("channels");
        // so this workaround has been introduced.

        foreach (var def in channelDefinitions)
        {
            chns.append_child("channel")
                .append_child_value("label", def.label)
                .append_child_value("unit", def.unit)
                .append_child_value("type", def.type);
        }

        outlet = new liblsl.StreamOutlet(streamInfo);
    }

    public void pushColor(bool enabled, Color newColor)
    {
        int offset = -1;

        if (enabled == true)
            currentSample[++offset] = 1f;
        else
            currentSample[++offset] = 0f;

        currentSample[++offset] = newColor.r;
        currentSample[++offset] = newColor.g;
        currentSample[++offset] = newColor.b;
        currentSample[++offset] = newColor.a;

        outlet.push_sample(currentSample, liblsl.local_clock());

    }

    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {

        //Validata Maske noch unterbringen?

        var list = new List<ChannelDefinition>();

        string[] colorData = { "enabled", "red", "green", "blue", "alpha" };

        foreach (var item in colorData)
        {
            var definition = new ChannelDefinition();
            definition.label = item;
            definition.unit = "0 to 1";
            definition.type = "color value";
            list.Add(definition);
        }


        return list;
    }

    #endregion


}
