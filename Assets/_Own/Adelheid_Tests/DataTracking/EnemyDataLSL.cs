﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// fuer LSL
using Assets.LSL4Unity.Scripts.Common;
using LSL;


public class EnemyDataLSL : MonoBehaviour
{

    // Fuer LSL

    private const string unique_source_id = "enemy_data_laser_space";

    //private string unique_source_id;

    private liblsl.StreamOutlet outlet;
    private liblsl.StreamInfo streamInfo;

    private int channelCount;

    /// <summary>
    /// Use a array to reduce allocation costs
    /// and reuse it for each sampling call
    /// </summary>
    private float[] currentSample;

    //public Transform sampleSource;

    public string StreamName = "EnemyDataLaserSpace";
    public string StreamType = "EnemyData_Numbers";



    /// <summary>
    /// Due to an instable framerate we assume a irregular data rate.
    /// </summary>
    //private const double dataRate = 90;
    private const double dataRate = liblsl.IRREGULAR_RATE;


    //Fuer Zugriff auf Daten
    GameObject gameController;
    ShieldManager shieldmanager;
    EnemyController enemyController;
    GameController controllerscript;


    // Start is called before the first frame update
    void Start()
    {
        //Find Enemiecontroller
        //fuer Zugang zum GameController
        gameController = GameObject.Find("GameController");
        shieldmanager = gameController.GetComponent<ShieldManager>();
        enemyController = gameController.GetComponent<EnemyController>();
        controllerscript = gameController.GetComponent<GameController>();

        var channelDefinitions = SetupChannels();

        channelCount = channelDefinitions.Count;

        currentSample = new float[channelCount];
        
        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, channelCount, dataRate, liblsl.channel_format_t.cf_int16, unique_source_id);

        // it's not possible to create a XMLElement before and append it.
        liblsl.XMLElement chns = streamInfo.desc().append_child("channels");
        // so this workaround has been introduced.

        foreach (var def in channelDefinitions)
        {
            chns.append_child("channel")
                .append_child_value("label", def.label)
                .append_child_value("unit", def.unit)
                .append_child_value("type", def.type);
        }

        outlet = new liblsl.StreamOutlet(streamInfo);

    }

    
    // Update is called once per frame
    void LateUpdate()
    {
        if (shieldmanager.shieldWasDeleted == true || enemyController.enemyWasDeleted == true || enemyController.enemyWasCreated == true || enemyController.projectileChanged == true)
        {
            enemyController.countEnemiesByTypes();
            PushEnemyData();

            shieldmanager.shieldWasDeleted = false;
            enemyController.enemyWasDeleted = false;
            enemyController.enemyWasCreated = false;
            enemyController.projectileChanged = false;
        }
        

    }

    public void PushEnemyData()
    {
        int offset = -1;

        string[] EnemyTypes = { "Drones", "SDrones", "LDrones", "LSDrones", "BDrones", "BSDrones", "EDrones", "Spiders", "SSpiders", "LSpiders", "LSSpiders", "BSpiders", "BSSpiders", "ESpiders" };


        foreach (var item in EnemyTypes)
        {
            currentSample[++offset] = enemyController.currentInstances[item];
        }
        foreach (var item in EnemyTypes)
        {
            currentSample[++offset] = enemyController.beatenInstances[item];
        }

        currentSample[++offset] = controllerscript.enemiesAlive;
        // Das sind nicht die zerstörten Feinde, sondern die, bei denen der Schuss ein Leben gefordert hat.
        currentSample[++offset] = controllerscript.destroyedDrones + controllerscript.destroyedSpiders;

        //Projectile
        currentSample[++offset] = enemyController.currentProjectils["Normal"];
        currentSample[++offset] = enemyController.repelledProjectils["Normal"];

        currentSample[++offset] = shieldmanager.NumberList.Count;

        outlet.push_sample(currentSample, liblsl.local_clock());

    }

    #region workaround for channel creation

    private ICollection<ChannelDefinition> SetupChannels()
    {

        var list = new List<ChannelDefinition>();

        string[] EnemyTypes = { "Drones", "SDrones", "LDrones", "LSDrones", "BDrones", "BSDrones", "EDrones", "Spiders", "SSpiders", "LSpiders", "LSSpiders", "BSpiders", "BSSpiders", "ESpiders"};
        foreach (var item in EnemyTypes)
        {
            var definition = new ChannelDefinition();
            definition.label = item + "_Alive";
            definition.unit = "counter";
            definition.type = "enemies alive value";
            list.Add(definition);
        }
        foreach (var item in EnemyTypes)
        {
            var definition = new ChannelDefinition();
            definition.label = item + "_Beaten";
            definition.unit = "counter";
            definition.type = "enemies beaten value";
            list.Add(definition);
        }

        string[] AllEnemies = { "Alive", "Hits" };

        foreach (var item in AllEnemies)
        {
            var definition = new ChannelDefinition();
            definition.label = "All_Enemies_"+ item;
            definition.unit = "counter";
            definition.type = "all enemies value";
            list.Add(definition);
        }

        string[] NormalProjectilesValues = { "Alive", "Repelled" }; // evtl. kommt hier Parabola hinzu, aber das funktioniert für Spinnen an der Decke sowieso nicht

        foreach (var item in NormalProjectilesValues)
        {
            var definition = new ChannelDefinition();
            definition.label = "Normal_Projectiles_" + item;
            definition.unit = "counter";
            definition.type = "normal projectiles value";
            list.Add(definition);
        }

        //Shieldanzahl
        var shields = new ChannelDefinition();
        shields.label = "Enemies_with_Shield";
        shields.unit = "counter";
        shields.type = "enemy value";
        list.Add(shields);


        return list;
    }

    #endregion


}
