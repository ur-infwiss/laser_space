﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class EnemyController : MonoBehaviour
{
    GameController Gamecontroller;

    //public GameObject gameOver = null;

    [HideInInspector]
    private Script_Spawner[] spawners;
    private int SPAWN_TIME = 1;

    private Dictionary<string, int> maxInstances = new Dictionary<string, int>();

    //Bloss wegen Logging nötig, nicht für eig Spielsteuerung

    public Dictionary<string, int> currentInstances = new Dictionary<string, int>();
    public Dictionary<string, int> beatenInstances = new Dictionary<string, int>();
    //Wie viele Projektile gerade da sind
    public Dictionary<string, int> currentProjectils = new Dictionary<string, int>();
    //Wie viele Projektile absichtlich abgewehrt wurden, d.h. durch Zerschießen oder Wegschlagen
    public Dictionary<string, int> repelledProjectils = new Dictionary<string, int>();

    // zur Unterscheidung ob Zerstörung vom Spieler oder vom Testleiter (per Clear und reset)
    public bool externalDestroy = false;

    // Start is called before the first frame update
    void Start()
    {
        Gamecontroller = gameObject.GetComponent<GameController>();
        spawners = FindObjectsOfType<Script_Spawner>();

        //MaxInstance Felder für Gegnertypen anlegen
        maxInstances.Add("Drones", 0);
        maxInstances.Add("SDrones", 0);
        maxInstances.Add("LDrones", 0);
        maxInstances.Add("LSDrones", 0);
        maxInstances.Add("BDrones", 0);
        maxInstances.Add("BSDrones", 0);
        maxInstances.Add("EDrones", 0);

        maxInstances.Add("Spiders", 0);
        maxInstances.Add("SSpiders", 0);
        maxInstances.Add("LSpiders", 0);
        maxInstances.Add("LSSpiders", 0);
        maxInstances.Add("BSpiders", 0);
        maxInstances.Add("BSSpiders", 0);
        maxInstances.Add("ESpiders", 0);

        //CurrentInstance Felder für Gegnertypen anlegen
        currentInstances.Add("Drones", 0);
        currentInstances.Add("SDrones", 0);
        currentInstances.Add("LDrones", 0);
        currentInstances.Add("LSDrones", 0);
        currentInstances.Add("BDrones", 0);
        currentInstances.Add("BSDrones", 0);
        currentInstances.Add("EDrones", 0);

        currentInstances.Add("Spiders", 0);
        currentInstances.Add("SSpiders", 0);
        currentInstances.Add("LSpiders", 0);
        currentInstances.Add("LSSpiders", 0);
        currentInstances.Add("BSpiders", 0);
        currentInstances.Add("BSSpiders", 0);
        currentInstances.Add("ESpiders", 0);

        //BeatenInstance Felder für Gegnertypen anlegen
        beatenInstances.Add("Drones", 0);
        beatenInstances.Add("SDrones", 0);
        beatenInstances.Add("LDrones", 0);
        beatenInstances.Add("LSDrones", 0);
        beatenInstances.Add("BDrones", 0);
        beatenInstances.Add("BSDrones", 0);
        beatenInstances.Add("EDrones", 0);

        beatenInstances.Add("Spiders", 0);
        beatenInstances.Add("SSpiders", 0);
        beatenInstances.Add("LSpiders", 0);
        beatenInstances.Add("LSSpiders", 0);
        beatenInstances.Add("BSpiders", 0);
        beatenInstances.Add("BSSpiders", 0);
        beatenInstances.Add("ESpiders", 0);

        //Projectile der Gegner mitloggen
        currentProjectils.Add("Normal", 0);
        currentProjectils.Add("Parabola", 0);
        repelledProjectils.Add("Normal", 0);
        repelledProjectils.Add("Parabola", 0);

        StartCoroutine(WaitAndSpawn());

    }

    public void CallOtherFunction(string line)
    {
        //Debug.Log("in CallOtherFunction");
        string[] words = line.Split(' ');

        if (words[0] == "SpawnAlways")
        {
            setMaxInstance(words[1], int.Parse(words[2]));
        }
        else if (words[0] == "SpawnOnce")
        {
            SpawnInstancesOnce(words[1], int.Parse(words[2]));
        }
        else if (words[0] == "Timer")
        {
            //Setzen des Timers
            StartCoroutine(Gamecontroller.TimerCountdown(int.Parse(words[1])));
        }
        else if (words[0] == "Winningcondition")
        {
            
            StartCoroutine(Gamecontroller.checkWinningCondition(int.Parse(words[1])));
        }
        /* Idee wäre abzufragen ob alle Feinde besiegt sind
        else if (words[0] == "EnemiesDestroyedCondition")
        {
            //Setzen des Timers
            StartCoroutine(Gamecontroller.checkEnemies());
        }*/
        else if (words[0] == "ScreenColor")
        {
            Gamecontroller.setScreencolor(words[1], words[2], words[3], words[4]);
        }
        else if (words[0] == "DisableScreenColor")
        {
            Gamecontroller.disableScreencolor();
        }

        else
        {
            Debug.Log("Zeile nicht verstanden: " + line);
        }


    }

    public void setMaxInstance(string name, int number)
    {
        maxInstances[name] = number;
        //Debug.Log("in setMax");
    }

    public void SpawnInstancesOnce(string name, int number)
    {

        //Debug.Log("in Once");
        //Debug.Log(spawners.Length);
        int index;
        
        for (int i = 0; i < number; i++)
        {
            index = Random.Range(0, spawners.Length);
            spawners[index].SpawnInstance(name);
            spawners[index].SetLayerRecursivelyScript();

            currentInstances[name] += 1;
            enemyWasCreated = true;
        }
    }

    public void DestroyAllInstances()
    {
        foreach (Script_Spawner spawner in spawners)
        {
            spawner.DestroyAllChildren();
        }
    }

    //Alle Instanzen zerstören und die Spawnrate zurücksetzen
    public void ResetInstances()
    {
        
        foreach(string key in maxInstances.Keys.ToList<string>())
        {
            maxInstances[key] = 0;
        }

         DestroyAllInstances();
    }

    //Momentane Anzahl aller Feinde 
    public int AllEnemiesCount()
    {
        int enemycount = 0;
        foreach (Script_Spawner spawner in spawners)
        {
            enemycount += spawner.all_instances;
        }
        return enemycount;
    }

    IEnumerator WaitAndSpawn()
    {


        while (true)
        {
            foreach (KeyValuePair<string, int> maxInstance in maxInstances)
            {
                

                //Ermitteln der momentanen Feinde eines Typs
                int currentEnemys = 0;
                foreach (Script_Spawner spawner in spawners)
                {
                    //Enemy enemytype = spawner.transform.Find(maxInstance.Key);
                    spawner.Enemys.ForEach(delegate (Script_Spawner.Enemy enemy)
                    {
                        if (enemy.CompareName(maxInstance.Key) == true)
                        {
                            currentEnemys += enemy.instances;
                        }
                    });
                }


                if (currentEnemys < maxInstance.Value)
                {
                    int index;
                    index = Random.Range(0, spawners.Length);
                    spawners[index].SpawnInstance(maxInstance.Key);
                    spawners[index].SetLayerRecursivelyScript();

                    currentInstances[maxInstance.Key] += 1;
                    enemyWasCreated = true;
                }


            }

            yield return new WaitForSeconds(SPAWN_TIME);
        }
    }



    // zum Loggen
    public bool enemyWasDeleted = false;
    public bool enemyWasCreated = false;
    public bool projectileChanged = false;

    // zum Abfragen der Anzahl der Gegner verschiedener Typen:
    public bool countEnemiesByTypes()
    {
        bool changed = false;

        foreach (KeyValuePair<string, int> currentInstance in new Dictionary<string, int> (currentInstances))
        {

            
            //Ermitteln der momentanen Feinde eines Typs
            int currentEnemys = 0;
            foreach (Script_Spawner spawner in spawners)
            {
                //Enemy enemytype = spawner.transform.Find(maxInstance.Key);
                spawner.Enemys.ForEach(delegate (Script_Spawner.Enemy enemy)
                {
                    if (enemy.CompareName(currentInstance.Key) == true)
                    {
                        currentEnemys += enemy.instances;
                    }
                });
            }


            if (currentEnemys < currentInstance.Value)
            {
                if (externalDestroy == false)
                {
                    beatenInstances[currentInstance.Key] += (currentInstance.Value - currentEnemys);
                }
                externalDestroy = false;
                currentInstances[currentInstance.Key] = currentEnemys;
                changed = true;
            }
            else if (currentEnemys > currentInstance.Value)
            {
                currentInstances[currentInstance.Key] = currentEnemys;
                changed = true;
            }


        }

        return changed;

    }


}
