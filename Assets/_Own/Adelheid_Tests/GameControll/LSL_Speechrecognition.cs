﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.LSL4Unity.Scripts.AbstractInlets;
using LSL;
using System.Linq;

public class LSL_Speechrecognition : AStringInlet
{
    /*
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void AdditionalStart()
    {
        
    }*/
    public string lastSample = "";
    GameObject GameController;
    ShieldManager shieldmanager;
    protected override void Process(string[] newSample, double timeStamp)
    {
        GameController = GameObject.Find("GameController");
        shieldmanager = GameController.GetComponent<ShieldManager>();
        //Debug.Log("Fisch");
        lastSample = string.Join(" ", newSample.Select(c => c.ToString()).ToArray());
        Debug.Log(lastSample);
        //Debug.Log(newSample[0]);
        shieldmanager.Delete(lastSample);
    }

}
