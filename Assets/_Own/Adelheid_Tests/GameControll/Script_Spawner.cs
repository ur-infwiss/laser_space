﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script Spawner soll alle Gegnertypen spawnen können
public class Script_Spawner : MonoBehaviour
{
    private int SPAWN_TIME = 1;

    public class Enemy
    {
        public string name;
        public GameObject prefab;
        public long maxInstances = 0;
        public GameObject children;
        public int instances { get { return children.transform.childCount; } }
        public bool shielding = false;

        public Enemy(string newName, GameObject newPrefab, bool newShielding, Transform newTransform)
        {
            name = newName;
            prefab = newPrefab;
            shielding = newShielding;
            children = new GameObject(name);
            children.transform.parent = newTransform;
            children.transform.position = Vector3.zero;
            children.transform.rotation = Quaternion.identity;
        }

        public bool CompareName(string comparedName)
        {
            if (name == comparedName) return true;
            else return false;
        }

    }

    public List<Enemy> Enemys = new List<Enemy>();

    public int all_instances
    {
        get
        {
            int counter = 0;
            Enemys.ForEach(delegate (Enemy enemy)
            {
                counter += enemy.instances;
            });
            return counter;
        }
    }
    
    //Prefabs für alle Gegnertypen
    public GameObject droneprefab;
    public GameObject shielddroneprefab;
    public GameObject littledroneprefab;
    public GameObject littleshielddroneprefab;
    public GameObject bigdroneprefab;
    public GameObject bigshielddroneprefab;
    public GameObject endbossdroneprefab;
    public GameObject spiderprefab;
    public GameObject shieldspiderprefab;
    public GameObject littlespiderprefab;
    public GameObject littleshieldspiderprefab;
    public GameObject bigspiderprefab;
    public GameObject bigshieldspiderprefab;
    public GameObject endbossspiderprefab;

    //Spawn Zone
    private BoxCollider spawnZone;

    //fuer Zugang zu Gamecontroller
    GameObject GameController;
    ShieldManager shieldmanager;

    // Start is called before the first frame update
    void Start()
    {
        //Zu jedem Prefab Enemyclass erstellen
        Enemys.Add(new Enemy("Drones", droneprefab, false, transform));
        Enemys.Add(new Enemy("SDrones", shielddroneprefab, true, transform));
        Enemys.Add(new Enemy("LDrones", littledroneprefab, false, transform));
        Enemys.Add(new Enemy("LSDrones", littleshielddroneprefab, true, transform));
        Enemys.Add(new Enemy("BDrones", bigdroneprefab, false, transform));
        Enemys.Add(new Enemy("BSDrones", bigshielddroneprefab, true, transform));
        Enemys.Add(new Enemy("EDrones", endbossdroneprefab, true, transform));

        Enemys.Add(new Enemy("Spiders", spiderprefab, false, transform));
        Enemys.Add(new Enemy("SSpiders", shieldspiderprefab, true, transform));
        Enemys.Add(new Enemy("LSpiders", littlespiderprefab, false, transform));
        Enemys.Add(new Enemy("LSSpiders", littleshieldspiderprefab, true, transform));
        Enemys.Add(new Enemy("BSpiders", bigspiderprefab, false, transform));
        Enemys.Add(new Enemy("BSSpiders", bigshieldspiderprefab, true, transform));
        Enemys.Add(new Enemy("ESpiders", endbossspiderprefab, true, transform));

        spawnZone = GetComponent<BoxCollider>();

        //fuer Zugang zum GameController
        GameController = GameObject.Find("GameController");
        shieldmanager = GameController.GetComponent<ShieldManager>();

    }

    public void SpawnInstance(string name)
    {
        //Debug.Log("In SpawnInstance");
        //Position bestimmen
        Vector3 initialPosition = spawnZone != null ?
            new Vector3(
                Random.Range(spawnZone.bounds.min.x, spawnZone.bounds.max.x),
                Random.Range(spawnZone.bounds.min.y, spawnZone.bounds.max.y),
                Random.Range(spawnZone.bounds.min.z, spawnZone.bounds.max.z)
            ) :
            transform.position;

        //Prefab raussuchen und spawnen
        //bool isWantedEnemy = false;
        Enemys.ForEach(delegate (Enemy enemy)
        {
            if (enemy.CompareName(name) == true)
            {
                if (enemy.shielding == false)
                {
                    Instantiate(enemy.prefab, initialPosition, transform.rotation, enemy.children.transform);
                }
                else //Gegner mit Schild
                {
                    List<int> AllNumbersList = new List<int>();
                    int number = 100;

                    while (number < 1000)
                    {
                        if ((shieldmanager.NumberList.IndexOf(number) < 0) & (number % 100 != 0))
                        {
                            AllNumbersList.Add(number);
                        }
                        number++;
                    }

                    //Debug.Log(AllNumbersList);
                    //Debug.Log("AllNumbers = "+string.Join("", new List<int>(AllNumbersList).ConvertAll(i => i.ToString()).ToArray()));
                    int newnumber = AllNumbersList[Random.Range(0, AllNumbersList.Count)];
                    shieldmanager.NumberList.Add(newnumber);
                    shieldmanager.NumberListString = string.Join(" ", new List<int>(shieldmanager.NumberList).ConvertAll(i => i.ToString()).ToArray());
                    Instantiate(enemy.prefab, initialPosition, transform.rotation, enemy.children.transform).transform.GetChild(0).GetChild(2).gameObject.name = newnumber.ToString();
                    //Debug.Log("NumberList = "+string.Join("", new List<int>(Static_Methods.NumberList).ConvertAll(i => i.ToString()).ToArray()));

                }
            }
            
        });


    }

    //Source: https://forum.unity.com/threads/change-gameobject-layer-at-run-time-wont-apply-to-child.10091/
    public void SetLayerRecursivelyScript()
    {
        Enemys.ForEach(delegate (Enemy enemy)
        {
            foreach (Transform trans in enemy.children.GetComponentsInChildren<Transform>(true))
            {
                trans.gameObject.layer = 9;
            }
        });
        /*
        foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = 9;
        }*/
    }

    public void DestroyAllChildren()
    {
        Enemys.ForEach(delegate (Enemy enemy)
        {
            for (int i = enemy.children.transform.childCount - 1; i >= 0; i--)
            {
                Destroy(enemy.children.transform.GetChild(i).gameObject);
            }
        });

        
    }


}
