﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using EasyButtons;

public class GameController : MonoBehaviour
{

    public string GameScript_Path;
    public string TutorialScript_Path;

    //Ungefaehre Spieldauer in min
    public int GameDuration = 15;
    public int TutorialDuration = 3;

    //Timestep sammelt die Anweisungen, die zeitlich auf einmal stattfinden, mit eine, Zeitstempel.
    struct Timestep
    {
        public double deltaTime;
        public List<string> Commandlines;
    }

    private List<Timestep> Gamesteps = new List<Timestep>();
    private List<Timestep> Tutorialsteps = new List<Timestep>();


    //Uebersetzt das Script in eine zeitliche Folge aus Schritten
    private List<Timestep> ReadScript(string ScriptPath, int Duration)
    {
        StreamReader reader = new StreamReader(ScriptPath);
        string TextLine;
        Timestep currentStep = new Timestep();
        currentStep.deltaTime = 0;
        currentStep.Commandlines = new List<string>();
        List<Timestep> Steplist = new List<Timestep>();

        TextLine = reader.ReadLine();
        while (TextLine != null)
        {
            string[] text = TextLine.Split(' ');
            if (text[0] == "//")
            { }
            else if (text[0] == "FixedDelay" | text[0] == "FlexibleDelay")
            {
                if (currentStep.Commandlines != null)
                {
                    Steplist.Add(currentStep);
                    currentStep = new Timestep();
                    if (text[0] == "FixedDelay")
                    {
                        currentStep.deltaTime = int.Parse(text[1]);
                    }
                    else
                    {
                        currentStep.deltaTime = int.Parse(text[1]) * Duration * 60 * 0.01;
                        //Debug.Log(currentStep.deltaTime);
                    }
                    currentStep.Commandlines = new List<string>();
                }
            }
            else
            {
                currentStep.Commandlines.Add(TextLine);
            }
            TextLine = reader.ReadLine();

        }

        Steplist.Add(currentStep);

        return Steplist;

    }

    //fuer Zugriff auf Enemy Controller
    public EnemyController enemycontroller;

    //LSL Ausgabe
    public GameDataLSL lslstream;

    // Start is called before the first frame update
    void Start()
    {
        Gamesteps = ReadScript(GameScript_Path, GameDuration);
        Tutorialsteps = ReadScript(TutorialScript_Path, TutorialDuration);
        enemycontroller = gameObject.GetComponent<EnemyController>();

        lslstream = GetComponent<GameDataLSL>();

    }

    private bool gamestatus = false;
    private bool tutorialstatus = false;

    [Button(ButtonMode.EnabledInPlayMode)]
    public void StartGame()
    {
        if (tutorialstatus == false)
        {
            gamestatus = true;
            writeToLSL();
        }
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public void StartTutorial()
    {
        if (gamestatus == false)
        {
            tutorialstatus = true;
            writeToLSL();
        }
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public void Clear()
    {
        if (enemycontroller == null) { enemycontroller = gameObject.GetComponent<EnemyController>(); }
        //Alle Gegner loeschen
        enemycontroller.DestroyAllInstances();
        enemycontroller.externalDestroy = true;

        EventSink.GameControllerStatechange(this);
        writeToLSL();
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public void Reset()
    {
        if (enemycontroller == null) { enemycontroller = gameObject.GetComponent<EnemyController>(); }

        tutorialstatus = false;
        gamestatus = false;
        timestepcounter = 0;
        //Alle Gegner loeschen; spawnen aufhören
        enemycontroller.ResetInstances();
        enemycontroller.externalDestroy = true;

        score = 0;
        timer = 0;
        wavenumber = 0;
        endgamestatus = 0;

        winner = false;
        gameOver.SetActive(false);
        GameObject.FindObjectOfType<VignetteController>().currentHits = 1.0f;
        //EventSink.GameControllerStatechange(this);

        successfullShots = 0;
        GameObject.FindObjectOfType<VignetteController>().hitCounterHead = 0;
        GameObject.FindObjectOfType<VignetteController>().hitCounterPistol = 0;

        writeToLSL();

    }

    //Timestep counter
    int timestepcounter = 0;
    float lasttime;

    // Update is called once per frame
    void Update()
    {
        if (Time.frameCount % 30 == 0)
        {
            if (gamestatus == true)
            {
                if (timestepcounter < Gamesteps.Count)
                {
                    if (timestepcounter == 0)
                    {
                        InterpretCommandLines(Gamesteps[timestepcounter].Commandlines);
                        timestepcounter++;
                        lasttime = Time.time;
                    }
                    else if (Time.time > (lasttime + Gamesteps[timestepcounter].deltaTime))
                    {
                        InterpretCommandLines(Gamesteps[timestepcounter].Commandlines);
                        timestepcounter++;
                        lasttime = Time.time;
                    }

                }
                else
                {
                    timestepcounter = 0;
                    gamestatus = false;
                    Debug.Log("Ende Game");
                    writeToLSL();
                }
            }
            else if (tutorialstatus == true)
            {
                if (timestepcounter < Tutorialsteps.Count)
                {
                    if (timestepcounter == 0)
                    {
                        InterpretCommandLines(Tutorialsteps[timestepcounter].Commandlines);
                        timestepcounter++;
                        lasttime = Time.time;
                    }
                    else if (Time.time > (lasttime + Tutorialsteps[timestepcounter].deltaTime))
                    {
                        InterpretCommandLines(Tutorialsteps[timestepcounter].Commandlines);
                        timestepcounter++;
                        lasttime = Time.time;
                    }

                }
                else
                {
                    timestepcounter = 0;
                    tutorialstatus = false;
                    Debug.Log("Ende Tutorial");
                    writeToLSL();
                }
            }

            //EventSink.GameControllerStatechange(this);

        }

    }

    void InterpretCommandLines(List<string> commandlines)
    {
        commandlines.ForEach(delegate (string line)
        {
            if (enemycontroller == null) { enemycontroller = gameObject.GetComponent<EnemyController>(); }

            Debug.Log(line);
            Debug.Log(Time.time);
            //CallOtherFunction(line);
            enemycontroller.CallOtherFunction(line);
            //if (enemycontroller == null) Debug.Log("Controller nicht gefunden");
        });
    }

    //Timer und Score
    [HideInInspector]
    public int timer = -1;
    [HideInInspector]
    public static int score = 0;
    [HideInInspector]
    public int wavenumber = 0;

    //Annahme: Timer zählt immer neue Welle ein

    //Coroutine für Timer - Countdown
    public IEnumerator TimerCountdown(int seconds)
    {
        wavenumber++;
        int second = seconds;
        while (second >= -1)
        {
            timer = second;
            second--;
			writeToLSL();
            yield return new WaitForSeconds(1);
        }

        //EventSink.GameControllerStatechange(this);
        writeToLSL();

    }

    //Static, um Zugriff von Gegnern aus zu vereinfachen
    public void IncreaseScore(int points)
    {
        score += points;
        writeToLSL();
    }

    //Game ending
    //Game over
    public GameObject gameOver = null;

    public void GameOver()
    {
        if (enemycontroller == null) { enemycontroller = gameObject.GetComponent<EnemyController>(); }


        endgamestatus = 2;

        gameOver.SetActive(true);

        enemycontroller.ResetInstances();

        EventSink.GameControllerStatechange(this);

        writeToLSL();
    }

    //Winning

    public IEnumerator checkWinningCondition(int restOfTime)
    {
        float time = Time.time;
        float timeUntilEnd = restOfTime;// Spieler darf 60 Sekunden noch weiterspielen

        bool stopWhile = false;

        if (enemycontroller == null) { enemycontroller = gameObject.GetComponent<EnemyController>(); }


        while (stopWhile == false)
        {
            if (enemycontroller.AllEnemiesCount() == 0)
            {
                YouWon();
                stopWhile = true;
            }
            else if (Time.time > time + timeUntilEnd)
            {
                YouWon();
                stopWhile = true;
            }
            yield return new WaitForSeconds(1);
        }
    }

    public bool winner = false;
    
    public void YouWon()
    {
        if (enemycontroller == null) { enemycontroller = gameObject.GetComponent<EnemyController>(); }


        endgamestatus = 1;
        winner = true;
        gameOver.SetActive(true);
        enemycontroller.ResetInstances();
        writeToLSL();
    }

    //for logging: Anzahl an getroffenen Pistol Targets
    public int successfullShots = 0;
    public static bool newSuccess = false;

    //Anzahl besiegter Gegner
    public int destroyedDrones = 0;
    public int destroyedSpiders = 0;
    public int destroyedProjectiles = 0;
    public int DestroyedTotal { get { return destroyedDrones + destroyedSpiders + destroyedProjectiles; } }

    public int enemiesAlive { get
        {
            if (enemycontroller == null) { enemycontroller = gameObject.GetComponent<EnemyController>(); };
            return enemycontroller.AllEnemiesCount();
        } }

    public void OnPistolTargetHit(PistolTarget pt)
    {
        if (pt.gameObject.GetComponent<DroneBehavior>() != null)
        {
            destroyedDrones += 1;
        }
        else if (pt.gameObject.GetComponent<SpiderBehavior>() != null)
        {
            destroyedSpiders += 1;
        }
        else if (pt.gameObject.GetComponent<EnemyProjectileBehavior>() != null
            || pt.gameObject.GetComponent<EnemyProjectileParabolaBehavior>() != null
            || pt.gameObject.GetComponent<EnemyProjectileShootgunBehavior>() != null)
        {
            destroyedProjectiles += 1;
        }

        successfullShots++; //d.h. Schuss hat Pistol Target getroffen
        newSuccess = true;

        EventSink.GameControllerStatechange(this);
    }

    // Screen einfärben
    public void setScreencolor(string red, string green, string blue, string alpha)
    {
        GameObject camera = GameObject.Find("Camera (eye)");
        //if (camera == null) Debug.Log("cameranotfound");
        //Debug.Log(red + " " + float.Parse(red));
        camera.GetComponent<FullScreenColor>().SetRGBA(float.Parse(red), float.Parse(green), float.Parse(blue), float.Parse(alpha));
    }

    public void disableScreencolor()
    {
        GameObject camera = GameObject.Find("Camera (eye)");
        camera.GetComponent<FullScreenColor>().Disable();
    }

    int endgamestatus = 0;
    private void writeToLSL()
    {
        if (lslstream == null) { lslstream = GetComponent<GameDataLSL>(); }
        lslstream.pushGameStatus(wavenumber, timer, score, tutorialstatus, gamestatus, endgamestatus);
    }

}
