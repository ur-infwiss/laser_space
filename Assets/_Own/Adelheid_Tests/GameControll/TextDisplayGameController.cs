﻿
using UnityEngine;
using System;
using UnityEngine.UI;

[RequireComponent(typeof(GameController))]
public class TextDisplayGameController : MonoBehaviour
{

    //Verweis auf Text Componente zum Anzeigen
    public Text textComponent;
    private GameController gamecontroller;
    

    // Start is called before the first frame update
    void Start()
    {
        gamecontroller = GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.frameCount % 30 == 0)
        {
            //string line1 = fps.SmoothedFrametimeMS + "ms " + fps.Fps + "FPS";
            string line1 = "Wave: " + gamecontroller.wavenumber;
            string line2 = "Score: " + GameController.score;
            string line3;
            if(gamecontroller.timer >= 0)
            {
                line3 = ">>> " + gamecontroller.timer + "s <<<";
            }
            else
            {
                line3 = "";
            }
            /*
            string line3;
            if (game.RemainingDurationOfCurrentWave > 0)
            {
                line3 = ">>> " + game.RemainingDurationOfCurrentWave + "s <<<";
            }
            else
            {
                line3 = "Enemies: " + game.EnemiesAlive;
            }*/
            textComponent.text = line1 + "\n" + line2 + "\n" + line3;
            
        }
    }
}
