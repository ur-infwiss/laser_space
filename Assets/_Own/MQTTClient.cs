﻿using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Extensions.ManagedClient;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MQTTClient : MonoBehaviour
{
    struct MessagePayload
    {
        public string[] args;
        public Dictionary<string, object> kwargs;
    }

    private Dictionary<string, WatchedValue> watchedValues = new Dictionary<string, WatchedValue>();

    class WatchedValue
    {
        public GameObject gameObject;
        public Component component;
        public FieldInfo fieldInfo;
        public PropertyInfo propertyInfo;

        private Dictionary<string, object> asDict;

        public void SetValue(object newValue)
        {
            fieldInfo?.SetValue(component, newValue);
            propertyInfo?.SetValue(component, newValue);
        }
        public void Invalidate()
        {
            if (asDict != null)
            {
                asDict["value"] = null;
            }
        }
        public string GetPropertyName()
        {
            if (fieldInfo != null)
            {
                return fieldInfo.Name;
            } else
            {
                return propertyInfo.Name;
            }
        }
        public Type GetPropertyType()
        {
            if (fieldInfo != null)
            {
                return fieldInfo.FieldType;
            }
            else
            {
                return propertyInfo.PropertyType;
            }
        }

        public Dictionary<string, object> getChangeAsDict()
        {
            if (asDict == null)
            {
                asDict = new Dictionary<string, object>
                        {
                            { "name", "GameObjectPropertyValue" },
                            { "gameObjectName", gameObject.name },
                            { "componentType", component.GetType().Name },
                            { "propertyName", GetPropertyName() },
                            { "value", null }
                        };
            }

            object currentValue = GetPropertyValue();
            if (!currentValue.Equals(asDict["value"]))
            {
                asDict["value"] = currentValue;
                return asDict;
            }
            else
            {
                return null;
            }
        }

        private object GetPropertyValue()
        {
            if (fieldInfo != null)
            {
                return fieldInfo.GetValue(component);
            }
            else
            {
                return propertyInfo.GetValue(component);
            }
        }
    }

    private WatchedValue getWatcher(string gameObjectName, string componentType, string propertyName)
    {
        string key = gameObjectName + "." + componentType + "." + propertyName;
        if (!watchedValues.ContainsKey(key))
        {
            WatchedValue watchedValue = new WatchedValue();
            watchedValue.gameObject = GameObject.Find(gameObjectName);
            if (watchedValue.gameObject == null)
            {
                //Debug.Log("Fehler 1: " + gameObjectName);
                return null;
                
            }
            watchedValue.component = watchedValue.gameObject.GetComponent(componentType);
            if (watchedValue.component == null)
            {
                //Debug.Log("Fehler 2: " + watchedValue.gameObject);
                return null;
            }
            watchedValue.fieldInfo = watchedValue.component.GetType().GetRuntimeField(propertyName);
            watchedValue.propertyInfo = watchedValue.component.GetType().GetRuntimeProperty(propertyName);
            if (watchedValue.fieldInfo == null && watchedValue.propertyInfo == null)
            {
                //Debug.Log("Fehler 3: "+ watchedValue.component);
                return null;
            }
            watchedValues[key] = watchedValue;
        }
        //dazu
        //Debug.Log("Key: " + key);

        return watchedValues[key];
        
        
    }



    private IManagedMqttClient mqttClient;
    private string topicEvents = "de/ur/iw/laserspace/events";
    private string topicCommands = "de/ur/iw/laserspace/commands";
    //private string topic = "eu/vtplus/vr/event";

    private void OnEnable()
    {
        Task clientStart = StartClientAsync();
        clientStart.Wait();
    }

    private async Task StartClientAsync()
    {
        // Setup and start a managed MQTT client.
        var options = new ManagedMqttClientOptionsBuilder()
            .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
            .WithClientOptions(new MqttClientOptionsBuilder()
                .WithCleanSession(true)
                .WithClientId("UnityClientLaserSpace")
                //.WithWebSocketServer("test.mosquitto.org:8080")
                //.WithWebSocketServer("localhost:1883")
                //.WithTcpServer("localhost")
                .WithTcpServer("127.0.0.1")
                //.WithWebSocketServer("localhost:8080/ws")
                //.WithTls()
                .Build())
            .Build();

        mqttClient = new MqttFactory().CreateManagedMqttClient();
        mqttClient.Connected += (s, e) =>
        {
            Debug.LogError("Connected: IsSessionPresent: " + e.IsSessionPresent);
            EventSink.mqttClient = this;
            EventSink.Connected();
        };
        mqttClient.ConnectingFailed += (s, e) =>
        {
            Debug.Log("ConnectingFailed: " + e.Exception);
            Debug.LogError("ConnectingFailed: " + e.Exception);
        };
        //mqttClient.ApplicationMessageProcessed += (s, e) =>
        //{
        //    Debug.Log("ApplicationMessageProcessed: " + e);
        //};
        mqttClient.ApplicationMessageReceived += (s, e) =>
        {
            // Debug.Log("Topic: " + e.ApplicationMessage.Topic + " Payload as string: " + e.ApplicationMessage.ConvertPayloadToString() + " + QoS = " + e.ApplicationMessage.QualityOfServiceLevel + " + Retain = " + e.ApplicationMessage.Retain);

            MessagePayload payload = JsonConvert.DeserializeObject<MessagePayload>(e.ApplicationMessage.ConvertPayloadToString());


            //write received Info to LSL
            WriteToLSL(e.ApplicationMessage.ConvertPayloadToString());

            if (e.ApplicationMessage.Topic == topicEvents)
            {
                // Debug.Log("Event: " + payload.kwargs["name"]);
            }
            else if (e.ApplicationMessage.Topic == topicCommands)
            {
                // Debug.Log("Command: " + payload.kwargs["name"]);

                if ((string)payload.kwargs["name"] == "setGameObjectProperty")
                {
                    string gameObjectName = (string)payload.kwargs["gameObjectName"];
                    string componentType = (string)payload.kwargs["componentType"];
                    string propertyName = (string)payload.kwargs["propertyName"];
                    object newValue = payload.kwargs["value"];

                    UnityMainThreadDispatcher.Instance().Enqueue(() => {
                        WatchedValue watchedValue = getWatcher(gameObjectName, componentType, propertyName);
                        if (watchedValue != null)
                        {
                            if (newValue is double && watchedValue.GetPropertyType() == typeof(float))
                            {
                                newValue = (float)(double)newValue;
                            }
                            else if (newValue is long && watchedValue.GetPropertyType() == typeof(int))
                            {
                                newValue = (int)(long)newValue;
                            }
                            

                            watchedValue.SetValue(newValue);
                            // property is now watched, changes will be reported
                            // if the watch is new, a change will certainly be reported
                        } else
                        {
                            Debug.LogError("Failed to get WatchedValue, cause: " + JsonConvert.SerializeObject(payload.kwargs));
                        }
                    });
                }

                if ((string)payload.kwargs["name"] == "invokeGameObjectMethod")
                {
                    string gameObjectName = (string)payload.kwargs["gameObjectName"];
                    string componentType = (string)payload.kwargs["componentType"];
                    string methodName = (string)payload.kwargs["methodName"];
                    Debug.LogError("Methode von: " + gameObjectName);

                    UnityMainThreadDispatcher.Instance().Enqueue(() => {
                        GameObject gameObject = GameObject.Find(gameObjectName);
                        //if (gameObject == null) Debug.Log("Error 1");
                        Component component = gameObject.GetComponent(componentType);
                        //if (component == null) Debug.Log("Error 2");
                        MethodInfo methodInfo = component.GetType().GetMethod(methodName);
                        //if (methodInfo == null) Debug.Log("Error 3");

                        methodInfo.Invoke(component, new object[0]);
                    });
                }



                if ((string)payload.kwargs["name"] == "watchGameObjectProperty")
                {
                    string gameObjectName = (string)payload.kwargs["gameObjectName"];
                    string componentType = (string)payload.kwargs["componentType"];
                    string propertyName = (string)payload.kwargs["propertyName"];

                    UnityMainThreadDispatcher.Instance().Enqueue(() => {
                        WatchedValue watchedValue = getWatcher(gameObjectName, componentType, propertyName);
                        if (watchedValue != null)
                        {
                            watchedValue.Invalidate();
                            // property is now watched, changes will be reported
                            // if the watch is new, a change will certainly be reported
                        }
                        else
                        {
                            Debug.LogError("Failed to get WatchedValue, cause: " + JsonConvert.SerializeObject(payload.kwargs));
                        }
                    });
                }
            }
        };
        mqttClient.SynchronizingSubscriptionsFailed += (s, e) =>
        {
            Debug.Log("SynchronizingSubscriptionsFailed: " + e.Exception);
            Debug.LogError("SynchronizingSubscriptionsFailed: " + e.Exception);
        };
        mqttClient.Disconnected += (s, e) =>
        {
            Debug.Log("Disconnected: ClientWasConnected: " + e.ClientWasConnected + (e.Exception == null ? "" : " Exception: " + e.Exception));
            Debug.LogError("Disconnected: ClientWasConnected: " + e.ClientWasConnected + (e.Exception == null ? "" : " Exception: " + e.Exception));

            EventSink.mqttClient = null;
        };

        //await mqttClient.SubscribeAsync("$SYS/#");
        await mqttClient.SubscribeAsync(new TopicFilterBuilder().WithTopic(topicEvents).WithTopic(topicCommands).Build());
        //await mqttClient.SubscribeAsync(new TopicFilterBuilder().WithTopic(topicCommands).Build());
        //await mqttClient.SubscribeAsync(new TopicFilterBuilder().WithTopic("$SYS/#").Build());

        // StartAsync returns immediately, as it starts a new thread using Task.Run, 
        // and so the calling thread needs to wait.
        await mqttClient.StartAsync(options);

        //await PublishEventAsync("Hallo Welt!");
    }

    void Update()
    {
        if (Time.frameCount % 5 == 0)
        {
            foreach (KeyValuePair<string, WatchedValue> entry in watchedValues)
            {
                Dictionary<string, object> dict = entry.Value.getChangeAsDict();
                if (dict != null)
                {
                    //Debug.Log("Logging change: " + entry.Value.GetPropertyName());
                    LogEvent(dict);
                }
            }
        }
    }

    public void LogEvent(Dictionary<string, object> kwargs, [System.Runtime.CompilerServices.CallerMemberName] string callerMemberName = "")
    {
        if (!kwargs.ContainsKey("name"))
        {
            kwargs.Add("name", callerMemberName);
        }
        string kwargsAsJSON = JsonConvert.SerializeObject(kwargs);
        PublishEventAsync(kwargsAsJSON);
    }

    public Task PublishEventAsync(string kwargsAsJSON, params object[] args)
    {
        string argsAsJSON = JsonConvert.SerializeObject(args);
        if (mqttClient.IsConnected)
        {
            //string payload = "{\"args\":[\"" + messageAsJSON + "\"]}";
            string payload = "{\"args\":" + argsAsJSON + ",\"kwargs\":" + kwargsAsJSON + "}";
            //Debug.Log("publishing: " + payload);
            return mqttClient.PublishAsync(topicEvents, payload);
        }
        else
        {
            return null;
        }
    }

    private void OnDisable()
    {
        Task clientStop = mqttClient.StopAsync();
        clientStop.Wait();
    }

    private void WriteToLSL(string inputstring)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() => {

            GameObject.FindObjectOfType<WebInterfaceInput>().GetComponent<WebInterfaceInput>().pushWebInput(inputstring);

        });

    }
}
