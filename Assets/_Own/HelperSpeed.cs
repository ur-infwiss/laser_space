﻿/* Kontrolliert die Geschwindigkeit der Helferdrohne
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridMaster;

public class HelperSpeed : MonoBehaviour
{

    //accessible & Changeable via Inspector -> This will change how fast the Helper moves/how slow it gets towards the end of each movement
    public float maxSpeed;
    public float minSpeed;
    public float currentSpeed;
    public float maxIdleSpeed = 0.7f; //=the max speed when we are not navigating to a target
    float acceleration;

    GridBase grid;
    HelperNavigation navigation;

    void Start()
    {
        grid = GridBase.GetInstance();
        navigation = HelperNavigation.GetInstance();
    }

    public void Accelerate() //slow acceleration at the start of each Action State
    {
        acceleration = 0.05f;
        currentSpeed += acceleration * Time.deltaTime;
    }

    public void Decelerate(float deceleration) //slow deceleration at the end of each Action State -> This leads to a delay in the execution of next Action State, as Drone has to slow down first. The Delay time can be set in each call of Deceleration() via float value
    {

        currentSpeed -= deceleration * Time.deltaTime;
    }

    public void MoveToTargetSpeed(float startDistance)
    {         float currentDistance = Vector3.Distance(navigation.targetPosition, transform.position);
        if (currentDistance > 5 * (startDistance / 6)) //First, the acceleration is very small, so the transition looks smooth
        {
            Accelerate();
        }
        if (currentDistance <= 5 * (startDistance / 6) && currentDistance > startDistance / 2) //Acceleration is calculated depending on the distance to the midpoint of the way, where we want to achieve maximum speed
        {
            if (currentSpeed <= maxSpeed)
            {
                acceleration = (Mathf.Pow(maxSpeed, 2) - Mathf.Pow(currentSpeed, 2)) / currentDistance;
                currentSpeed += acceleration * Time.deltaTime;
            }
        }
        if (currentDistance < startDistance / 3) //deceleration depends on the distance to the target, where we want to achieve minspeed.
        {
            if (currentSpeed >= minSpeed)
            {
                acceleration = (Mathf.Pow(minSpeed, 2) - Mathf.Pow(currentSpeed, 2)) / currentDistance;
                currentSpeed += acceleration * Time.deltaTime;
            }
        }

    }


    public static HelperSpeed instance;
    public static HelperSpeed GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        instance = this;
    }
}
