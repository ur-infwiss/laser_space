﻿using System.Diagnostics;
using UnityEngine;

public class MosquittoServer : MonoBehaviour
{

    public static Process process;

    private static string platformDependentFolder()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsPlayer:
            case RuntimePlatform.WindowsEditor:
                return "windows-x64";
            default:
                throw new System.Exception("Unexpected Application.platform: " + Application.platform);
        }
    }

    private static string platformDependentBinary()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsPlayer:
            case RuntimePlatform.WindowsEditor:
                return "mosquitto.exe";
            default:
                throw new System.Exception("Unexpected Application.platform: " + Application.platform);
        }
    }

    void OnEnable()
    {
        string folder = Application.streamingAssetsPath + "/mosquitto/" + platformDependentFolder();
        process = new Process();
        process.StartInfo.FileName = folder + "/" + platformDependentBinary();
        process.StartInfo.WorkingDirectory = folder;
        //process.StartInfo.Arguments = "-c ./mosquitto.conf";
        process.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
        //process.StartInfo.RedirectStandardError = true;
        //process.StartInfo.RedirectStandardOutput = true;
        //process.StartInfo.CreateNoWindow = true;
        //process.StartInfo.UseShellExecute = false;
        if (!process.Start())
        {
            UnityEngine.Debug.LogError("Failed to start " + process);
        }

        //string log = process.StandardOutput.ReadToEnd();
        //string errorLog = process.StandardError.ReadToEnd();
    }

    void OnDisable()
    {
        if (!process.HasExited)
        {
            process.Kill();
            process.Close();
        }
    }
}
