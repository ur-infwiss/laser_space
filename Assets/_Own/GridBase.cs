﻿/*A* Algorithmus übernommen und angepasst von https://github.com/sharpaccent/Astar-for-Unity
 * In diesem Skript wird die Grid aufgebaut, auf der sich die Helferdrohne bei der Navigation bewegt.
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridMaster
{
    public class GridBase : MonoBehaviour
    {
        //Setting up the grid
        public int maxGridSizeX;
        public int maxGridSizeY;
        public int maxGridSizeZ;

        int minAreaBoundsX;
        int minAreaBoundsY;
        int minAreaBoundsZ;
        Bounds areaBounds;

        public Node[,,] grid; // our grid

        //public GameObject gridFloorPrefab;

        Vector3 startNodePosition;
        public Transform target;

        public List<Node> p;

        void Start()
        {
            //The typical way to create a grid
            areaBounds = BoundsOfScene.GetBoundsOfScene();

            //maximum size of the grid
            maxGridSizeX = Mathf.RoundToInt(areaBounds.max.x) + Mathf.Abs(Mathf.RoundToInt(areaBounds.min.x));
            maxGridSizeY = Mathf.RoundToInt(areaBounds.max.y) + Mathf.Abs(Mathf.RoundToInt(areaBounds.min.y));
            maxGridSizeZ = Mathf.RoundToInt(areaBounds.max.z) + Mathf.Abs(Mathf.RoundToInt(areaBounds.min.z));

            //minimum of area bounds
            minAreaBoundsX = Mathf.RoundToInt(areaBounds.min.x);
            minAreaBoundsY = Mathf.RoundToInt(areaBounds.min.y);
            minAreaBoundsZ = Mathf.RoundToInt(areaBounds.min.z);

            //The worldposition of each node. These values are stored in the grid 
            int nodePositionX = minAreaBoundsX;
            int nodePositionY = minAreaBoundsY;
            int nodePositionZ = minAreaBoundsZ;

            grid = new Node[maxGridSizeX, maxGridSizeY, maxGridSizeZ];

            for (int x = 0; x < maxGridSizeX; x++)
            {
                for (int y = 0; y < maxGridSizeY; y++)
                {
                    for (int z = 0; z < maxGridSizeZ; z++)
                    {
                        /* For testing, Nodes can be instantiated as Worldobjects -> Makes them visible and creates a list of nodes under the parent object
                         * 
                         * 
                        GameObject go = Instantiate(gridFloorPrefab, new Vector3(nodePositionX, nodePositionY, nodePositionZ),
                            Quaternion.identity) as GameObject;
                        go.transform.name = nodePositionX.ToString() + " " + nodePositionY.ToString() + " " + nodePositionZ.ToString();
                        go.transform.parent = transform;
                        */                       

                        //Create a new node and update it's values
                        Node node = new Node();
                        node.x = nodePositionX;
                        node.y = nodePositionY;
                        node.z = nodePositionZ;
                        //node.worldObject = go;

                        //then place it to the grid
                        grid[x, y, z] = node; //world positions of node are stored in the corresponding point in the grid i.e. World position minX,minY,minZ is stored at [0,0,0]

                        if (Physics.CheckSphere(new Vector3(nodePositionX, nodePositionY, nodePositionZ), 1, DistanceToSurfaceGrid.GetLayerMask))
                        {
                            node.isWalkable = false; //Check for static obstacles within grid. If there is an obstacle (wall) the node is set as not walkable
                        }

                        if (Vector3.Distance(new Vector3(nodePositionX, nodePositionY, nodePositionZ), Camera.main.transform.position) < 0.5f)
                            node.isWalkable = false; //keeping a certain distance to the Camera

                        //node.worldObject.SetActive(false);
                        nodePositionZ++; //next z position
                    }
                    nodePositionY++; //next y position
                    nodePositionZ = minAreaBoundsZ; //z position is set back to startvalue
                }
                nodePositionX++; //next x position
                nodePositionY = minAreaBoundsY; //y position is set back to startvalue
                //once we have gone through all x positions, the loop is done
            }
        }

        public Node GetNode(int x, int y, int z)
        {

            //Used to get the corresponding grid positions to worldpositions
            //If it's greater than all the maximum values we have or smaller than the minAreaBounds
            //then it's going to return null
            int maxPosX = Mathf.RoundToInt(areaBounds.max.x);
            int maxPosY = Mathf.RoundToInt(areaBounds.max.y);
            int maxPosZ = Mathf.RoundToInt(areaBounds.max.z);

            Node nodePositionOnGrid = null;

            if (x < maxPosX && x >= minAreaBoundsX &&
            y < maxPosY && y >= minAreaBoundsY &&
                z < maxPosZ && z >= minAreaBoundsZ)
            {

                nodePositionOnGrid = grid[x + Mathf.Abs(minAreaBoundsX), y + Mathf.Abs(minAreaBoundsY), z + Mathf.Abs(minAreaBoundsZ)];

            }
            return nodePositionOnGrid;
        }

        public Node GetNodeFromVector3(Vector3 pos) //Returns Vector3 as a node
        {
            int x = Mathf.RoundToInt(pos.x);
            int y = Mathf.RoundToInt(pos.y);
            int z = Mathf.RoundToInt(pos.z);

            Node nodeOnGrid = GetNode(x, y, z);
            return nodeOnGrid;
        }

        public bool CheckTargetType() //check if the target is the camera
        {
            if (target.tag == "MainCamera")
            {
                return true;
            }
            return false;
        }


        public static GridBase instance;
        public static GridBase GetInstance()
        {
            return instance;
        }

        void Awake()
        {
            instance = this;
        }
    }
}
