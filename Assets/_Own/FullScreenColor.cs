﻿using EasyButtons;
using UnityEngine;
using Wilberforce.FinalVignette;

[RequireComponent(typeof(FinalVignetteCommandBuffer))]
public class FullScreenColor : MonoBehaviour
{
	public float red = 0;
	public float green = 0;
	public float blue = 0;
	public float alpha = 0;
	
    public void SetColor(Color newColor)
    {
        FinalVignetteCommandBuffer vignette = GetComponent<FinalVignetteCommandBuffer>();
        vignette.enabled = true;
        vignette.VignetteOuterColor = newColor;

		vignette.VignetteOuterValueDistance = 0f;
		vignette.VignetteInnerValueDistance = 0f;
		
        FullScreenTracking fstracking = GetComponent<FullScreenTracking>();
        fstracking.pushColor(true, newColor);
    }

    public void SetRGBA(float r, float g, float b, float a)
    {
        SetColor(new Color(r, g, b, a));
		red = r;
		blue = b;
		green = g;
		alpha = a;
		
    }

    [Button]
    public void Disable()
    {
        GetComponent<FinalVignetteCommandBuffer>().enabled = false;
        GetComponent<FinalVignetteCommandBuffer>().VignetteOuterColor = new Color(0.76f, 0, 0, 1);

        FullScreenTracking fstracking = GetComponent<FullScreenTracking>();
        fstracking.pushColor(false, new Color(0f,0f,0f,0f));
    }

    [Button]
    public void SetHalfWhite()
    {
        SetRGBA(1, 1, 1, 0.5f);
    }

    [Button]
    public void SetRed()
    {
        SetColor(Color.red);
    }

    [Button]
    public void SetGreen()
    {
        SetColor(Color.green);
    }

    [Button]
    public void SetBlue()
    {
        SetColor(Color.blue);
    }
}
