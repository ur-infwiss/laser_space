﻿using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class EventSink
{

    public static MQTTClient mqttClient = null; // note to self: writing references in C# is atomic
    public static LSLMarkerLogger lslLogger = null;

    public static void Appear(GameObject gameObject)
    {
        if (mqttClient != null)
        {
            mqttClient.LogEvent(new Dictionary<string, object>
            {
                { "gameObjectName", gameObject.name.Replace("(Clone)", "").Trim() },
                { "position", gameObject.transform.position.ToString() }
            });
        }
        if (lslLogger != null)
        {
            lslLogger.LogEvent(new Dictionary<string, object>
            {
                { "gameObjectName", gameObject.name.Replace("(Clone)", "").Trim() },
                { "position", gameObject.transform.position.ToString() }
            });
        }
    }

    public static void Disappear(GameObject gameObject)
    {
        if (mqttClient != null)
        {
            mqttClient.LogEvent(new Dictionary<string, object>
            {
                { "gameObjectName", gameObject.name.Replace("(Clone)", "").Trim() },
                { "position", gameObject.transform.position.ToString() }
            });
        }
        if (lslLogger != null)
        {
            lslLogger.LogEvent(new Dictionary<string, object>
            {
                { "gameObjectName", gameObject.name.Replace("(Clone)", "").Trim() },
                { "position", gameObject.transform.position.ToString() }
            });
        }
    }

    internal static void PistolShot(GameObject pistol, Vector3 start, Vector3 end, PistolTarget hitTarget, float angleToCenterOfTarget)
    {
        if (mqttClient != null)
        {
            mqttClient.LogEvent(new Dictionary<string, object>
            {
                { "gameObjectName", pistol.name },
                { "start", start.ToString() },
                { "end", end.ToString() },
                { "hitTarget", hitTarget ? hitTarget.name : null },
                { "angleToCenterOfTarget", angleToCenterOfTarget }
            });
        }
        if (lslLogger != null)
        {
            lslLogger.LogEvent(new Dictionary<string, object>
            {
                { "gameObjectName", pistol.name },
                { "start", start.ToString() },
                { "end", end.ToString() },
                { "hitTarget", hitTarget ? hitTarget.name : null },
                { "angleToCenterOfTarget", angleToCenterOfTarget }
            });
        }
    }

    public static void GameStatechange(Game game)
    {
        float currentHits = GameObject.FindObjectOfType<VignetteController>().currentHits;
        if (mqttClient != null)
        {
            mqttClient.LogEvent(new Dictionary<string, object>
            {
                { "destroyedDrones", game.destroyedDrones },
                { "destroyedSpiders", game.destroyedSpiders },
                { "destroyedProjectiles", game.destroyedProjectiles },
                { "EnemiesAlive", game.EnemiesAlive },
                { "currentWave", game.currentWave },
                { "startOfCurrentWave", game.startOfCurrentWave },
                { "time", Time.time },
                { "currentHits", currentHits },
            });
        }
        if (lslLogger != null)
        {
            lslLogger.LogEvent(new Dictionary<string, object>
            {
                { "destroyedDrones", game.destroyedDrones },
                { "destroyedSpiders", game.destroyedSpiders },
                { "destroyedProjectiles", game.destroyedProjectiles },
                { "EnemiesAlive", game.EnemiesAlive },
                { "currentWave", game.currentWave },
                { "startOfCurrentWave", game.startOfCurrentWave },
                { "time", Time.time },
                { "currentHits", currentHits },
            });
        }
    }

    public static void GameControllerStatechange(GameController gamecontroller)
    {
        float currentHits = GameObject.FindObjectOfType<VignetteController>().currentHits;
        if (mqttClient != null)
        {
            mqttClient.LogEvent(new Dictionary<string, object>
            {
                { "destroyedDrones", gamecontroller.destroyedDrones },
                { "destroyedSpiders", gamecontroller.destroyedSpiders },
                { "destroyedProjectiles", gamecontroller.destroyedProjectiles },
                { "EnemiesAlive", gamecontroller.enemiesAlive },
                { "currentWave", gamecontroller.wavenumber },
                { "startOfCurrentWave", gamecontroller.timer },
                { "time", Time.time },
                { "currentHits", currentHits },
            });
        }
        if (lslLogger != null)
        {
            lslLogger.LogEvent(new Dictionary<string, object>
            {
                { "destroyedDrones", gamecontroller.destroyedDrones },
                { "destroyedSpiders", gamecontroller.destroyedSpiders },
                { "destroyedProjectiles", gamecontroller.destroyedProjectiles },
                { "EnemiesAlive", gamecontroller.enemiesAlive },
                { "currentWave", gamecontroller.wavenumber },
                { "startOfCurrentWave", gamecontroller.timer },
                { "time", Time.time },
                { "currentHits", currentHits },
            });
        }
    }

    public static void FrameDuration(float frameDuration)
    {
        if (mqttClient != null)
        {
            mqttClient.LogEvent(new Dictionary<string, object>
            {
                { "frameDuration", frameDuration }
            });
        }
    }

    public static void Connected()
    {
        if (mqttClient != null)
        {
            mqttClient.LogEvent(new Dictionary<string, object>
            {
            });
        }
        if (lslLogger != null)
        {
            lslLogger.LogEvent(new Dictionary<string, object>
            {
            });
        }
    }
}