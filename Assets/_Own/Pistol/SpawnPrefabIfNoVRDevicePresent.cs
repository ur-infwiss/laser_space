﻿using EasyButtons;
using UnityEngine;

public class SpawnPrefabIfNoVRDevicePresent : MonoBehaviour
{
    public GameObject prefab;

    void Start()
    {
        if (!UnityEngine.XR.XRDevice.isPresent)
        {
            Spawn();
        }
    }

    [Button]
    public void Spawn()
    {
        Instantiate(prefab, transform);
    }
}
