﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

using EasyButtons;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class DistanceToSurfaceGrid : MonoBehaviour
{
    public int approximateNumberOfGridCells = 1000000;

    private static int LAYER_MASK = -1;
    public static int GetLayerMask
    {
        get
        {
            if (LAYER_MASK == -1)
            {
                LAYER_MASK = 1 << LayerMask.NameToLayer("Static Surface");
            }

            return LAYER_MASK;
        }
    }

    private static bool needsUpdate = true;
    public static bool NeedsUpdate { get { return needsUpdate; } }

    private static Vector3 gridCellSize;
    private static Vector3 halfGridCellSize;
    private static long profilingCheckSphereCount = 0;

    // stores the value of the scalar field at the *center* of each grid cell
    private static float[,,] distanceToNearestSurface;

    public static Vector3 GridCellSize { get { UpdateGridIfNecessary(); return gridCellSize; } }

    public static Vector3Int GridSize { get { UpdateGridIfNecessary(); return new Vector3Int(distanceToNearestSurface.GetLength(0), distanceToNearestSurface.GetLength(1), distanceToNearestSurface.GetLength(2)); } }

    [Button]
    private static void UpdateEntireGrid()
    {
        int MAX_NUMBER_OF_GRID_CELLS = GameObject.FindObjectOfType<DistanceToSurfaceGrid>().approximateNumberOfGridCells;

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();
        profilingCheckSphereCount = 0;

        Bounds boundsOfScene = BoundsOfScene.GetBoundsOfScene();
        float volume = boundsOfScene.size.x * boundsOfScene.size.y * boundsOfScene.size.z;
        float volumePerGridCell = volume / MAX_NUMBER_OF_GRID_CELLS;
        float averageCellSize = Mathf.Pow(volumePerGridCell, 0.33333f);

        distanceToNearestSurface = new float[1, 1, 1];
        distanceToNearestSurface = new float[
            Mathf.RoundToInt(boundsOfScene.size.x / averageCellSize),
            Mathf.RoundToInt(boundsOfScene.size.y / averageCellSize),
            Mathf.RoundToInt(boundsOfScene.size.z / averageCellSize)];
        gridCellSize = new Vector3(
            boundsOfScene.size.x / distanceToNearestSurface.GetLength(0),
            boundsOfScene.size.y / distanceToNearestSurface.GetLength(1),
            boundsOfScene.size.z / distanceToNearestSurface.GetLength(2));
        halfGridCellSize = gridCellSize * 0.5f;

        if (!TryToLoadArrayFromFile())
        {
            DeleteCacheFile();

            Vector3 position = Vector3.zero;
            for (int z = 0; z < distanceToNearestSurface.GetLength(2); z++)
            {
                position.z = (z + 0.5f) * gridCellSize.z + boundsOfScene.min.z;
                for (int y = 0; y < distanceToNearestSurface.GetLength(1); y++)
                {
                    position.y = (y + 0.5f) * gridCellSize.y + boundsOfScene.min.y;
                    for (int x = 0; x < distanceToNearestSurface.GetLength(0); x++)
                    {
                        position.x = (x + 0.5f) * gridCellSize.x + boundsOfScene.min.x;

                        float min = 0;
                        float max = float.PositiveInfinity;

                        for (int dz = -1; dz <= +1; dz++)
                        {
                            if (z + dz >= 0 && z + dz < distanceToNearestSurface.GetLength(2))
                            {
                                for (int dy = -1; dy <= +1; dy++)
                                {
                                    if (y + dy >= 0 && y + dy < distanceToNearestSurface.GetLength(1))
                                    {
                                        for (int dx = -1; dx <= +1; dx++)
                                        {
                                            if (x + dx >= 0 && x + dx < distanceToNearestSurface.GetLength(0))
                                            {
                                                float neighborValue = distanceToNearestSurface[x + dx, y + dy, z + dz];
                                                if (neighborValue != 0)
                                                {
                                                    float distanceToNeighbor = new Vector3(gridCellSize.x * dx, gridCellSize.y * dy, gridCellSize.z * dz).magnitude;
                                                    min = Mathf.Max(min, neighborValue - distanceToNeighbor);
                                                    max = Mathf.Min(max, neighborValue + distanceToNeighbor);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        distanceToNearestSurface[x, y, z] = ComputeDistanceToNearestSurface(position, min, max);
                    }
                }
#if UNITY_EDITOR
                if (stopWatch.Elapsed.Seconds > 1)
                {
                    if (EditorUtility.DisplayCancelableProgressBar("Computing Distance Grid", "", (z + 0.5f) / distanceToNearestSurface.GetLength(2) * 0.9f))
                    {
                        EditorUtility.ClearProgressBar();
                        GameObject.FindObjectOfType<DistanceToSurfaceGrid>().approximateNumberOfGridCells = 100000;
                        needsUpdate = true;
                        EditorUtility.SetDirty(GameObject.FindObjectOfType<DistanceToSurfaceGrid>());
                        return;
                    }
                }
#endif
            }

            findAndFlipOutsideParts();

#if UNITY_EDITOR
            EditorUtility.ClearProgressBar();
#endif

            stopWatch.Stop();
            UnityEngine.Debug.Log("Distance Grid - update duration: " + stopWatch.Elapsed
                + " calls to CheckSphere: " + profilingCheckSphereCount
                + " calls / cell: " + (profilingCheckSphereCount / (float)distanceToNearestSurface.Length));

            SaveArrayToFile();
        }
    }

    private static IEnumerable<Vector3Int> NeighborsOf(Vector3Int p, Vector3Int size)
    {
        for (int z = Math.Max(0, p.z - 1); z < Math.Min(size.z, p.z + 2); z++)
        {
            for (int y = Math.Max(0, p.y - 1); y < Math.Min(size.y, p.y + 2); y++)
            {
                for (int x = Math.Max(0, p.x - 1); x < Math.Min(size.x, p.x + 2); x++)
                {
                    Vector3Int result = new Vector3Int(x, y, z);
                    if (p != result)
                    {
                        yield return result;
                    }
                }
            }
        }
    }

    private static IEnumerable<Vector3Int> CornerPoints(Vector3Int size)
    {
        foreach (int z in new int[] { 0, size.z - 1 })
        {
            foreach (int y in new int[] { 0, size.y - 1 })
            {
                foreach (int x in new int[] { 0, size.x - 1 })
                {
                    yield return new Vector3Int(x, y, z);
                }
            }
        }
    }

    private static void findAndFlipOutsideParts()
    {
        float flipThreshold = gridCellSize.magnitude * 1f;

        Vector3Int gridSize = new Vector3Int(
            distanceToNearestSurface.GetLength(0),
            distanceToNearestSurface.GetLength(1),
            distanceToNearestSurface.GetLength(2));
        bool[,,] visited = new bool[gridSize.x, gridSize.y, gridSize.z];

        // assume that all 8 corners are outside and search inwards from there
        int corner = 0;
        foreach (var start in CornerPoints(gridSize))
        {
#if UNITY_EDITOR
            EditorUtility.DisplayCancelableProgressBar("Computing Distance Grid", "", 0.9f + (corner / 8.0f) * 0.1f);
            corner++;
#endif

            // breadth-first iteration over the grid
            Queue<Vector3Int> agenda = new Queue<Vector3Int>();
            agenda.Enqueue(start);
            visited[start.x, start.y, start.z] = true;
            while (agenda.Count > 0)
            {
                Vector3Int p = agenda.Dequeue();
                float valueOfP = distanceToNearestSurface[p.x, p.y, p.z];

                // p is outside -> stored distance is actually negative
                distanceToNearestSurface[p.x, p.y, p.z] = -valueOfP;

                foreach (Vector3Int n in NeighborsOf(p, gridSize))
                {
                    if (!visited[n.x, n.y, n.z])
                    {
                        float valueOfN = distanceToNearestSurface[n.x, n.y, n.z];
                        if (valueOfP >= valueOfN || valueOfP > flipThreshold)  // n is also outside
                        {
                            agenda.Enqueue(n);
                            visited[n.x, n.y, n.z] = true;
                        }
                    }
                }
            }
        }
    }

    private static void SaveArrayToFile()
    {
        var byteArray = new byte[distanceToNearestSurface.Length * 4];
        Buffer.BlockCopy(distanceToNearestSurface, 0, byteArray, 0, byteArray.Length);
        string filename = GetPathOfArrayFile();
        UnityEngine.Debug.Log("Saving DistanceToSurfaceGrid to " + filename + "...");
        File.WriteAllBytes(filename, byteArray);
    }

    private static bool TryToLoadArrayFromFile()
    {
        string filename = GetPathOfArrayFile();
        try
        {
            var byteArray = File.ReadAllBytes(filename);
            if (distanceToNearestSurface.Length * 4 == byteArray.Length)
            {
                Buffer.BlockCopy(byteArray, 0, distanceToNearestSurface, 0, byteArray.Length);
                UnityEngine.Debug.Log("Loaded DistanceToSurfaceGrid from " + filename + ".");
                return true;
            }
        }
        catch (IOException)
        {
        }
        UnityEngine.Debug.Log("Failed to load DistanceToSurfaceGrid from " + filename + ".");
        return false;
    }

    [Button]
    public static void DeleteCacheFile()
    {
        for (int i = 0; i <= CacheFileVersion; i++)
        {
            string filename = GetPathOfArrayFile(i);
            UnityEngine.Debug.Log("Deleting " + filename + "...");
            File.Delete(filename);
        }
        needsUpdate = true;
    }

    // Increase this number every time the computation of the contents of the cache file changes!
    private static int CacheFileVersion = 2;

    private static string GetPathOfArrayFile(int cacheFileVersion = -1)
    {
        if (cacheFileVersion == -1)
        {
            cacheFileVersion = CacheFileVersion;
        }
        return Application.persistentDataPath + "\\distanceToNearestSurface" + (cacheFileVersion == 0 ? "" : "." + cacheFileVersion) + ".bin";
    }

    private static float ComputeDistanceToNearestSurface(Vector3 position, float min = 0, float max = float.PositiveInfinity)
    {
        float epsilon = gridCellSize.magnitude / 10;
        float upperBound = 0;
        float lowerBound = 0;
        if (max == float.PositiveInfinity)
        {
            // grow upperBound until it actually intersects with something
            while (!Physics.CheckSphere(position, upperBound, GetLayerMask) && upperBound < 1000000)
            {
                profilingCheckSphereCount++;
                upperBound = upperBound * 2 + epsilon;
            }
            lowerBound = Mathf.Max(0, (upperBound - epsilon) / 2);
        }
        else
        {
            upperBound = max;
            lowerBound = Mathf.Max(0, min);
        }

        // binary search with Physics.CheckSphere as pivot
        while (upperBound - lowerBound > epsilon)
        {
            float middle = (lowerBound + upperBound) / 2;
            if (Physics.CheckSphere(position, middle, GetLayerMask))
            {
                upperBound = middle;
            }
            else
            {
                lowerBound = middle;
            }
            profilingCheckSphereCount++;
        }

        return (lowerBound + upperBound) / 2;
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        BoundsOfScene.Invalidate();
        needsUpdate = true;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        BoundsOfScene.Invalidate();
        needsUpdate = true;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnValidate()
    {
        approximateNumberOfGridCells = Mathf.Clamp(approximateNumberOfGridCells, 10000, 1000000);
        needsUpdate = true;
    }

    public static float DistanceToNearestSurface(Vector3 position)
    {
        UpdateGridIfNecessary();

        Bounds boundsOfScene = BoundsOfScene.GetBoundsOfScene();
        Vector3 localPosition = position - boundsOfScene.min - halfGridCellSize;
        localPosition.x /= gridCellSize.x;
        localPosition.y /= gridCellSize.y;
        localPosition.z /= gridCellSize.z;
        int x0 = Mathf.FloorToInt(localPosition.x);
        int y0 = Mathf.FloorToInt(localPosition.y);
        int z0 = Mathf.FloorToInt(localPosition.z);
        int x1 = x0 + 1;
        int y1 = y0 + 1;
        int z1 = z0 + 1;

        // outside of the grid?
        if (x0 < 0 || x1 >= distanceToNearestSurface.GetLength(0) ||
            y0 < 0 || y1 >= distanceToNearestSurface.GetLength(1) ||
            z0 < 0 || z1 >= distanceToNearestSurface.GetLength(2))
        {
            return boundsOfScene.size.magnitude;
        }

        // use 2x2x2 scalar values to compute something similar to the gradient of the distance field
        float alphaX = localPosition.x - x0;
        float alphaY = localPosition.y - y0;
        float alphaZ = localPosition.z - z0;

        float d000 = distanceToNearestSurface[x0, y0, z0];
        float d100 = distanceToNearestSurface[x1, y0, z0];
        float d010 = distanceToNearestSurface[x0, y1, z0];
        float d110 = distanceToNearestSurface[x1, y1, z0];
        float d001 = distanceToNearestSurface[x0, y0, z1];
        float d101 = distanceToNearestSurface[x1, y0, z1];
        float d011 = distanceToNearestSurface[x0, y1, z1];
        float d111 = distanceToNearestSurface[x1, y1, z1];

        float d = Mathf.Lerp(Mathf.Lerp(Mathf.Lerp(d000, d100, alphaX), Mathf.Lerp(d010, d110, alphaX), alphaY), Mathf.Lerp(Mathf.Lerp(d001, d101, alphaX), Mathf.Lerp(d011, d111, alphaX), alphaY), alphaZ);

        return d;
    }

    /// <summary>
    /// Computes the gradient at the given position.
    /// </summary>
    /// <param name="position"></param>
    /// <returns>the gradient at the given position.</returns>
    public static Vector3 ComputeVectorToNearestSurface(Vector3 position)
    {
        UpdateGridIfNecessary();

        Bounds boundsOfScene = BoundsOfScene.GetBoundsOfScene();
        Vector3 localPosition = position - boundsOfScene.min - halfGridCellSize;
        localPosition.x /= gridCellSize.x;
        localPosition.y /= gridCellSize.y;
        localPosition.z /= gridCellSize.z;
        int x0 = Mathf.FloorToInt(localPosition.x);
        int y0 = Mathf.FloorToInt(localPosition.y);
        int z0 = Mathf.FloorToInt(localPosition.z);
        int x1 = x0 + 1;
        int y1 = y0 + 1;
        int z1 = z0 + 1;

        // outside of the grid?
        if (x0 < 0 || x1 >= distanceToNearestSurface.GetLength(0) ||
            y0 < 0 || y1 >= distanceToNearestSurface.GetLength(1) ||
            z0 < 0 || z1 >= distanceToNearestSurface.GetLength(2))
        {
            return Vector3.zero;
        }

        // use 2x2x2 scalar values to compute something similar to the gradient of the distance field
        float alphaX = localPosition.x - x0;
        float alphaY = localPosition.y - y0;
        float alphaZ = localPosition.z - z0;

        float d000 = distanceToNearestSurface[x0, y0, z0];
        float d100 = distanceToNearestSurface[x1, y0, z0];
        float d010 = distanceToNearestSurface[x0, y1, z0];
        float d110 = distanceToNearestSurface[x1, y1, z0];
        float d001 = distanceToNearestSurface[x0, y0, z1];
        float d101 = distanceToNearestSurface[x1, y0, z1];
        float d011 = distanceToNearestSurface[x0, y1, z1];
        float d111 = distanceToNearestSurface[x1, y1, z1];

        // (x,y,z) is actually the gradient of the distance field
        float x = Mathf.Lerp(Mathf.Lerp(d000 - d100, d010 - d110, alphaY), Mathf.Lerp(d001 - d101, d011 - d111, alphaY), alphaZ);
        float y = Mathf.Lerp(Mathf.Lerp(d000 - d010, d100 - d110, alphaX), Mathf.Lerp(d001 - d011, d101 - d111, alphaX), alphaZ);
        float z = Mathf.Lerp(Mathf.Lerp(d000 - d001, d100 - d101, alphaX), Mathf.Lerp(d010 - d011, d110 - d111, alphaX), alphaY);
        float d = Mathf.Lerp(Mathf.Lerp(Mathf.Lerp(d000, d100, alphaX), Mathf.Lerp(d010, d110, alphaX), alphaY), Mathf.Lerp(Mathf.Lerp(d001, d101, alphaX), Mathf.Lerp(d011, d111, alphaX), alphaY), alphaZ);

        return new Vector3(x, y, z).normalized * d;
    }

    public static Vector3 FindNearestIsoSurface(Vector3 currentPosition, float isoValue = 0)
    {
        UpdateGridIfNecessary();

        float epsilon = gridCellSize.magnitude * 0.05f;
        isoValue = Mathf.Max(isoValue, epsilon);

        float remainingDistance = float.PositiveInfinity;
        Vector3 step;
        int i = 0;
        do
        {
            Vector3 toSurface = ComputeVectorToNearestSurface(currentPosition);
            remainingDistance = toSurface.magnitude - isoValue;
            step = toSurface.normalized * remainingDistance * 0.75f;
            currentPosition += step;
            i++;
        } while (Mathf.Abs(remainingDistance) > epsilon && step.magnitude > epsilon && i < 10);

        return currentPosition;
    }

    private static void UpdateGridIfNecessary()
    {
        if (needsUpdate)
        {
            UpdateEntireGrid();
            needsUpdate = false;
        }
    }

    void OnDrawGizmosSelected()
    {
        UpdateGridIfNecessary();

        Bounds boundsOfScene = BoundsOfScene.GetBoundsOfScene();
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(boundsOfScene.min + gridCellSize / 2, gridCellSize);
        Gizmos.DrawWireCube(boundsOfScene.max - gridCellSize / 2, gridCellSize);
        Gizmos.DrawWireCube(boundsOfScene.center, gridCellSize);
    }
}
