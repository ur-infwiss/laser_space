﻿using EasyButtons;
using UnityEngine;

public class SpiderBehavior : MonoBehaviour
{
    public enum State
    {
        SNAP_TO_SURFACE, APPROACH_FIRING_POSITION, CHARGE_SHOT, FIRE_ON_PLAYER, WAIT_AFTER_SHOT
    };

    public State state = State.SNAP_TO_SURFACE;
    public float distanceToSurfaceIsoValue;
    public float targetDistanceToMainCamera;
    public float maxSpeed;

    public GameObject explosionPrefab;
    public GameObject projectilePrefab;
    public AudioClip soundeffectDestroyed;
    public Transform muzzleTransform;
    public GameObject muzzleFlashPrefab;


    public AudioSource soundeffectChargeAndFire;
    public float chargeTimeBeforeFiring = 1;

    private float timestampOfLastMovementModeChange;

    // um zu checken ob spinne hängt...
    public Vector3[] movementchange;
    private int firstframenumber;

    private Animator animator;

    //enemy - type dependent score
    public int scorepoints = 10;
    public GameObject showScore;
    //Eigentlich ungünstig, da dann Drohne den Score setzen muss...
    //für mehrere Leben
    public int lives = 1;
    [HideInInspector]
    private int scorepointslostlife = 10;
    //für Schussanzahl
    public int shots = 1;
    private float betweenshotstime = 0.5f;
    private int shotscounter;
    private bool inShootingMode = false;
    private bool firedShot = false;
    private float animationTimeBeforeFiring = 0.5f;

    GameObject gameController;

    void Start()
    {
        animator = transform.GetComponentInChildren<Animator>();
        animator.SetInteger("states", 0);
        gameObject.tag = "Enemy";
        EventSink.Appear(gameObject);

        gameController = GameObject.Find("GameController");

        if (muzzleTransform == null)
        {
            muzzleTransform = transform.Find("L_fang_3");
        }
        Debug.Assert(muzzleTransform != null);

        //For counting lives
        PistolTarget pistolTarget = GetComponent<PistolTarget>();
        if (pistolTarget != null)
        {
            pistolTarget.OnHit += (hitGameObject =>
            {
                if (lives > 1)
                {
                    lives--;
                    gameController.GetComponent<GameController>().IncreaseScore(scorepointslostlife);
                    //Score anzeigen
                    GameObject scoring = Instantiate(showScore, transform.position, new Quaternion(0f, 0f, 0f, 0f));
                    scoring.GetComponent<ScoreBehaviour>().SetScore(scorepointslostlife);
                    scoring.transform.LookAt(GameObject.Find("Camera (eye)").transform.position);
                }
                else
                {
                    Destroy(gameObject);

                }

            });

            
        }

        //erstmaliges setzen von shotcounter
        shotscounter = shots;

        timestampOfLastMovementModeChange = Time.time;

        //gegen festhaengen
        movementchange = new Vector3[45];
        firstframenumber = Time.frameCount;

    }

    void Update()
    {
        float time = Time.time + gameObject.GetInstanceID() % 97;
        Vector4 furForceGlobal = new Vector4(Mathf.Sin(time * 1.1f), Mathf.Sin(time * 1.3f), Mathf.Sin(time * 1.5f), 0) * 0.5f;
        GetComponentInChildren<Renderer>().material.SetVector("_ForceGlobal", furForceGlobal);

        for(int i = movementchange.Length; i > 0; i--)
        {
            if (i != 1)
            {
                movementchange[i - 1] = movementchange[i - 2];
            }
            else
            {
                movementchange[i - 1] = transform.position;
            }
        }

        switch (state)
        {
            case State.SNAP_TO_SURFACE:
                {
                    animator.SetInteger("states", 0);
                    SnapPositionToNearestSurface();
                    RotateToMainCamera();
                    state = State.APPROACH_FIRING_POSITION;
                    timestampOfLastMovementModeChange = Time.time;
                    break;
                }
            case State.APPROACH_FIRING_POSITION:
                {
                    //gegen spinne sitzt fest:
                    if (Time.frameCount > firstframenumber + 45)
                    {
                        checkIfEnemyIsMoving();
                    }

                    animator.SetInteger("states", 1);
                    float speed = speedTowardsCamera();

                    //speed = -1f; war bloss zum Testen des rückwärtslaufens

                    transform.Translate(Vector3.forward * speed * Time.deltaTime);
					
					SnapPositionToNearestSurface();
					

                    Quaternion targetRotation = computeOrientationToMainCamera();
                    float deltaAngle = Quaternion.Angle(transform.rotation, targetRotation);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime);
                    Debug.Log(deltaAngle);

                    float animationSpeedFromAngle = Mathf.Clamp01(deltaAngle / 10.0f);
                    float animationSpeedFromLinear = speed / maxSpeed;
                    float animationSpeed = Mathf.Max(animationSpeedFromLinear, animationSpeedFromAngle);

                    animationSpeed = Mathf.Clamp(animationSpeed, 0.05f, 1);
                    animator.speed = animationSpeed;

                    if (animationSpeed < 0.2f && (Time.time > 5 + timestampOfLastMovementModeChange))
                    {
                        state = State.CHARGE_SHOT;
                        timestampOfLastMovementModeChange = Time.time;
                        break;
                    }

                    break;
                }
            case State.CHARGE_SHOT:
                {
                    animator.SetInteger("states", 2);
                    animator.speed = 1;

                    if (timestampOfLastMovementModeChange + 1 < Time.time) inShootingMode = true; // Warten von 1 s nach Wechsel zum Idle zustand

                    if (inShootingMode)
                    {
                        if (!soundeffectChargeAndFire.isPlaying)
                        {
                            soundeffectChargeAndFire.Play();
                        }

                        if (timestampOfLastMovementModeChange + chargeTimeBeforeFiring - animationTimeBeforeFiring < Time.time)
                        {
                            state = State.FIRE_ON_PLAYER;
                            timestampOfLastMovementModeChange = Time.time;
                            break;
                        }
                    }


                    break;

                    
                }
            case State.FIRE_ON_PLAYER:
                {
                    animator.SetInteger("states", 3);
                    animator.speed = 1f;



                    if (shotscounter > 0 && Time.frameCount % 23 == 0)
                    {
                        if (timestampOfLastMovementModeChange + animationTimeBeforeFiring < Time.time)
                        {
                            animator.speed = 0;
                            if (muzzleFlashPrefab != null)
                            {
                                Instantiate(muzzleFlashPrefab, muzzleTransform.position, muzzleTransform.rotation, transform);
                            }

                            if (projectilePrefab != null)
                            {
                                Instantiate(projectilePrefab, muzzleTransform.position, muzzleTransform.rotation);
                            }

							
							
                            shotscounter--;
                            //animator.SetInteger("states", 2);
                            if (shotscounter == 0) animator.speed = 1;
                        }
                    }
                    
                    else if (shotscounter == 0 && timestampOfLastMovementModeChange + 2f < Time.time)
                    {
                        Debug.Log("Wechsel");
                        state = State.WAIT_AFTER_SHOT;
                        timestampOfLastMovementModeChange = Time.time;
                        shotscounter = shots;

                    }

                    break;

                }
            case State.WAIT_AFTER_SHOT:
                {
                    animator.SetInteger("states", 2);
                    if (Time.time > timestampOfLastMovementModeChange + 2)
                    {
                        state = State.APPROACH_FIRING_POSITION;
                        timestampOfLastMovementModeChange = Time.time;
                        break;
                    }
                    break;
                }

            default:
                {
                    throw new System.Exception("Forgot to implement a case!");
                }
        }
    }

    [Button]
    public void SnapPositionToNearestSurface()
    {
        transform.position = DistanceToSurfaceGrid.FindNearestIsoSurface(transform.position, distanceToSurfaceIsoValue);
    }

    [Button]
    public void RotateToMainCamera()
    {
        transform.rotation = computeOrientationToMainCamera();
    }

    private Quaternion computeOrientationToMainCamera()
    {
        Vector3 gradient = DistanceToSurfaceGrid.ComputeVectorToNearestSurface(transform.position);
        Vector3 toCamera = (Camera.main.transform.position - transform.position).normalized;
        Vector3 up = gradient.normalized * -1;

        //Fix fuer falsche Rotation an Säulenwänden:
        Vector3 leftPillarPosition = GameObject.Find("LeftPillar").transform.position;
        Vector3 rightPillarPosition = GameObject.Find("RightPillar").transform.position;
        if (Vector3.Distance(leftPillarPosition, transform.position)<3.2f)
        {
            if(Vector3.Angle(transform.position - leftPillarPosition,up)>135 & Vector3.Angle(transform.position - leftPillarPosition, up) < 225)
            {
                up *= -1;
            }
        }
        else if(Vector3.Distance(rightPillarPosition, transform.position) < 3.2f)
        {
            if (Vector3.Angle(transform.position - rightPillarPosition, up) > 135 & Vector3.Angle(transform.position - rightPillarPosition, up) < 225)
            {
                up *= -1;
            }
        }


        Vector3 right = Vector3.Cross(toCamera, up).normalized;
        Vector3 forward = Vector3.Cross(up, right).normalized;
		
		// Debug.Log(forward);
        return Quaternion.LookRotation(forward, up);
    }

    private float speedTowardsCamera()
    {
        float distanceToCamera = (Camera.main.transform.position - transform.position).magnitude;
        float remainingDistance = distanceToCamera - targetDistanceToMainCamera;
        return Mathf.Clamp(remainingDistance, 0, maxSpeed);
        //auch rückwärts laufen lassen
        //return Mathf.Clamp(remainingDistance, -maxSpeed, maxSpeed);
    }

    private bool applicationIsQuitting = false;

    private void OnApplicationQuit()
    {
        applicationIsQuitting = true;
    }

    private void checkIfEnemyIsMoving()
    {
        float speed = speedTowardsCamera();
        float[] changeInPositionValue = new float[] { 0f, 0f, 0f };
        if (speed/maxSpeed > 0.5) //d.h. spinne nicht in schiessposition sondern eigentlich weit entfernt
        {
            for(int i = 0; i<(movementchange.Length-1); i++)
            {
                //addieren aller beträge in jede richtung
                changeInPositionValue[0] += Mathf.Abs(movementchange[i + 1].x - movementchange[i].x);
                changeInPositionValue[1] += Mathf.Abs(movementchange[i + 1].y - movementchange[i].y);
                changeInPositionValue[2] += Mathf.Abs(movementchange[i + 1].z - movementchange[i].z);

            }

            if (changeInPositionValue[0] + changeInPositionValue[1] + changeInPositionValue[2] <  speed * Time.deltaTime * 45)
            {
                Debug.Log(gameObject.name + ": Spinne sitzt fest");
                transform.position += transform.up * 0.1f;
            }
        }
    }

    private void OnDestroy()
    {
        if (!applicationIsQuitting && explosionPrefab != null)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);

            if (soundeffectDestroyed != null)
            {
                AudioSource.PlayClipAtPoint(soundeffectDestroyed, transform.position);
            }

            if (gameController.GetComponent<EnemyController>().externalDestroy == false)
            {
                //Score
                gameController.GetComponent<GameController>().IncreaseScore(scorepoints);

                //Score anzeigen
                GameObject scoring = Instantiate(showScore, transform.position, new Quaternion(0f, 0f, 0f, 0f));


                scoring.GetComponent<ScoreBehaviour>().SetPositionAndRotation(transform.position);
                scoring.GetComponent<ScoreBehaviour>().SetScore(scorepoints);


            }

            gameController.GetComponent<EnemyController>().enemyWasDeleted = true;

        }

        

        EventSink.Disappear(gameObject);
    }
}
