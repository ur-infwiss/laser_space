﻿using UnityEngine;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class DroneBehavior : MonoBehaviour
{
    //enemy - type dependent score
    public int scorepoints = 10;
    public GameObject showScore;
    //Eigentlich ungünstig, da dann Drohne den Score setzen muss...
    //für mehrere Leben
    public int lives = 1;
    [HideInInspector]
    private int scorepointslostlife = 10;
    //für Schussanzahl
    public int shots = 1;
    private float betweenshotstime = 0.5f;
    private int shotscounter;

    public GameObject explosionPrefab;
    public GameObject projectilePrefab;
    public GameObject muzzleFlashPrefab;
    public Transform muzzleTransform;

    public BoxCollider selectedFiringPoint;
    public Vector3 currentMovementTarget = Vector3.positiveInfinity;

    public float maxSpeed = 1;
    public MovementDerivativesTracker movementDerivativesTracker;

    [HideInInspector]
    private float initialDistanceToCurrentTarget = 10000;
    [HideInInspector]
    private float myRadius;

    private float distanceToConsiderTargetReached;

    public enum ActionState { MoveTowardsTarget, RotateTowardsTarget, RotateTowardsPlayer, ChargeShot, FireOnPlayer };
    public ActionState actionState = ActionState.RotateTowardsTarget;
    private float timestampOfLastMovementModeChange;

    public AudioSource soundeffectChargeAndFire;
    public float chargeTimeBeforeFiring = 0;
    public AudioClip soundeffectDestroyed;

    //Fuer Static Methods
    GameObject gameController;

    void Start()
    {
        gameObject.tag = "Enemy";
        myRadius = GetComponentInChildren<SphereCollider>().radius;
        distanceToConsiderTargetReached = myRadius / 2;
        Debug.Assert(movementDerivativesTracker != null);
        timestampOfLastMovementModeChange = Time.time;

        gameController = GameObject.Find("GameController");

        if (muzzleTransform == null)
        {
            muzzleTransform = transform.Find("Muzzle");
        }
        Debug.Assert(muzzleTransform != null);

        //For counting lives
        PistolTarget pistolTarget = GetComponent<PistolTarget>();
        if (pistolTarget != null)
        {
            pistolTarget.OnHit += (hitGameObject =>
            {
                if (lives > 1)
                {
                    lives--;
                    gameController.GetComponent<GameController>().IncreaseScore(scorepointslostlife);
                    //Score anzeigen
                    GameObject scoring = Instantiate(showScore, transform.position, new Quaternion(0f, 0f, 0f, 0f));
                    scoring.GetComponent<ScoreBehaviour>().SetScore(scorepointslostlife);
                    scoring.transform.LookAt(GameObject.Find("Camera (eye)").transform.position);
                }
                else
                {
                    Destroy(gameObject);

                }
                
            });

            
        }

        //erstmaliges setzen von shotcounter
        shotscounter = shots;

        EventSink.Appear(gameObject);
    }

void Update()
    {
        switch (actionState)
        {
            case ActionState.RotateTowardsTarget:
                {
                    if (selectedFiringPoint == null)
                    {
                        SelectRandomFiringPointAndCurrentTarget();
                    }

                    VerySlowlyRotateTowards(currentMovementTarget);

                    // rotate for 1 second
                    if (timestampOfLastMovementModeChange + 1 < Time.time)
                    {
                        actionState = ActionState.MoveTowardsTarget;
                        timestampOfLastMovementModeChange = Time.time;
                        break;
                    }

                    break;
                }

            case ActionState.MoveTowardsTarget:
                {
                    MovePositionTowardsCurrentTarget();

                    float distanceToTarget = (currentMovementTarget - transform.position).magnitude;
                    if (distanceToTarget < distanceToConsiderTargetReached)
                    {
                        actionState = ActionState.RotateTowardsPlayer;
                        timestampOfLastMovementModeChange = Time.time;
                        break;
                    }

                    break;
                }

            case ActionState.RotateTowardsPlayer:
                {
                    VerySlowlyRotateTowards(Camera.main.transform.position);

                    // rotate for 1 second
                    if (timestampOfLastMovementModeChange + 1 < Time.time)
                    {
                        actionState = ActionState.ChargeShot;
                        timestampOfLastMovementModeChange = Time.time;
                        break;
                    }

                    break;
                }

            case ActionState.ChargeShot:
                {
                    if (!soundeffectChargeAndFire.isPlaying)
                    {
                        soundeffectChargeAndFire.Play();
                    }

                    VerySlowlyRotateTowards(Camera.main.transform.position);

                    // remain in this state during the charging part of the sound
                    if (timestampOfLastMovementModeChange + chargeTimeBeforeFiring < Time.time)
                    {
                        actionState = ActionState.FireOnPlayer;
                        timestampOfLastMovementModeChange = Time.time;
                        break;
                    }

                    break;
                }

            case ActionState.FireOnPlayer:
                {

                    if (shotscounter > 0)
                    {
                        if (timestampOfLastMovementModeChange + (betweenshotstime * (shots - shotscounter)) < Time.time)
                        {
                            if (muzzleFlashPrefab != null)
                            {
                                Instantiate(muzzleFlashPrefab, muzzleTransform.position, muzzleTransform.rotation, transform);
                            }

                            if (projectilePrefab != null)
                            {
                                Instantiate(projectilePrefab, muzzleTransform.position, muzzleTransform.rotation);
                            }

                            shotscounter--;
                        }
                        break;
                    }
                    else
                    {
                        shotscounter = shots;
                        // go to new target
                        selectedFiringPoint = null;
                        actionState = ActionState.RotateTowardsTarget;
                        timestampOfLastMovementModeChange = Time.time;
                        break;
                    }
                    
                    
                }
        }
    }

    private void SelectRandomFiringPointAndCurrentTarget()
    {
        // look up all firing points, sort them near to far
        var firingPointsNearToFar = FindObjectsOfType<DroneFiringPoint>()
            .OrderBy(droneFiringPoint => (droneFiringPoint.transform.position - transform.position).magnitude);

        // select one randomly, nearest with highest probability
        int i = 0;
        while (Random.value > 0.5f)
        {
            i = (i + 1) % firingPointsNearToFar.Count();
        }

        // every firing point must have a box collider
        selectedFiringPoint = firingPointsNearToFar.ElementAt(i).GetComponent<BoxCollider>();

        // pick random point within firing point's box collider as currentMovementTarget
        currentMovementTarget = selectedFiringPoint.transform.TransformPoint(new Vector3(
            selectedFiringPoint.center.x + (Random.value - 0.5f) * selectedFiringPoint.size.x,
            selectedFiringPoint.center.y + (Random.value - 0.5f) * selectedFiringPoint.size.y,
            selectedFiringPoint.center.z + (Random.value - 0.5f) * selectedFiringPoint.size.z
            ));
    }

    private void MovePositionTowardsCurrentTarget()
    {
        float currentdistanceToCurrentTarget = (transform.position - currentMovementTarget).magnitude;
        float speedFactor = 1;
        speedFactor = Mathf.Min(speedFactor, currentdistanceToCurrentTarget);
        speedFactor = Mathf.Min(speedFactor, initialDistanceToCurrentTarget - currentdistanceToCurrentTarget);
        speedFactor = Mathf.Max(speedFactor, 0.01f);
        float speed = speedFactor * maxSpeed;
        var direction = (currentMovementTarget - transform.position).normalized;
        var droneRepellingforce = QueryDroneRepellingForce();
        if (droneRepellingforce.magnitude > 0.01f)
        {
            direction = Vector3.Slerp(direction, droneRepellingforce.normalized, droneRepellingforce.magnitude);
            Debug.DrawRay(transform.position, droneRepellingforce, Color.red);
        }
        transform.position += direction * Time.deltaTime * speed;

        Debug.DrawLine(transform.position, currentMovementTarget, Color.white);
    }

    private void VerySlowlyRotateTowards(Vector3 target)
    {
        // we change transform.position here, because the script RotateTowardsMovement will then rotate the drone body

        Vector3 toTarget = target - transform.position;
        float currentdistanceToCurrentTarget = toTarget.magnitude;
        float speedFactor = 0.01f;
        float speed = speedFactor * maxSpeed;
        var direction = toTarget.normalized;
        transform.position += direction * Time.deltaTime * speed;

        Debug.DrawLine(transform.position, target, Color.gray);
    }

    private Vector3 QueryDroneRepellingForce()
    {
        Vector3 totalForce = Vector3.zero;
        foreach (DroneRepellingSphere droneRepellingSphere in DroneRepellingSphere.instances)
        {
            totalForce += droneRepellingSphere.QueryNormalizedRepellingVector(transform.position);
        }
        return totalForce;
    }

    private bool applicationIsQuitting = false;

    private void OnApplicationQuit()
    {
        applicationIsQuitting = true;
    }

    private void OnDestroy()
    {
        

        if (!applicationIsQuitting && explosionPrefab != null)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);

            if (soundeffectDestroyed != null)
            {
                AudioSource.PlayClipAtPoint(soundeffectDestroyed, transform.position);
            }

            if (gameController.GetComponent<EnemyController>().externalDestroy == false)
            {
                gameController.GetComponent<GameController>().IncreaseScore(scorepoints);


                //Score anzeigen
                //
                GameObject scoring = Instantiate(showScore, transform.position, new Quaternion(0f, 0f, 0f, 0f));


                scoring.GetComponent<ScoreBehaviour>().SetPositionAndRotation(transform.position);
                scoring.GetComponent<ScoreBehaviour>().SetScore(scorepoints);
                
                
            }

            gameController.GetComponent<EnemyController>().enemyWasDeleted = true;
        }

        

        EventSink.Disappear(gameObject);
    }
}
