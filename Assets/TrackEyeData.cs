﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// fuer Eye Tracking
using ViveSR.anipal.Eye;
//Ausgabe in csv Datei
using System.Text;
using System.IO;
using System;


public class TrackEyeData : MonoBehaviour
{
	//Eye Data Konstrukte
	private EyeData_v2 eyeData = new EyeData_v2();
	//private static VerboseData verboseData;
	
	private int frame_sequenz, timestamp;
	private Vector3 gazeOriginLeft, gazeOriginRight;
	private Vector3 gazeDirectionLeft, gazeDirectionRight;
	private float pupilDiameterLeft, pupilDiameterRight;
	private float eyeOpenLeft, eyeOpenRight;
	private Vector2 pupilPositionLeft, pupilPositionRight;
	private float eyeWidthLeft, eyeWidthRight;
	
	//fuer Vignette
	private FullScreenColor vignette;

	private string path;
	
    // Start is called before the first frame update
    void Start()
    {
		System.DateTime now = System.DateTime.Now;
        path = "D:\\Gonschorek\\RecordetData\\" + now.Hour + "_" + now.Minute + ".csv";
		File.AppendAllText(path, "vig_r; vig_g; vig_b; vig_a; frame_sequenz; timestamp; r_gaze_origin_x; r_gaze_origin_y; r_gaze_origin_z; l_gaze_origin_x; l_gaze_origin_y; l_gaze_origin_z; r_gaze_direction_x; r_gaze_direction_y; r_gaze_direction_z; l_gaze_direction_x; l_gaze_direction_y; l_gaze_direction_z; r_pupil_diameter; l_pupil_diameter; r_eye_openness; l_eye_openness; r_pupil_position_x; r_pupil_position_y; r_eye_width; l_eye_width\n");
		
		GameObject camera = GameObject.Find("Camera (eye)");
        //if (camera == null) Debug.Log("cameranotfound");
        //Debug.Log(red + " " + float.Parse(red));
        vignette = camera.GetComponent<FullScreenColor>();
		
	}

    // Update is called once per frame
    void Update()
    {
		bool StartRecord = false;
		
		ViveSR.Error error = SRanipal_Eye_API.GetEyeData_v2(ref eyeData);
		
        //SRanipal_Eye.GetEyeData(ref eyeData);    
		// SRanipal_Eye.GetVerboseData(out verboseData);
		
		if (error == ViveSR.Error.WORK)
		{
			StartRecord = true;
		}
		
		if (StartRecord == true)
		{
			
			if (vignette.enabled == true)
			{
				//Frame Sequenz and time stamp
				frame_sequenz = eyeData.frame_sequence;
				timestamp = eyeData.timestamp;
				//Gaze Data
				gazeOriginLeft = eyeData.verbose_data.left.gaze_origin_mm;
				gazeOriginRight = eyeData.verbose_data.right.gaze_origin_mm;
				gazeDirectionLeft = eyeData.verbose_data.left.gaze_direction_normalized;
				gazeDirectionRight = eyeData.verbose_data.right.gaze_direction_normalized;
				// pupil diameter    
				pupilDiameterLeft = eyeData.verbose_data.left.pupil_diameter_mm;
				pupilDiameterRight = eyeData.verbose_data.right.pupil_diameter_mm;
				// eye open    
				eyeOpenLeft = eyeData.verbose_data.left.eye_openness;    
				eyeOpenRight = eyeData.verbose_data.right.eye_openness;
				// pupil positions    
				pupilPositionLeft = eyeData.verbose_data.left.pupil_position_in_sensor_area; 
				pupilPositionRight = eyeData.verbose_data.right.pupil_position_in_sensor_area;    
				//EyeWidth
				eyeWidthLeft = eyeData.expression_data.left.eye_wide;
				eyeWidthRight = eyeData.expression_data.right.eye_wide;
				
				string text = vignette.red + ";" + vignette.green + ";" + vignette.blue + ";" + vignette.alpha + ";"
				+ frame_sequenz + ";" + timestamp + ";" 
				+ gazeOriginRight.x + ";" + gazeOriginRight.y + ";" + gazeOriginRight.z + ";" 
				+ gazeOriginLeft.x + ";" + gazeOriginLeft.y + ";" + gazeOriginLeft.z + ";"
				+ gazeDirectionRight.x + ";" + gazeDirectionRight.y + ";" + gazeDirectionRight.z + ";" 
				+ gazeDirectionLeft.x + ";" + gazeDirectionLeft.y + ";" + gazeDirectionLeft.z + ";" 
				+ pupilDiameterRight + ";" + pupilDiameterLeft + ";"
				+ eyeOpenRight + ";" + eyeOpenLeft + ";"
				+ pupilPositionRight.x + ";" +pupilPositionRight.y + ";"
				+ pupilPositionLeft.x + ";" +pupilPositionLeft.y + ";"
				+ eyeWidthRight + ";" + eyeWidthLeft 
				+ "\n";
				
			
				File.AppendAllText(path, text);
			}
			
		}
		
		
		
    }
}
