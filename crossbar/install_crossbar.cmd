@echo off
python -V | findstr /c:"3.6." && echo "Python 3.6 detected" || echo "WARNING: Only tested with Python 3.6. Remove this warning if you want to continue anyway." && pause && exit 1
pip install virtualenv
virtualenv venv
call activate.cmd
pip install -r requirements.txt
pause